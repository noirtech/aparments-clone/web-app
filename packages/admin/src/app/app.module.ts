import 'messenger/build/js/messenger.js';
import 'jquery.nestable/jquery.nestable.js';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, PreloadAllModules } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ROUTES } from './app.routes';
import { AppComponent } from './app.component';
import { AppConfig } from './app.config';
import { ErrorComponent } from './pages/error/error.component';

 /*services*/
import { AuthService } from "./shared/services/auth.service";
import { AlertsService } from "./shared/services/alerts.service";
import { ApiService } from "./shared/services/api.service";
import { DataService } from "./shared/services/data.service";
import { AuthGuard } from "./guard/auth-guard.guard";
import { RoleGuard } from "./guard/role.guard";

const APP_PROVIDERS = [
  AppConfig,
  AuthService,
  ApiService,
  DataService,
  AlertsService,
  RoleGuard,
  AuthGuard
];

@NgModule({
  bootstrap: [ AppComponent ],
  declarations: [
    AppComponent,
    ErrorComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(ROUTES, {
      useHash: false,
      preloadingStrategy: PreloadAllModules
    })
  ],
  providers: [
    APP_PROVIDERS
  ]
})
export class AppModule {}
