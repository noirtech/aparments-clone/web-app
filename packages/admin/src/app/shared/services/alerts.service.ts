import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
declare let Messenger: any;

@Injectable()
export class AlertsService {


    constructor() {

    }

    public simpleErrorAlert(message: string) {
        this.clearMessages();
        Messenger(
            { extraClasses: 'messenger-fixed messenger-on-top' }
        ).post({
            message: message ? message : 'Something bad happened; please try again later.',
            type: 'error',
            showCloseButton: true,
            hideAfter: 4,
            hideOnNavigate: true
        });
    }

    public simpleSuccessAlert(message: string) {
        this.clearMessages();
        Messenger(
            { extraClasses: 'messenger-fixed messenger-on-top' }
        ).post({
            message: message,
            type: 'success',
            showCloseButton: true,
            hideAfter: 4,
            hideOnNavigate: true
        });
    }

    public simpleInfoAlert(message: string) {
        this.clearMessages();
        Messenger(
            { extraClasses: 'messenger-fixed messenger-on-top' }
        ).post({
            message: message,
            type: 'info',
            showCloseButton: true,
            hideAfter: 4,
            hideOnNavigate: true
        });
    }

    clearMessages() {
        Messenger().hideAll();
    }

    message(text: string, type?: "success" | "info" | "error") {
        switch (type) {
            case 'success':
                this.simpleSuccessAlert(text);
                break;
            case 'info':
                this.simpleInfoAlert(text)
                break;
            case 'error':
                this.simpleErrorAlert(text)
                break;
            default:
                this.simpleInfoAlert(text)
                break;
        }
    }
}