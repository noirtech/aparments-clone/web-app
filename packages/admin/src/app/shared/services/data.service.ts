import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable()
export class DataService {

    public data:any = {}
    private cloneData:any = {}
    
    constructor(private router:Router){

    }

    setCloneData(data, url?:string){
        if (data) {
            delete data.id;            
            delete data.createdAt;
            this.cloneData = data;
        } else {
            this.cloneData = null;
        }
        if (url) {
            this.router.navigateByUrl(url);
        }
    }

    getCloneData() {
        return this.cloneData ? this.cloneData : {}
    }

    setData(key: string, data: any) {
        this.data[key] = data;
    }

    getData(key: string) {
        return this.data[key] ? this.data[key] : null
    }   
}