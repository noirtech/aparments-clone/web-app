import { Injectable, EventEmitter } from '@angular/core';
import { Router } from "@angular/router";
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { environment } from "../../../environments/environment";
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { AlertsService } from "./alerts.service";

@Injectable()
export class AuthService {
    public token: string = '';
    public user: any;
    private password: any;
    private logDuration: number = 3300000;
    private logedTime: number
    private apiEndPoint: string = '';
    public userInfoChange: EventEmitter<any> = new EventEmitter();


    constructor(
        private router: Router,
        private http: HttpClient,
        private alert: AlertsService,
    ) {
        this.apiEndPoint = 'https://192.168.1.8:3000/';
        this.apiEndPoint = 'https://api.findyournextapartment.com/';
    }

    login(body) {
        this.password = body.password
        return this.http.post(this.apiEndPoint + 'auth/login', body).pipe(
            catchError(this.handleError.bind(this))
        );
    }

    setAuthData(data: any) {
        this.setLogTime();
        setTimeout(() => {
            this.logout();
        }, this.logDuration);
        this.setToken(data.token);
        //this.setUser(data.user)
        this.setUser({email: "test1@test.com", level: "admin", name:"Hola"})
    }

    setToken(token: string) {
        this.token = token;
        localStorage.setItem('token', token)
    }

    setUser(user: any) {
        this.user = user;
        localStorage.setItem('user', JSON.stringify(user))
        this.userInfoChange.emit(this.getUser())
    }

    getToken(): string {
        this.token = this.token ? this.token : localStorage.getItem('token')
        return this.token;
    }

    getUser() {
        this.user = this.user ? this.user : JSON.parse(localStorage.getItem('user'))
        return this.user
    }

    checkToken(): boolean {
        let checked = localStorage.getItem('token') ? true : false
        return checked
    }

    logout() {
        this.router.navigate(['/login']);
        localStorage.clear();
    }

    isAdmin(): boolean {
        return this.getUser().role == 'admin'
    }

    isAgent(): boolean {
        return this.getUser().role == 'subadmin'
    }

    private handleError(error: HttpErrorResponse) {
        let message
        if (error.error instanceof ErrorEvent) {
        } else {
            message = error.error.error.message
            this.alert.simpleErrorAlert(message);
        }
        return throwError(
            'Something bad happened; please try again later.');
    };

    getLogTime(): number {
        return localStorage.getItem('logtime') ? parseInt(localStorage.getItem('logtime')) : null
    }

    setLogTime() {
        this.logedTime = new Date().getTime() + this.logDuration;
        localStorage.setItem('logtime', this.logedTime.toString())
    }

    checkLogTime() {
        if (this.getLogTime()) {
            let dif = new Date().getTime() - this.getLogTime()
            console.log('dif', dif);
            if (dif > 0) {
                this.logout();
            } else {
                setTimeout(() => {
                    this.logout()
                }, (-1 * dif));
            }

        } else {
            this.logout()
        }
    }
}
