import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { AlertsService } from '../services/alerts.service';
import { environment } from "../../../environments/environment";
import { Observable, AsyncSubject } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

declare let swal: any;

@Injectable()
export class ApiService {
    private apiEndPoint: string = '';

    constructor(
        private http: HttpClient,
        private router: Router,
        private authService: AuthService,
        private alert: AlertsService,
    ) {
        if (environment.production == true) {
            this.apiEndPoint = 'https://192.168.1.8:3000/';
            this.apiEndPoint = 'https://api.findyournextapartment.com/';

        } else {
            this.apiEndPoint = 'https://192.168.1.8:3000/';
            this.apiEndPoint = 'https://api.findyournextapartment.com/';

        }
    }

    post(body: object, controller: string, headers?: HttpHeaders) {
        return this.http.post(this.apiEndPoint + controller, body, { headers: headers ? headers : this.getHeader() }).pipe(
            catchError(this.handleError.bind(this))
        );
    }

    get(controller: string, id?: string, params?: string) {
        id = id ? '/' + id : '';
        params = params ? params : '';
        return this.http.get(this.apiEndPoint + controller + id + params, { headers: this.getHeader() }).pipe(
            catchError(this.handleError.bind(this))
        );
    }

    put(body: object, controller: string, id: string) {
        return this.http.put(this.apiEndPoint + controller + '/' + id, body, { headers: this.getHeader() }).pipe(
            catchError(this.handleError.bind(this))
        );
    }

    delete(controller: string, id: string) {
        id = id ? '/' + id : '';
        return this.http.delete(this.apiEndPoint + controller + id, { headers: this.getHeader() }).pipe(
            catchError(this.handleError.bind(this))
        );
    }

    patch(body: object, controller: string, id: string) {
        return this.http.patch(this.apiEndPoint + controller + '/' + id, body, { headers: this.getHeader() }).pipe(
            catchError(this.handleError.bind(this))
        );
    }

    getHeader() {
        let header: HttpHeaders = new HttpHeaders();
        header = header.append('Content-Type', 'application/json');
        header = header.append('Access-Control-Allow-Origin', '*');
        header = header.append('Authorization', 'Bearer ' + this.authService.getToken());
        return header
    }

    private handleError(error: HttpErrorResponse) {
        let message
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
        } else {
            message = error.error.error.message
            this.alert.simpleErrorAlert(message);
        }
        return throwError(
            'Something bad happened; please try again later.');
    };

    message(text: string, type?: "success" | "info" | "error") {
        switch (type) {
            case 'success':
                this.alert.simpleSuccessAlert(text);
                break;
            case 'info':
                this.alert.simpleInfoAlert(text)
                break;
            case 'error':
                this.alert.simpleErrorAlert(text)
                break;
            default:
                this.alert.simpleInfoAlert(text)
                break;
        }
    }
}
