
import { Component, OnInit } from "@angular/core";
import { Location } from '@angular/common';
import { FormGroup, FormControl, Validators } from "@angular/forms";

import { process, State } from '@progress/kendo-data-query';
import { ApiService } from "../../../shared/services/api.service";
import { config } from "../config.models";
import { error } from 'util';

@Component({
  selector: 'app-expenses',
  templateUrl: './expenses.component.html',
  styleUrls: ['./expenses.component.scss']
})
export class ExpensesComponent implements OnInit {

  public expense: FormGroup = new FormGroup({
    name: new FormControl('', Validators.required),
    type: new FormControl('', Validators.required),
    price: new FormControl(0, Validators.required)
  })

  public expenses = config.expenses;
  public expenseType = config.expensesTypes;
  public lookup_data = [];
  public lookup_selected: any;
  public idx: number;
  hasData: boolean;



  constructor(
    private location: Location,
    private api: ApiService,
  ) {
    this.expense.markAsTouched();
  }

  ngOnInit() {
    this.getExpenses()
  }

  lookup_callback(data, index) {
    console.log(data, index);
    this.expense.patchValue(this.lookup_selected)
  }

  add() {
    this.lookup_data.push(this.expense.value);
    console.log(this.lookup_data);
    this.lookup_selected = false;
  }

  update() {
    this.lookup_data[this.idx] = this.expense.value;
    this.lookup_selected = false;
    this.clear();
  }

  edit(index) {
    this.idx = index;
    this.expense.patchValue(this.lookup_data[index]);
    this.lookup_selected = true;
  }

  clear() {
    this.expense.reset();
    this.idx = null;
  }

  save() {
    let body = {
      key: 'expenses',
      content: this.lookup_data
    }
    if (!this.hasData) {
      this.api.post(body, 'configurations').subscribe(res => {
        this.hasData = true;
        this.api.message('Operation completed successfully', 'success')

      }, error => {

      })
    } else {
      this.api.patch(body, 'configurations', 'expenses').subscribe(res => {
        this.api.message('Operation completed successfully', 'success')

      }, error => {

      })
    }
  }

  getExpenses() {
    this.api.get('configurations/expenses').subscribe(res => {
      this.lookup_data = res['content'];
      this.hasData = true;
    })
  }

  goBack() {
    this.location.back();
  }
}