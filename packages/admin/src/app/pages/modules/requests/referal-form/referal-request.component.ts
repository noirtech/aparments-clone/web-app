import { Component, OnInit, Input, OnChanges} from '@angular/core';
import { ApiService } from "../../../../shared/services/api.service";
interface Referral {
  firstName: string,
  lastName: string,
  referFirstName: string,
  referLastName: string,
  referEmail: string,
  referPhone: string,
}

@Component({
  selector: 'referal-request',
  templateUrl: './referal-request.component.html',
  styleUrls: ['../requests.component.scss']
})
export class ReferalRequestsComponent implements OnInit, OnChanges {
  @Input() id;
  public data: any = {};

  constructor(
    private api: ApiService,
  ) { }

  ngOnChanges() {
    if (this.id) {
      this.getData()
    }
  }

  getData() {
    this.api.get('messages/' + this.id).subscribe(res => {
      this.data = res;
    });
  }

  ngOnInit() {
  }

}

