import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { ApiService } from "../../../../shared/services/api.service";

interface Rebate {
  firstName: string,
  lastName: string,
  email: string,
  phone: string,
  apartment: string,
  date: string,
  rentPrice: number,
  leaseTerm: number,
  leasingAgent: string,
}

@Component({
  selector: 'contact-request',
  templateUrl: './contact-request.component.html',
  styleUrls: ['../requests.component.scss']
})

export class ContactRequestComponent implements OnInit, OnChanges {
  @Input() id;
  public data: any = {};

  constructor(
    private api: ApiService,
  ) { }

  ngOnChanges() {
    if (this.id) {
      this.getData()
    }
  }

  getData() {
    this.api.get('messages/' + this.id).subscribe(res => {
      this.data = res;
    });
  }

  ngOnInit() {
  }

}

