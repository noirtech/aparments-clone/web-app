
import { Component, OnInit, } from "@angular/core";
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import {
    GridDataResult,
    DataStateChangeEvent
} from '@progress/kendo-angular-grid';
import { GridComponent } from '@progress/kendo-angular-grid';
import { ExcelExportData } from '@progress/kendo-angular-excel-export';
import { process, State } from '@progress/kendo-data-query';
import { config } from "../../config.models";
import { ApiService } from '../../../../shared/services/api.service';
import { DataService } from '../../../../shared/services/data.service';


@Component({
    selector: 'messagges',
    templateUrl: './list.template.html',
    styleUrls: ['./list.component.scss']
})

export class MessagesListComponent implements OnInit {
    public module = '';
    public title = {
        'contact-form': 'contact form requests',
        'referral-form': 'referral form requests',
        'rebate-form': 'rebate form requests',
    }

    public type: string;
    public gridData: GridDataResult;
    public state: State = {
        skip: 0,
        take: 20
    };
    public data = [];
    public config = [];


    constructor(
        private param: ActivatedRoute,
        private router: Router,
        private location: Location,
        private api: ApiService,
        private dataService: DataService,
    ) {
        this.param.params.subscribe(param => {
            this.module = param.mdl;
            this.type = param.type;
            this.config = config.messages[this.type]
            this.data = [];
            this.processData(this.data);
            this.getData();
        })
        this.allData = this.allData.bind(this);
    }

    ngOnInit() {
    }

    getData() {
        this.data = this.dataService.getData(this.module);
        this.data ? this.processData(this.data) : null;
        this.api.get(this.module + '?filter[type]=' + this.type).subscribe((res: any[]) => {
            this.data = res;
            this.dataService.setData(this.module, res)
            this.processData(this.data);
        })
    }

    processData(data) {
        this.gridData = process(data, this.state);
    }

    dataStateChange(state: DataStateChangeEvent) {
        this.state = state;
        this.gridData = process(this.data, this.state);
    }

    goBack() {
        this.location.back();
    }

    edit(id) {
        this.router.navigate(['/app/view/' + this.type + '/' + id])
    }

    exportToExcel(grid: GridComponent) {
        grid.saveAsExcel();
    }

    allData(): ExcelExportData {
        const result: ExcelExportData = {
            data: process(this.data, { filter: this.state.filter }).data
        };
        return result;
    }
}
