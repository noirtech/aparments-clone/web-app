import { Component, OnInit } from "@angular/core";
import { Location } from '@angular/common';
import { FormGroup, FormControl, Validators } from "@angular/forms";

import { process, State } from '@progress/kendo-data-query';
import { ApiService } from "../../../shared/services/api.service";
import { config } from "../config.models";

@Component({
  selector: 'app-amenities',
  templateUrl: './amenities.component.html',
  styleUrls: ['./amenities.component.scss']
})
export class AmenitiesComponent implements OnInit {

  public amenity: FormGroup = new FormGroup({
    name: new FormControl('', Validators.required),
    category: new FormControl('', Validators.required),
    title: new FormControl('', Validators.required)
  })

  public amenities = config.amenities;
  public amenitiesTypes = config.amenitiesTypes;
  public lookup_data = [];
  public lookup_selected: any;
  public idx: number;
  public hasData: boolean = false;



  constructor(
    private location: Location,
    private api: ApiService,
  ) {
    this.amenity.markAsTouched();
  }

  ngOnInit() {
    this.getExpenses()
  }

  lookup_callback(data, index) {
    console.log(data, index);
    this.amenity.patchValue(this.lookup_selected)
  }

  add() {
    this.lookup_data.push(this.amenity.value);
    console.log(this.lookup_data);
    this.lookup_selected = false;
  }

  update() {
    this.lookup_data[this.idx] = this.amenity.value;
    this.lookup_selected = false;
    this.clear();
  }

  edit(index) {
    this.idx = index;
    this.amenity.patchValue(this.lookup_data[index]);
    this.lookup_selected = true;
  }

  clear() {
    this.amenity.reset();
    this.idx = null;
  }

  save() {
    let body = {
      key: 'amenities',
      content: this.lookup_data
    }
    if (!this.hasData) {
      this.api.post(body, 'configurations').subscribe(res => {
        this.hasData = true;
        this.api.message('Operation completed successfully', 'success')

      }, error => {

      })
    } else {
      this.api.patch(body, 'configurations', 'amenities').subscribe(res => {
        this.api.message('Operation completed successfully', 'success')

      }, error => {

      })
    }

  }

  getExpenses() {
    this.api.get('configurations/amenities').subscribe(res => {
      this.lookup_data = res['content'];
      this.hasData = true;
    })
  }

  goBack() {
    this.location.back();
  }
}
