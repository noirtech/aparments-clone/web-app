import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup } from "@angular/forms";
import { ApiService } from "../../../shared/services/api.service";


@Component({
    selector: 'create',
    templateUrl: './create.template.html',
    styleUrls: ['./create.component.scss']
})

export class CreateComponent {
    
    public view = '';
    public creating:boolean = false;
    public data: FormGroup = new FormGroup({});

    constructor(
        private param: ActivatedRoute,
        private location: Location,
        private api: ApiService,
    ) {
        this.param.params.subscribe(param => {
            Object.keys(this.data.value).forEach(key => { this.data.removeControl(key) })
            this.view = param.mdl;
        })
    }

    create() {
        this.creating = true;
        console.log(this.data.value);
        this.api.post(this.data.value, this.view).subscribe(res => {
            this.creating = false;
            this.api.message('Operation completed successfully', 'success')
        },error => {
            this.creating = false;            
        })
    }

    goBack() {
        this.location.back();
    }

    
}
