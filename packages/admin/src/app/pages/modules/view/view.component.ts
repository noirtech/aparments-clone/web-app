
import { Component } from "@angular/core";
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup } from "@angular/forms";
import { ApiService } from "../../../shared/services/api.service";
import { DataService } from "../../../shared/services/data.service";

@Component({
    selector: 'view',
    templateUrl: './view.template.html',
    styleUrls: ['./view.component.scss']
})

export class ViewComponent {

    public view: string;
    public id: any;
    public title = {
        'contact-form': 'Contact form',
        'referral-form': 'Referral form',
        'rebate-form': 'Rebate form'
    }
    constructor(
        private param: ActivatedRoute,
        private location: Location,
        private api: ApiService,
        private dataService: DataService,
    ) {
        this.param.params.subscribe(param => {
            this.view = param.mdl;
            this.id = param.id;
        })
    }



    goBack() {
        this.location.back();
    }
}
