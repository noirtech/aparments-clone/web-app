export const config = {
    users: [
        { name: "name", label: "Name", width: "150", type: "", template: "" },
        { name: "email", label: "Email", width: "", type: "", template: "" },
        { name: "role", label: "Level", width: "", type: "", template: "" },
    ],
    properties: [
        { name: "name", label: "Name", width: "200", type: "", template: "short" },
        { name: "verified", label: "verified", width: "150", type: "boolean", template: "boolean" },
        { name: "priority", label: "priority", width: "150", type: "", template: "" },
        { name: "address", label: "address", width: "180", type: "", template: "address" },
        { name: "description", label: "Description", width: "300", type: "", template: "long" },
    ],
    amenities: [
        { name: "name", label: "Name", width: "", type: "", template: "" },
        { name: "category", label: "Category", width: "", type: "", template: "" },
        { name: "title", label: "Title", width: "", type: "", template: "" },
    ],
    expenses: [
        { name: "name", label: "Name", width: "", type: "", template: "" },
        { name: "type", label: "Type", width: "", type: "", template: "" },
        { name: "price", label: "Price", width: "", type: "", template: "" },
    ],
    "contact-form": [
        { name: "firstName", label: "Name", width: "", type: "", template: "" },
        { name: "lastName", label: "LastName", width: "", type: "", template: "" },
        { name: "email", label: "Email", width: "", type: "", template: "" },
        { name: "phone", label: "Phone", width: "", type: "", template: "" },
        { name: "city", label: "City", width: "", type: "", template: "" },
    ],
    "referral-form": [
        { name: "referFirstName", label: "Name", width: "", type: "", template: "" },
        { name: "referLastName", label: "LastName", width: "", type: "", template: "" },
        { name: "referEmail", label: "Email", width: "", type: "", template: "" },
        { name: "referPhone", label: "Phone", width: "", type: "", template: "" },
    ],
    "rebate-form": [
        { name: "firstName", label: "Name", width: "", type: "", template: "" },
        { name: "lastName", label: "LastName", width: "", type: "", template: "" },
        { name: "email", label: "Email", width: "", type: "", template: "" },
        { name: "phone", label: "Phone", width: "", type: "", template: "" },
        { name: "apartment", label: "Apartment", width: "", type: "", template: "" },
        { name: "date", label: "Date", width: "", type: "date", template: "" },
    ],
    officeHour: [
        { name: "dayOfWeek", label: "Days", width: "", type: "", template: "" },
        { name: "opens", label: "Open", width: "100", type: "", template: "" },
        { name: "closes", label: "Close", width: "100", type: "", template: "" },
    ],
    expensesTypes: [
        { value: "", label: "" },
        { value: "recurring", label: "Recurring" },
        { value: "one-time", label: "One time" },
        { value: "utilities-included", label: "Utilities included" }
    ],
    amenitiesTypes: [
        { value: "", label: "" },
        { value: "recurring", label: "Recurring" },
        { value: "one-time", label: "One time" },
        { value: "utilities-included", label: "Utilities included" }
    ],
    messages: {
        "referral-form": [
            { name: "firstName", label: "Name", width: "150", type: "", template: "" },
            { name: "lastName", label: "last Name", width: "150", type: "", template: "" },
            { name: "type", label: "Type", width: "150", type: "", template: "" },
            { name: "email", label: "Email", width: "150", type: "", template: "" },
            { name: "phone", label: "Phone", width: "150", type: "", template: "" },
            { name: "city", label: "City", width: "150", type: "", template: "" },
            { name: "preferred", label: "Preferred", width: "150", type: "", template: "" },
            { name: "nBeds", label: "Beds", width: "150", type: "numeric", template: "" },
            { name: "sending", label: "Sending", width: "150", type: "boolean", template: "" },
            { name: "nBaths", label: "Baths", width: "150", type: "numeric", template: "" },
            { name: "allowDogs", label: "Allow Dogs", width: "150", type: "boolean", template: "" },
            { name: "allowCats", label: "Allow Cats", width: "150", type: "boolean", template: "" },
            { name: "howHeard", label: "How Heard", width: "150", type: "", template: "" },
            { name: "size", label: "Size", width: "150", type: "numeric", template: "" },
            { name: "budget", label: "Budget", width: "150", type: "", template: "" },
            { name: "awareOf", label: "Aware Of", width: "150", type: "", template: "" },
            { name: "comments", label: "Comments", width: "150", type: "", template: "" },
        ],
        "rebate-form": [
            { name: "firstName", label: "Name", width: "150", type: "", template: "" },
            { name: "lastName", label: "last Name", width: "150", type: "", template: "" },
            { name: "type", label: "Type", width: "150", type: "", template: "" },
            { name: "email", label: "Email", width: "150", type: "", template: "" },
            { name: "phone", label: "Phone", width: "150", type: "", template: "" },
            { name: "apartment", label: "Apartment", width: "150", type: "", template: "" },
            { name: "date", label: "Date", width: "150", type: "date", template: "" },
            { name: "rentPrice", label: "Rent Price", width: "150", type: "numeric", template: "" },
            { name: "leaseTerm", label: "Lease Term", width: "150", type: "", template: "" },
            { name: "fynaName", label: "FynaName", width: "150", type: "", template: "" },
            { name: "leasingAgent", label: "Leasing Agent", width: "150", type: "", template: "" }
        ],
        "contact-form": [
            { name: "firstName", label: "Name", width: "150", type: "", template: "" },
            { name: "lastName", label: "last Name", width: "150", type: "", template: "" },
            { name: "type", label: "Type", width: "150", type: "", template: "" },
            { name: "email", label: "Email", width: "150", type: "", template: "" },
            { name: "phone", label: "Phone", width: "150", type: "", template: "" },
            { name: "city", label: "City", width: "150", type: "", template: "" },
            { name: "preferred", label: "Preferred", width: "150", type: "", template: "" },
            { name: "nBeds", label: "Beds", width: "150", type: "numeric", template: "" },
            { name: "sending", label: "Sending", width: "150", type: "boolean", template: "" },
            { name: "nBaths", label: "Baths", width: "150", type: "numeric", template: "" },
            { name: "allowDogs", label: "Allow Dogs", width: "150", type: "boolean", template: "" },
            { name: "allowCats", label: "Allow Cats", width: "150", type: "boolean", template: "" },
            { name: "howHeard", label: "How Heard", width: "150", type: "", template: "" },
            { name: "size", label: "Size", width: "150", type: "numeric", template: "" },
            { name: "budget", label: "Budget", width: "150", type: "", template: "" },
            { name: "awareOf", label: "Aware Of", width: "150", type: "", template: "" },
            { name: "comments", label: "Comments", width: "150", type: "", template: "" },
        ]
    }
}

export const listConfig = {
    properties: {
        limit: 2000
    },
    users: {
        limit: 1
    },
    messages: {
        limit: 1000
    }
}
