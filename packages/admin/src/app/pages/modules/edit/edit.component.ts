
import { Component } from "@angular/core";
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup } from "@angular/forms";
import { ApiService } from "../../../shared/services/api.service";
import { DataService } from "../../../shared/services/data.service";

@Component({
    selector: 'edit',
    templateUrl: './edit.template.html',
    styleUrls: ['./edit.component.scss']
})

export class EditComponent {
    public view = '';
    public id = '';
    public data: FormGroup = new FormGroup({});
    public params: any;
    public editing: boolean = false;

    constructor(
        private param: ActivatedRoute,
        private location: Location,
        private api: ApiService,
        private dataService: DataService,
    ) {
        this.param.params.subscribe(param => {
            Object.keys(this.data.value).forEach(key => { this.data.removeControl(key) })
            this.view = param.mdl;
            this.id = param.id;
            this.getData();
        })
    }

    edit() {
        this.editing = true;            
        this.api.put(this.data.value, this.view, this.id).subscribe(res => {
            this.editing = false;   
            this.api.message('Operation completed successfully', 'success')
        },error => {
            this.editing = false;            
        })
    }

    getData() {
        this.api.get(this.view,this.id).subscribe((res: any) => {
            this.data.patchValue(res)
        })
    }

    goBack() {
        this.location.back();
    }

    clone(){
        this.dataService.setCloneData(this.data.value,'/app/create/properties');        
    }
}
