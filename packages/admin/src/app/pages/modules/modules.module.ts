import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TooltipModule } from 'ngx-bootstrap';


import { RouterModule } from '@angular/router';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';
import { ViewComponent } from './view/view.component';
import { ProductsComponent } from './products/products.component';
import { ListComponent } from './list/list.component';
import { MessagesListComponent } from './requests/list/list.component';
import { GridModule, ExcelModule } from '@progress/kendo-angular-grid';
import { WidgetModule } from '../../layout/widget/widget.module';
import { DialogModule } from 'primeng/dialog';
import { TableModule } from 'primeng/table';
import { MultiSelectModule } from 'primeng/multiselect';
import { DropdownModule } from 'primeng/dropdown';
import { UsersComponent } from './users/users.component';
import { FrecuenQuestionsComponent } from "./settings/fq/fq.component";
import { HowWorkComponent } from "./settings/howWork/howWork.component";
import { TermConditionsComponent } from "./settings/termConditions/termConditions.component";
import { QuillModule } from "ngx-quill";
import { TextEditorComponent } from './text-editor/text-editor.component';
import { RoleGuard } from '../../guard/role.guard';
import { ExpensesComponent } from './expenses/expenses.component';
import { AmenitiesComponent } from './amenities/amenities.component';
import { MainCitiesComponent } from './settings/mainCities/main-cities.component';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { ContactRequestComponent } from "./requests/contact-form/contact-request.component";
import { ReferalRequestsComponent } from "./requests/referal-form/referal-request.component";
import { RebateRequestsComponent } from "./requests/rebate-form/rebate-request.component";

export const routes = [
  { path: 'create/:mdl', component: CreateComponent, pathMatch: 'full' },
  { path: 'edit/:mdl/:id', component: EditComponent, pathMatch: 'full' },
  { path: 'view/:mdl/:id', component: ViewComponent, pathMatch: 'full' },
  { path: 'list/:mdl', component: ListComponent, pathMatch: 'full' },
  { path: 'messages/:mdl/:type', component: MessagesListComponent, pathMatch: 'full' },
  { path: 'fq', component: FrecuenQuestionsComponent, pathMatch: 'full', canActivate: [RoleGuard] },
  { path: 'hw', component: HowWorkComponent, pathMatch: 'full', canActivate: [RoleGuard] },
  { path: 'policies', component: TermConditionsComponent, pathMatch: 'full', canActivate: [RoleGuard] },
  { path: 'expenses', component: ExpensesComponent, pathMatch: 'full', canActivate: [RoleGuard] },
  { path: 'amenities', component: AmenitiesComponent, pathMatch: 'full', canActivate: [RoleGuard] },
  { path: 'main-cities', component: MainCitiesComponent, pathMatch: 'full', canActivate: [RoleGuard] }
];


@NgModule({
  imports: [
    CommonModule,
    GridModule,
    ExcelModule,
    FormsModule,
    ReactiveFormsModule,
    WidgetModule,
    DialogModule,
    MultiSelectModule,
    DropdownModule,
    TableModule,
    AutoCompleteModule,
    TooltipModule.forRoot(),
    QuillModule.forRoot(),
    RouterModule.forChild(routes)

  ],
  declarations: [
    CreateComponent,
    EditComponent,
    ViewComponent,
    ListComponent,
    MessagesListComponent,
    ProductsComponent,
    UsersComponent,
    FrecuenQuestionsComponent,
    HowWorkComponent,
    TermConditionsComponent,
    TextEditorComponent,
    ExpensesComponent,
    AmenitiesComponent,
    ContactRequestComponent,
    ReferalRequestsComponent,
    RebateRequestsComponent,
    MainCitiesComponent
  ]
})
export class ModulesModule {
  static routes = routes;
}
