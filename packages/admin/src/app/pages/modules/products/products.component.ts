
import { Component, Input, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators, FormBuilder } from "@angular/forms";
import { exampleData, fields } from "./products";
import { config } from "../config.models";
import { DataService } from "../../../shared/services/data.service"
import { ApiService } from "../../../shared/services/api.service"
declare var google: any;
var autocomplete: any;
declare let L: any;
var map;
@Component({
    selector: 'products',
    templateUrl: './products.template.html',
    styleUrls: ['../form.component.scss']
})

export class ProductsComponent implements OnInit {
    @Input() id: string;
    @Input() product: FormGroup;
    public floorPlans: FormGroup;
    public officeHours: FormGroup;
    public amenities: FormGroup;
    public photos: FormGroup;
    public nearby: FormGroup;
    public schools: FormGroup;
    public creatingFloorPlan: boolean = false;
    public creatingOfficeHour: boolean = false;
    public creatingPhoto: boolean = false;
    public creatingPhotoFP: boolean = false;
    public creatingNearby: boolean = false;
    public creatingSchool: boolean = false;
    public creatingFloorPlansPhoto: boolean = false;
    public displayLookup: boolean = false;
    public addressLookup: boolean = false;
    public index: number;
    public index2: number;
    public lookupFields: any[] = [];
    public filteredAvailability: any[];
    public lookup_data: any;
    public actionCounter: number = 0;
    public lookup_selected: any;
    public whichOne: string;
    public title: string;
    public view: string = 'home';
    public globalFilterFields: any[] = [];
    public expensesConfig = config.expenses;
    public officeHourConfig = config.officeHour;
    public days = [
        { label: 'Monday', value: 'Monday' },
        { label: 'Tuesday', value: 'Tuesday' },
        { label: 'Wednesday', value: 'Wednesday' },
        { label: 'Thursday', value: 'Thursday' },
        { label: 'Friday', value: 'Friday' },
        { label: 'Saturday', value: 'Saturday' },
        { label: 'Sunday', value: 'Sunday' },
    ];
    public bedrooms = [
        { label: null, value: null },
        { label: 'Studio', value: 0 },
        { label: '1', value: 1 },
        { label: '2', value: 2 },
        { label: '3', value: 3 },
        { label: '4', value: 4 },
        { label: '5', value: 5 },
        { label: '6', value: 6 },
    ];

    public languages = [
        { label: 'English', value: 'English' },
        { label: 'Spanish', value: 'Spanish' }
    ]

    public schoolCategory = [
        { label: null, value: null },
        { label: 'Private', value: 'private' },
        { label: 'Public', value: 'public' }
    ]

    constructor(
        private formBuilder: FormBuilder, 
        private dataService: DataService,
        private api: ApiService
        ) { }

    addressOnShow() {
        autocomplete = new google.maps.places.Autocomplete(document.getElementById('addressSearch'));
        autocomplete.setFields(['address_components', 'geometry', 'formatted_address'])
        autocomplete.addListener('place_changed', () => this.addressChange(this));
    }

    addressChange(_this) {
        let obj: any = {};
        let address = autocomplete.getPlace();
        let datos = address.address_components.reduce((before, { types, long_name }) =>
            ({ ...before, [types[0]]: long_name })
            , {})
        obj.location = { coordinates: [address.geometry.location.lat(), address.geometry.location.lng()] }
        //obj.country = datos.country ? datos.country : '';
        obj.state = datos.administrative_area_level_1 ? datos.administrative_area_level_1 : '';
        obj.city = datos.locality ? datos.locality : '';
        obj.locality = datos.administrative_area_level_2 ? datos.administrative_area_level_2 : '';
        obj.zipCode = datos.postal_code ? datos.postal_code : '';
        _this.product.patchValue({ address: obj })
    }

    ngOnInit() {
        this.createForms();
        this.product.markAllAsTouched();
        this.floorPlans.markAllAsTouched();
        this.officeHours.markAllAsTouched();
        this.photos.markAllAsTouched();
        this.schools.markAllAsTouched();
        this.nearby.markAllAsTouched();
        this.product.patchValue(this.dataService.getCloneData());
        this.dataService.setCloneData({});
    }

    createForms() {
        this.product.addControl('name', new FormControl('', Validators.required))
        this.product.addControl('origin', new FormControl(''))
        this.product.addControl('description', new FormControl(''))
        this.product.addControl('nearby', new FormControl([]))
        this.product.addControl('schools', new FormControl([]))
        this.product.addControl('floorPlans', new FormControl([]))
        this.product.addControl('expenses', new FormControl([]))
        this.product.addControl('amenities', new FormControl([]))
        this.product.addControl('languages', new FormControl([]))
        this.product.addControl('officeHours', new FormControl([]))
        this.product.addControl('photos', new FormControl([]))
        this.product.addControl('address', new FormGroup({
            "location": new FormGroup({
                "type": new FormControl('point', Validators.required),
                "coordinates": new FormControl([], Validators.required),
            }),
            "country": new FormControl('us', Validators.required),
            "locality": new FormControl('', Validators.required),
            "city": new FormControl('', Validators.required),
            "state": new FormControl('', Validators.required),
            "zipCode": new FormControl('', Validators.required)
        }, Validators.required))
        this.product.addControl('photos', new FormControl([]))

        this.floorPlans = new FormGroup({
            bedrooms: new FormControl(0, Validators.required),
            bathrooms: new FormControl(0, Validators.required),
            name: new FormControl('', Validators.required),
            unit: new FormControl(''),
            availability: new FormControl('', Validators.required),
            size: new FormControl(0),
            deposit: new FormControl(0, Validators.required),
            price: new FormControl([], Validators.required),
            photos: new FormControl([])
        })

        this.officeHours = new FormGroup({
            dayOfWeek: new FormControl([], Validators.required),
            opens: new FormControl('', Validators.required),
            closes: new FormControl('', Validators.required),
        })

        this.photos = new FormGroup({
            src: new FormControl('', Validators.required),
            thumbnail: new FormControl(''),
            description: new FormControl(''),
            mediaType: new FormControl(0),
            attachmentType: new FormControl(0),
            altText: new FormControl(''),
        })

        this.nearby = new FormGroup({
            name: new FormControl(''),
            category: new FormControl(''),
            distance: new FormControl(''),
            driving: new FormControl(''),
        })

        this.schools = new FormGroup({
            type: new FormControl(''),
            category: new FormControl(''),
            grades: new FormControl(''),
            students: new FormControl(''),
            phone: new FormControl('')
        })

    }


    addToArray(path: string, wichone: string, position: number, data) {
        //let newData:any[] = this.product.get(path).value;
        //newData[position] = data;
        this.product.patchValue({ [wichone]: [25, 35] })
    }

    addFloorPlan() {
        let data = [...this.product.get('floorPlans').value, ...this.floorPlans.value];
        this.product.patchValue({ floorPlans: data });
        this.floorPlans.reset();
    }

    addElemenet(whichOne) {
        this.title = whichOne;
        let data = [...this.product.get(whichOne).value, ...this[whichOne].value];
        console.log({ [whichOne]: data })
        this.product.patchValue({ [whichOne]: data });
        this[whichOne].reset();
    }

    delete(whichOne, index) {
        let data = this.product.get(whichOne).value;
        data.splice(index, 1);
        this.product.patchValue({ [whichOne]: data });
    }

    edit(whichOne, index) {
        this.title = whichOne;
        this[whichOne].patchValue(this.product.get(whichOne).value[index]);
        this.index = index;
    }

    update(whichOne) {
        let data: any[] = this.product.get(whichOne).value;
        data[this.index] = this[whichOne].value;
        this.product.patchValue({ [whichOne]: data });
        this.index = null;
        this.actionCounter++;
    }

    onDisplayLookup(whichOne: string) {
        this.title = whichOne;
        this.lookupFields = fields[whichOne];
        this.api.get('configurations/' + whichOne).subscribe(res => {
            this.lookup_data = res.content;
            this.globalFilterFields = this.lookupFields.map(el => el.name)
            this.lookup_selected = this.product.get(whichOne).value;
            this.whichOne = whichOne;
            this.displayLookup = true;
        })
    }

    lookup_callback() {
        this.product.patchValue(
            { [this.whichOne]: this.lookup_selected }
        )
    }

    reset(wichone: string) {
        this[wichone].reset();
    }

    public newPrice: number
    addToData(data: any, child: string, parent: string) {
        console.log('la data es', data);

        let newdata = [...this[parent].get(child).value, ...this[data]]
        this[parent].patchValue({ [child]: newdata })

    }

    public availabilities = ["available-soon", "available-now", "none-of-these-are-available"]

    filterAvailability(event) {
        let query = event.query;
        this.filteredAvailability = this.filterA(query, this.availabilities);
    }

    filterA(query, availabilities: any[]): any {
        let filtered: any[] = [];
        availabilities.forEach((el, index) => {
            if (el.toLowerCase().indexOf(query.toLowerCase()) == 0) {
                filtered.push(el);
            }
        })
        console.log(filtered);
        return filtered;
    }

    addPhotoFP() {
        let data: any[] = this.floorPlans.get('photos').value ? [...this.floorPlans.get('photos').value, ...this.photos.value] : [...this.photos.value]
        this.floorPlans.patchValue({ photos: data })
        this.photos.reset();
    }

    editPhotoFP(index) {
        this.index2 = index;
        this.photos.patchValue(this.floorPlans.get('photos').value[index]);
    }

    deletePhotoFP(index) {
        let data: any[] = this.floorPlans.get('photos').value;
        data.splice(index, 1)
        this.floorPlans.patchValue({ photos: data });
    }

    updatePhotoFP() {
        let data: any[] = this.floorPlans.get('photos').value;
        data[this.index2] = this.photos.value
        this.floorPlans.patchValue({ photos: data })
    }


}
