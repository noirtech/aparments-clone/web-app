export const exampleData = {
    "expenses": [
        {
            "type": "recurring",
            "name": "Cat Rent",
            "price": "$10"
        },
        {
            "type": "recurring",
            "name": "Dog Rent",
            "price": "$10"
        },
        {
            "type": "one-time",
            "name": "Admin Fee",
            "price": "$100"
        },
        {
            "type": "one-time",
            "name": "Application Fee",
            "price": "$40"
        },
        {
            "type": "one-time",
            "name": "Cat Fee",
            "price": "$400"
        },
        {
            "type": "one-time",
            "name": "Dog Fee",
            "price": "$400"
        },
        {
            "type": "utilities-included",
            "name": "Trash Removal",
            "price": "Included"
        },
        {
            "type": "one-time2",
            "name": "Cat Fee",
            "price": "$400"
        },
        {
            "type": "one-time2",
            "name": "Dog Fee",
            "price": "$400"
        },
        {
            "type": "utilities-included2",
            "name": "Trash Removal",
            "price": "Included"
        }
    ],
    "amenities": [
        {
          "category": "unique-features",
          "name": "custom",
          "title": "24 hour shuttle access to WinStar Casino"
        },
        {
          "category": "unique-features",
          "name": "custom",
          "title": "Breakfast Bar"
        },
        {
          "category": "unique-features",
          "name": "custom",
          "title": "Freeway Access"
        },
        {
          "category": "unique-features",
          "name": "custom",
          "title": "Gift Wrap Station"
        },
        {
          "category": "unique-features",
          "name": "custom",
          "title": "High Ceilings"
        },
        {
          "category": "unique-features",
          "name": "custom",
          "title": "Less than 1 Mile from Winstar Casino!"
        },
        {
          "category": "unique-features",
          "name": "custom",
          "title": "Linen Storage"
        },
        {
          "category": "unique-features",
          "name": "custom",
          "title": "Theatre Room"
        },
        {
          "category": "unique-features",
          "name": "custom",
          "title": "Wood Laminate flooring"
        },
        {
          "category": "services",
          "name": "package-service",
          "title": "Package Service"
        },
        {
          "category": "services",
          "name": "community-wide-wifi",
          "title": "Community-Wide WiFi"
        },
        {
          "category": "services",
          "name": "wi-fi-at-pool-and-clubhouse",
          "title": "Wi-Fi at Pool and Clubhouse"
        },
        {
          "category": "services",
          "name": "maintenance-on-site",
          "title": "Maintenance on site"
        },
        {
          "category": "services",
          "name": "property-manager-on-site",
          "title": "Property Manager on Site"
        },
        {
          "category": "services",
          "name": "24-hour-availability",
          "title": "24 Hour Availability"
        },
        {
          "category": "services",
          "name": "furnished-units-available",
          "title": "Furnished Units Available"
        },
        {
          "category": "services",
          "name": "renters-insurance-program",
          "title": "Renters Insurance Program"
        },
        {
          "category": "services",
          "name": "online-services",
          "title": "Online Services"
        },
        {
          "category": "services",
          "name": "planned-social-activities",
          "title": "Planned Social Activities"
        },
        {
          "category": "services",
          "name": "guest-apartment",
          "title": "Guest Apartment"
        },
        {
          "category": "interior",
          "name": "business-center",
          "title": "Business Center"
        },
        {
          "category": "interior",
          "name": "clubhouse",
          "title": "Clubhouse"
        },
        {
          "category": "interior",
          "name": "lounge",
          "title": "Lounge"
        },
        {
          "category": "interior",
          "name": "multi-use-room",
          "title": "Multi Use Room"
        },
        {
          "category": "interior",
          "name": "breakfastcoffee-concierge",
          "title": "Breakfast/Coffee Concierge"
        },
        {
          "category": "interior",
          "name": "conference-room",
          "title": "Conference Room"
        },
        {
          "category": "interior",
          "name": "corporate-suites",
          "title": "Corporate Suites"
        },
        {
          "category": "outdoor-space",
          "name": "courtyard",
          "title": "Courtyard"
        },
        {
          "category": "outdoor-space",
          "name": "grill",
          "title": "Grill"
        },
        {
          "category": "outdoor-space",
          "name": "picnic-area",
          "title": "Picnic Area"
        },
        {
          "category": "outdoor-space",
          "name": "balcony",
          "title": "Balcony"
        },
        {
          "category": "outdoor-space",
          "name": "patio",
          "title": "Patio"
        },
        {
          "category": "outdoor-space",
          "name": "porch",
          "title": "Porch"
        },
        {
          "category": "outdoor-space",
          "name": "deck",
          "title": "Deck"
        },
        {
          "category": "outdoor-space",
          "name": "grill",
          "title": "Grill"
        },
        {
          "category": "fitness-&-recreation",
          "name": "fitness-center",
          "title": "Fitness Center"
        },
        {
          "category": "fitness-&-recreation",
          "name": "pool",
          "title": "Pool"
        },
        {
          "category": "fitness-&-recreation",
          "name": "bike-storage",
          "title": "Bike Storage"
        },
        {
          "category": "fitness-&-recreation",
          "name": "basketball-court",
          "title": "Basketball Court"
        },
        {
          "category": "fitness-&-recreation",
          "name": "volleyball-court",
          "title": "Volleyball Court"
        },
        {
          "category": "fitness-&-recreation",
          "name": "walkingbiking-trails",
          "title": "Walking/Biking Trails"
        },
        {
          "category": "fitness-&-recreation",
          "name": "gameroom",
          "title": "Gameroom"
        },
        {
          "category": "fitness-&-recreation",
          "name": "media-centermovie-theatre",
          "title": "Media Center/Movie Theatre"
        },
        {
          "category": "features",
          "name": "high-speed-internet-access",
          "title": "High Speed Internet Access"
        },
        {
          "category": "features",
          "name": "wi-fi",
          "title": "Wi-Fi"
        },
        {
          "category": "features",
          "name": "washerdryer---in-unit",
          "title": "Washer/Dryer - In Unit"
        },
        {
          "category": "features",
          "name": "air-conditioning",
          "title": "Air Conditioning"
        },
        {
          "category": "features",
          "name": "heating",
          "title": "Heating"
        },
        {
          "category": "features",
          "name": "ceiling-fans",
          "title": "Ceiling Fans"
        },
        {
          "category": "features",
          "name": "cable-ready",
          "title": "Cable Ready"
        },
        {
          "category": "features",
          "name": "satellite-tv",
          "title": "Satellite TV"
        },
        {
          "category": "features",
          "name": "tubshower",
          "title": "Tub/Shower"
        },
        {
          "category": "features",
          "name": "handrails",
          "title": "Handrails"
        },
        {
          "category": "features",
          "name": "sprinkler-system",
          "title": "Sprinkler System"
        },
        {
          "category": "features",
          "name": "wheelchair-accessible-rooms",
          "title": "Wheelchair Accessible (Rooms)"
        },
        {
          "category": "kitchen",
          "name": "dishwasher",
          "title": "Dishwasher"
        },
        {
          "category": "kitchen",
          "name": "disposal",
          "title": "Disposal"
        },
        {
          "category": "kitchen",
          "name": "ice-maker",
          "title": "Ice Maker"
        },
        {
          "category": "kitchen",
          "name": "pantry",
          "title": "Pantry"
        },
        {
          "category": "kitchen",
          "name": "kitchen",
          "title": "Kitchen"
        },
        {
          "category": "kitchen",
          "name": "microwave",
          "title": "Microwave"
        },
        {
          "category": "kitchen",
          "name": "oven",
          "title": "Oven"
        },
        {
          "category": "kitchen",
          "name": "range",
          "title": "Range"
        },
        {
          "category": "kitchen",
          "name": "refrigerator",
          "title": "Refrigerator"
        },
        {
          "category": "kitchen",
          "name": "freezer",
          "title": "Freezer"
        },
        {
          "category": "kitchen",
          "name": "instant-hot-water",
          "title": "Instant Hot Water"
        },
        {
          "category": "living-space",
          "name": "hardwood-floors",
          "title": "Hardwood Floors"
        },
        {
          "category": "living-space",
          "name": "carpet",
          "title": "Carpet"
        },
        {
          "category": "living-space",
          "name": "vinyl-flooring",
          "title": "Vinyl Flooring"
        },
        {
          "category": "living-space",
          "name": "dining-room",
          "title": "Dining Room"
        },
        {
          "category": "living-space",
          "name": "views",
          "title": "Views"
        },
        {
          "category": "living-space",
          "name": "walk-in-closets",
          "title": "Walk-In Closets"
        },
        {
          "category": "living-space",
          "name": "furnished",
          "title": "Furnished"
        },
        {
          "category": "living-space",
          "name": "double-pane-windows",
          "title": "Double Pane Windows"
        },
        {
          "category": "security",
          "name": "package-service",
          "title": "Package Service"
        },
        {
          "category": "security",
          "name": "property-manager-on-site",
          "title": "Property Manager on Site"
        }
      ],
}

export const fields = {
    "expenses": [
        { "name": "name", "label": "Name" },
        { "name": "type", "label": "Type" },
        { "name": "price", "label": "Price" }
    ],
    "amenities": [
        { "name": "name", "label": "Name" },
        { "name": "category", "label": "Category" },
        { "name": "title", "label": "Title" }
    ]
}