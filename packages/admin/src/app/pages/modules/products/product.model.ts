export interface IFloorPlan {
    bedrooms: number;
    bathrooms: number;
    price: [number, number];
    photos: IPhoto[];
    name: string;
    unit: string;
    size: number;
    availability: "available-soon" | "available-now" | "none-of-these-are-available" | string;
    deposit: number;
}
export interface IPhoto {
    src: string;
    thumbnail: string;
    description: string;
    mediaType: number;
    attachmentType: number;
    altText: string;
}
export interface IAmenity {
    name: string;
    title: string;
    category: string;
    description?: string;
    policies?: string[];
}
export interface INearby {
    name: string;
    category: string;
    driving: string;
    distance: string;
}
export interface ISchool {
    grades: string;
    type: string;
    category: "public" | "private";
    students: number;
    phone: string;
}
export interface IOfficeHour {
    dayOfWeek: Array<"Monday" | "Tuesday" | "Wednesday" | "Thursday" | "Friday" | "Saturday" | "Sunday">;
    opens: string;
    closes: string;
}
export interface ILocation {
    type: "Point";
    coordinates: [number, number];
}
export interface IGeoLocation {
    location: ILocation;
    locality: string;
    city: string;
    zipCode: string;
    state: string;
    country: "us";
}
export interface IExpense {
    type: "recurring" | "one-time" | "utilities-included";
    name: string;
    price: string;
}
export interface IRentProperty {
    id: string;
    origin: string;
    name: string;
    description: string;
    nearby: INearby[];
    schools: ISchool[];
    floorPlans: IFloorPlan[];
    photos: IPhoto[];
    amenities: IAmenity[];
    languages: string[];
    officeHours: IOfficeHour[];
    address: IGeoLocation;
    expenses: IExpense[];
}
export interface IUserCredentials {
    email: string;
    password: string;
}
export interface IUser {
    id: string;
    email: string;
    name?: string;
    role: "admin" | "subadmin";
}
