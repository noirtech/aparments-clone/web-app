import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';


@Component({
  selector: 'text-editor',
  templateUrl: './text-editor.component.html',
  styleUrls: ['./text-editor.component.scss']
})
export class TextEditorComponent implements OnInit {

  @Input() editorForm: FormGroup;
  public editorStyle = {
    height: '500px',
    backgroundColor: '#ffffff'
  }

  constructor() {
  }


  ngOnInit() {
    this.editorForm.addControl('text', new FormControl(''))
  }

}
