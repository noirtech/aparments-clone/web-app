
import { Component, OnInit, } from "@angular/core";
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import {
    GridDataResult,
    DataStateChangeEvent
} from '@progress/kendo-angular-grid';
import { GridComponent } from '@progress/kendo-angular-grid';
import { ExcelExportData } from '@progress/kendo-angular-excel-export';
import { process, State } from '@progress/kendo-data-query';
import { config, listConfig } from "../config.models";
import { ApiService } from '../../../shared/services/api.service';
import { DataService } from '../../../shared/services/data.service';
import { Subscription } from 'rxjs';


@Component({
    selector: 'list',
    templateUrl: './list.template.html',
    styleUrls: ['./list.component.scss']
})

export class ListComponent implements OnInit {
    public module = '';
    public type = '';
    public gridData: GridDataResult;
    public state: State = {
        skip: 0,
        take: 20
    };
    public data = [];
    public oldData = [];
    public config = [];
    public skip: number = 0;
    public subs: Subscription



    constructor(
        private param: ActivatedRoute,
        private router: Router,
        private location: Location,
        private api: ApiService,
        private dataService: DataService,
    ) {
        this.param.params.subscribe(param => {
            this.module = param.mdl;
            this.type = param.type;
            this.config = this.type ? config[this.module][this.type] : config[this.module];
            this.data = [];
            this.subs ? this.subs.unsubscribe() : null;
            this.processData(this.data);
            this.getCount();
        })
        this.allData = this.allData.bind(this);
    }

    ngOnInit() {
    }

    getData(count?: number) {
        this.oldData = this.dataService.getData(this.module) ? this.dataService.getData(this.module) : this.data;
        this.oldData ? this.processData(this.oldData) : null;
        if (count < this.skip) {
            let where = this.type ? 'filter[where][type]=' + this.type + '&' : '';
            let query = this.setQuery() ? this.setQuery() : ''
            this.subs = this.api.get(this.module + '?'+ where +'filter[limit]=' + listConfig[this.module].limit + '&filter[skip]=' + (count * listConfig[this.module].limit) + query).subscribe((res: any[]) => {
                this.data = this.data != null ? [...this.data, ...res] : res;
                this.oldData ? null : this.processData(this.data);
                this.getData(count += 1)
            })
        } else {
            this.dataService.setData(this.module, this.data);
            this.processData(this.data);
            this.subs ? this.subs.unsubscribe() : null;
        }
    }

    getCount() {
        this.api.get(this.module + '/count').subscribe((data: any) => {
            this.skip = Math.ceil(data.count / listConfig[this.module].limit)
            console.log('skiip', this.skip);
            this.getData(0)
        })
    }

    setQuery() {
        let query = "";
        let array = this.config.map(el => el.name);
        array.push("id");
        array.forEach(el => { query += '&filter[fields][' + el + ']=true' });
        return query
    }

    processData(data) {
        this.gridData = process(data, this.state);
    }

    dataStateChange(state: DataStateChangeEvent) {
        this.state = state;
        this.gridData = process(this.data, this.state);
    }

    goBack() {
        this.location.back();
    }

    edit(id) {
        this.router.navigate(['/app/edit/' + this.module + '/' + id])
    }

    ngOnDestroy() {
        this.subs ? this.subs.unsubscribe() : null;
    }

    exportToExcel(grid: GridComponent) {
        grid.saveAsExcel();
      }

    allData(): ExcelExportData {
        const result: ExcelExportData = {
          data: process(this.data, { filter: this.state.filter }).data
        };
        return result;
      }
}
