import { Component, OnInit } from "@angular/core";
import { Location } from '@angular/common';
import { process, State } from '@progress/kendo-data-query';
import { ApiService } from "../../../../shared/services/api.service";

@Component({
  selector: 'main-cities',
  templateUrl: './main-cities.component.html',
  styleUrls: ['./main-cities.component.scss']
})
export class MainCitiesComponent implements OnInit {

  public data:any[] = [];
  public imgData:any = {};
  public index = null;
  public hasData: boolean = false;

  constructor(
    private location: Location,
    private api: ApiService,
  ) {
  }

  ngOnInit() {
    this.mainCities()
  }

  add(){
    this.data.push(this.imgData)
  }

  update(){
    this.data[this.index] = this.imgData;
    this.index = null;
  }

  edit(i){
    this.index = i;
    this.imgData = {...this.data[i]}
  }

  delete(i){
    this.data.splice(i,1);
  }
  save() {
    let body = {
      key: 'main-cities',
      cities: this.data
    }
    if (!this.hasData) {
      this.api.post(body, 'configurations').subscribe(res => {
        this.hasData = true;
        this.api.message('Operation completed successfully', 'success')

      }, error => {

      })
    } else {
      this.api.patch(body, 'configurations', 'main-cities').subscribe(res => {
        this.api.message('Operation completed successfully', 'success')

      }, error => {

      })
    }

  }

  mainCities() {
    this.api.get('configurations/main-cities').subscribe(res => {
      this.data = res['cities'];
      this.hasData = true;
    })
  }

  goBack() {
    this.location.back();
  }
}
