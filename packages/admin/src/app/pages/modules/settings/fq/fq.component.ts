
import { Component, Input, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators, FormBuilder } from "@angular/forms";
import { Location } from '@angular/common';
import { ApiService } from '../../../../shared/services/api.service';

@Component({
    selector: 'frecuent-questions',
    templateUrl: './fq.template.html',
    styleUrls: ['../settings.component.scss']
})

export class FrecuenQuestionsComponent implements OnInit {

    public text:FormGroup = new FormGroup({});
    public hasData: boolean = false;

    constructor(
        private formBuilder: FormBuilder,
        private location: Location,
        private api: ApiService,
    ) { }

    ngOnInit() {
      this.getdata()
    }

    goBack() {
        this.location.back();
    }

    save() {
        let body = {
          key: 'faq',
          content: this.text.value.text
        }
        if (!this.hasData) {
          this.api.post(body, 'configurations').subscribe(res => {
            this.hasData = true;
          }, error => {
    
          })
        } else {
          this.api.patch(body, 'configurations', 'faq').subscribe(res => {
    
          }, error => {
    
          })
        }
    
      }
    
      getdata() {
        this.api.get('configurations/faq').subscribe(res => {
          this.text.patchValue({ text: res['content'] });
          this.hasData = true;
        })
      }
}
