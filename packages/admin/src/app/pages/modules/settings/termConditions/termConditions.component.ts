
import { Component, Input, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators, FormBuilder } from "@angular/forms";
import { Location } from '@angular/common';
import { ApiService } from '../../../../shared/services/api.service';

@Component({
  selector: 'term-conditions',
  templateUrl: './termConditions.template.html',
  styleUrls: ['../settings.component.scss']
})

export class TermConditionsComponent implements OnInit {

  public text: FormGroup = new FormGroup({});
  public hasData: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private location: Location,
    private api: ApiService,
  ) { }

  ngOnInit() {
    this.getdata()
  }

  goBack() {
    this.location.back();
  }

  save() {
    let body = {
      key: 'term',
      content: this.text.value.text
    }
    if (!this.hasData) {
      this.api.post(body, 'configurations').subscribe(res => {
        this.hasData = true;
      }, error => {

      })
    } else {
      this.api.patch(body, 'configurations', 'term').subscribe(res => {

      }, error => {

      })
    }

  }

  getdata() {
    this.api.get('configurations/term').subscribe(res => {
      this.text.patchValue({ text: res['content'] });
      this.hasData = true;
    })
  }
}