import { Component, HostBinding } from '@angular/core';
import { FormBuilder, Validator, FormGroup, FormControl, Validators } from "@angular/forms";
import { AuthService } from "../../shared/services/auth.service";
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  styleUrls: ['./login.style.scss'],
  templateUrl: './login.template.html'
})
export class LoginComponent {
  @HostBinding('class') classes = 'login-page app';

  loginForm = this.FormBuilder.group({
    email: ['', Validators.email],
    password: ['', Validators.required]
  })
  constructor(
    private FormBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
  ) { }

  onSubmit() {
    document.getElementById('submit').classList.add('show')
    this.authService.login(this.loginForm.value).subscribe(data => {
      document.getElementById('submit').classList.remove('show')
      this.authService.setAuthData(data)
      this.router.navigate(['/app/list/properties'])
    }, error => { 
      document.getElementById('submit').classList.remove('show')
    })

  }
}
