import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../shared/services/auth.service';
import { AlertsService } from "../shared/services/alerts.service";

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {

  constructor(
    private AuthService: AuthService,
    private alert: AlertsService,
  ) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    this.alert.clearMessages()
    if (this.AuthService.getUser().role != 'admin') {
      this.alert.simpleErrorAlert('Your access level does not allow it')
      return false
    } else {
      return true;
    }
  }

}
