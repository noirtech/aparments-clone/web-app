import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from "../shared/services/auth.service";
import { AlertsService } from "../shared/services/alerts.service";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private AuthService: AuthService,
    private alert: AlertsService,
    private router: Router
  ) {

  }

  // canActivate(
  //   next: ActivatedRouteSnapshot,
  //   state: RouterStateSnapshot
  // ) {
  //   this.alert.clearMessages()
  //   if (this.AuthService.checkToken()) {
  //     return true
  //   }
  //   //this.router.navigateByUrl('/login')
  //   //this.alert.simpleErrorAlert('You must login')
  //   return true
  // }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {
    this.alert.clearMessages()
    if (this.AuthService.checkToken()) {
      return true
    }
    this.router.navigateByUrl('/login')
    this.alert.simpleErrorAlert('You must login')
    return false
  }

}
