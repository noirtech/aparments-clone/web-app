import Vue from 'vue'
import { Module } from 'vuex'
import { IRentProperty } from 'apartments-shared'
import { RootState } from './index'
import { convertToLBFilter, InternalFilters } from '~/utils/query'

type CompareProperty = {
  property: IRentProperty,
  isReady: boolean
}
type State = {
  items: Record<number, IRentProperty[]>,
  coordinates: IRentProperty[],
  count: number,
  compareList: CompareProperty[],
  currentPage: number,
  loading: boolean
}

const store: Module<State, RootState> = {
  state: () => ({
    items: {},
    coordinates: [],
    count: 0,
    currentPage: 1,
    compareList: [],
    loading: false
  }),
  mutations: {
    setItems (state, payload: { page: number, properties: IRentProperty[] }) {
      Vue.set(state.items, payload.page, Object.freeze(payload.properties))
    },
    setPage (state, page: number) {
      state.currentPage = page
    },
    resetItems (state) {
      state.items = {}
      state.coordinates = []
    },
    setCount (state, count: number) {
      state.count = count
    },
    setCoordinates (state, coordinates) {
      state.coordinates = Object.freeze(coordinates)
    },
    setLoading (state, value: boolean) {
      state.loading = value
    },
    addCompare (state, value: CompareProperty) {
      state.compareList.push(value)
    },
    removeCompare (state, idx: number) {
      state.compareList.splice(idx, 1)
    }
  },
  getters: {
    getItem (state): (key: string) => IRentProperty | undefined {
      return key => Object.values(state.items).flat().find(({ id }) => id === key)
    },
    getCoordinates (state): IRentProperty[] {
      return state.coordinates
    },
    getItems (state): IRentProperty[] {
      const page = state.currentPage
      return page in state.items ? state.items[page] : []
    },
    getCompareList (state): IRentProperty[] {
      return state.compareList.map(v => v.property)
    },
    inCompareList (state) {
      return (id: string) => !!state.compareList.find(v => v.property.id === id)
    },
    canAddToCompare (state) {
      return state.compareList.length < 4
    },
    isComparing (state): boolean {
      return state.compareList.length > 0
    },
    getAllItems (state): IRentProperty[] {
      return Object.values(state.items).flat()
    }
  },
  actions: {
    fetchItem (_, id: string): Promise<IRentProperty> {
      return this.$axios.$get(`/properties/${id}`)
    },
    addToCompare ({ commit }, property: IRentProperty) {
      commit('addCompare', <CompareProperty>{
        property,
        isReady: true
      })
    },
    async changePage ({ commit, rootState, state }, page) {
      if (!(page in state.items)) {
        commit('setLoading', true)
        const filter = convertToLBFilter(rootState.filters)
        const properties: IRentProperty[] = await this.$axios.$get('/properties', {
          params: {
            filter: {
              limit: 15,
              skip: (page - 1) * 15,
              fields: {
                id: true,
                name: true,
                address: true,
                photos: true,
                summary: true
              },
              ...filter
            }
          }
        })
        commit('setItems', { page, properties })
        commit('setLoading', false)
      }
      commit('setPage', page)
    },
    async fetchItems ({ commit, state }, inQueries: InternalFilters) {
      const filter = convertToLBFilter(inQueries)
      const page = state.currentPage
      commit('setLoading', true)
      const { count } = await this.$axios.$get('/properties/count', { params: { where: filter?.where } })
      commit('setCount', count)

      const properties: IRentProperty[] = await this.$axios.$get('/properties', {
        params: {
          filter: {
            limit: 15,
            skip: (page - 1) * 15,
            fields: {
              id: true,
              name: true,
              address: true,
              cover: true,
              amenities: true,
              schools: true,
              photos: true,
              summary: true
            },
            ...filter
          }
        }
      })

      const coordinates: IRentProperty[] = await this.$axios.$get('/properties', {
        params: {
          filter: {
            limit: 400,
            fields: {
              id: true,
              name: true,
              cover: true,
              address: true
            },
            ...filter
          }
        }
      })

      commit('setPage', page)
      commit('resetItems')
      commit('setItems', { page, properties })
      commit('setCoordinates', coordinates)
      commit('setLoading', false)
    }
  }
}

export const { state, mutations, getters, actions } = store
