import { Module } from 'vuex'
import { IUser, IRentProperty } from 'apartments-shared'
import { RootState } from './index'

type State = {
  user: IUser | null,
  token: string,
  loading: boolean,
  favorites: IRentProperty[]
}

const store: Module<State, RootState> = {
  namespaced: true,
  state: () => ({
    user: null,
    token: '',
    favorites: [],
    loading: false
  }),
  mutations: {
    STORE_TOKEN (state, token: string) {
      state.token = token
    },
    STORE_USER (state, user: IUser | null) {
      state.user = user
    },
    SET_LOADING (state, value: boolean) {
      state.loading = value
    },
    LOGOUT (state) {
      state.user = null
      state.token = ''
      state.loading = false
    },
    ADD_FAVORITE (state, value: IRentProperty) {
      state.favorites.push(value)
    },
    REMOVE_FAVORITE (state, id: string) {
      const idx = state.favorites.findIndex(v => v.id === id)
      state.favorites.splice(idx, 1)
    }
  },
  getters: {
    isLoggedIn (state): boolean {
      return !!state.token
    },
    isLoading (state): boolean {
      return state.loading
    },
    getFavorites (state) {
      return state.favorites
    },
    canAddFavorites (state) {
      return state.favorites.length < 10
    },
    inFavorites (state) {
      return (id: string) => !!state.favorites.find(v => v.id === id)
    }
  },
  actions: {
    async signIn ({ commit }, userData: {email:string, password: string}) {
      commit('SET_LOADING', true)

      try {
        const { token } = await this.$axios.$post('/auth/login', userData)
        // commit('STORE_USER', user)
        commit('STORE_TOKEN', token)
      } finally {
        commit('SET_LOADING', false)
      }
    },
    async signUp ({ commit }, userData) {
      commit('SET_LOADING', true)
      try {
        await this.$axios.$post('/auth/register', userData)
      } finally {
        commit('SET_LOADING', false)
      }
    },
    removeFromFavorite ({ commit }, id: string) {
      commit('REMOVE_FAVORITE', id)
    },
    addToFavorite ({ commit }, property: IRentProperty) {
      commit('ADD_FAVORITE', property)
    }
  }
}

export const { state, mutations, getters, actions } = store
