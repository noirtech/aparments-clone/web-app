import { Module } from 'vuex'
import { InternalFilters } from '~/utils/query'

export type RootState = {
  configuration: Array<{key:string}>,
  filters: InternalFilters
}

const store: Module<RootState, RootState> = {
  state: () => ({
    configuration: [],
    filters: {
      hasAir: undefined,
      hasPool: undefined,
      hasWasher: undefined,
      hasBike: undefined,
      hasCourtyard: undefined,
      hasMaintenance: undefined,
      hasPicnic: undefined,
      hasWiFi: undefined,
      hasUtilsIncluded: undefined,
      minPrice: undefined,
      maxPrice: undefined,
      minBeds: undefined,
      maxBeds: undefined,
      search: undefined,
      minSize: undefined,
      maxSize: undefined,
      minBaths: undefined,
      maxBaths: undefined,
      view: undefined,
      locationComp: undefined,
      mapPolygon: undefined,
      searchType: undefined,
      searchValue: undefined,
      fromCity: undefined
    }
  }),
  mutations: {
    setConfiguration (state, payload: RootState['configuration']) {
      state.configuration.push(...payload)
    },
    updateFilter (state, payload) {
      if (payload.view && Array.isArray(payload.view) && typeof payload.view[0] === 'string') {
        payload = {
          ...payload,
          view: payload.view.map((coord: string) => coord.split(',').map(v => +v))
        }
      }
      state.filters = {
        ...state.filters,
        ...payload
      }
    },
    resetFilter (state) {
      state.filters = {
        hasAir: undefined,
        hasPool: undefined,
        hasWasher: undefined,
        hasBike: undefined,
        hasCourtyard: undefined,
        hasMaintenance: undefined,
        hasPicnic: undefined,
        hasWiFi: undefined,
        hasUtilsIncluded: undefined,
        minPrice: undefined,
        maxPrice: undefined,
        minBeds: undefined,
        search: undefined,
        maxBeds: undefined,
        minSize: undefined,
        maxSize: undefined,
        minBaths: undefined,
        maxBaths: undefined,
        locationComp: undefined,
        view: undefined,
        mapPolygon: undefined,
        searchType: undefined,
        searchValue: undefined,
        fromCity: undefined
      }
    }
  },
  getters: {
    getConfiguration (state) {
      return (key: string) => state.configuration.find(conf => conf.key === key)
    }
  },
  actions: {
    async nuxtServerInit ({ commit }) {
      const configuration = await this.$axios.$get('/configurations')
      commit('setConfiguration', configuration)
    }
  }
}

export const { state, mutations, getters, actions } = store
