
import { inject, computed, Ref, reactive } from '@vue/composition-api'
import { IRentProperty } from 'apartments-shared'
import { mdiHeartRemove, mdiCompare, mdiHeartPlusOutline, mdiHeartOff } from '@mdi/js'
import { AppKeys } from '@/utils'

type PropertySaving = {
  toggle: (property: IRentProperty) => void,
  items: Readonly<Ref<IRentProperty[]>>,
  icon: string,
  canAdd: Readonly<Ref<boolean>>,
  hasItems: Readonly<Ref<boolean>>,
  inList: Readonly<Ref<boolean>>
}

export function useComparison(id: string): PropertySaving
export function useComparison(): Omit<PropertySaving, 'inList'>
export function useComparison (id?: string): any {
  const context = inject(AppKeys.Context)!
  if (!context) { return }
  const { root: { $store } } = context

  const canAdd = computed(() => $store.getters['properties/canAddToCompare'])
  const inList = computed(() => $store.getters['properties/inCompareList'](id))
  const items = computed<IRentProperty[]>(() => $store.getters['properties/getCompareList'])
  const hasItems = computed(() => items.value.length > 0)

  const toggle = (property?: IRentProperty) => {
    if (!property) {
      if (inList.value) {
        $store.commit('properties/removeCompare', items.value.findIndex(v => v.id === id))
      } else if (canAdd.value) {
        $store.dispatch('properties/addToCompare', property)
      }
      return
    }
    const isComparing = $store.getters['properties/inCompareList'](property.id)
    if (isComparing) {
      $store.commit('properties/removeCompare', items.value.findIndex(v => v.id === property.id))
    } else if (canAdd.value) {
      $store.dispatch('properties/addToCompare', property)
    }
  }
  if (id) {
    return reactive({
      toggle,
      canAdd,
      icon: mdiCompare,
      inList,
      hasItems,
      items
    })
  }
  return reactive({
    toggle,
    items,
    canAdd,
    hasItems,
    icon: mdiCompare
  })
}

export function useFavorite(id: string): PropertySaving
export function useFavorite(): Omit<PropertySaving, 'inList' | 'toggle'>
export function useFavorite (id?: string): any {
  const context = inject(AppKeys.Context)!
  if (!context) { return }
  const { root: { $store } } = context

  const canAdd = computed(() => $store.getters['auth/canAddFavorites'])
  const inList = computed(() => $store.getters['auth/inFavorites'](id))
  const icon = computed(() =>
    inList.value ? mdiHeartRemove : canAdd.value ? mdiHeartPlusOutline : mdiHeartOff)

  const items = computed<IRentProperty[]>(() => $store.getters['auth/getFavorites'])
  const hasItems = computed(() => items.value.length > 0)

  const toggle = (property: IRentProperty) => {
    if (inList.value) {
      $store.dispatch('auth/removeFromFavorite', id)
    } else if (canAdd.value) {
      $store.dispatch('auth/addToFavorite', property)
    }
  }
  if (id) {
    return reactive({
      toggle,
      canAdd,
      icon,
      inList,
      hasItems,
      items
    })
  }
  return reactive({
    canAdd,
    icon,
    hasItems,
    items
  })
}
