import Vue from 'vue'

import { ref, onMounted, watch, SetupContext, Ref } from '@vue/composition-api'
import { Loader, LoaderOptions, google as Google } from 'google-maps'
import { debounce } from 'ts-debounce'
import { IRentProperty } from 'apartments-shared'

export type Map = google.maps.Map
export type AutocompletePrediction = google.maps.places.AutocompletePrediction

const options: LoaderOptions = {
  language: 'en',
  libraries: ['places']
}
let loader: Loader
if (process.client) {
  loader = new Loader('AIzaSyAhRJu8PMeIu87WwsKb9baMPTly5DqpERo', options)
}

export type MapInstance = { google: Google['maps'], map: Map }

export function useGoogle (init: (maps: Google['maps']) => Promise<void> | void) {
  if (process.server) { return }
  return loader.load().then((google) => {
    return init({ ...google.maps })
  })
}

export function useGMap (opts?: google.maps.MapOptions) {
  const node = ref<HTMLElement>(null)
  const map = ref<Map>(null)
  const initialized = ref<boolean>(false)
  const google = ref<Google>(null)
  type InitFunction = ({ map: Map }) => void
  let init: InitFunction
  onMounted(async () => {
    await Vue.nextTick()
    google.value = Object.freeze(await loader.load())
    map.value = new google.value.maps.Map(node.value!, opts)
    initialized.value = true
    init(map.value)
  })

  const onMapInit = (initFunction: InitFunction) => {
    init = initFunction
  }

  return {
    node,
    map,
    initialized,
    onMapInit,
    google
  }
}

export function useDraw (google: Ref<Google | null>, map: Ref<Map | null>) {
  const isDrawing = ref(false)
  const polygon = ref<google.maps.Polygon>(null)

  watch([isDrawing, google], () => {
    if (!google.value || !map.value) { return }
    map.value.setOptions({
      draggable: false,
      zoomControl: false,
      scrollwheel: false,
      disableDoubleClickZoom: false
    })
    if (!isDrawing.value) {
      map.value!.setOptions({
        draggable: true,
        scrollwheel: true,
        disableDoubleClickZoom: true
      })
      google.value.maps.event.clearListeners(map.value.getDiv(), 'mousedown')
      return
    }

    google.value.maps.event.addDomListenerOnce(map.value.getDiv(), 'mousedown', () => {
      const poly = new google.value!.maps.Polyline({ map: map.value!, clickable: false })
      const move = map.value!.addListener('mousemove', (e) => {
        poly.getPath().push(e.latLng)
      })

      google.value!.maps.event.addListenerOnce(map.value!, 'mouseup', () => {
        google.value!.maps.event.removeListener(move)
        const paths = poly.getPath().getArray()
        paths.push(paths[0])
        poly.setMap(null)
        polygon.value = new google.value!.maps.Polygon({ map: map.value!, paths, fillOpacity: 0.2 })
        map.value!.setOptions({
          draggable: true,
          scrollwheel: true,
          disableDoubleClickZoom: true
        })
      })
    })
  }, { lazy: true })

  return { isDrawing, polygon }
}

let sessionToken: google.maps.places.AutocompleteSessionToken

export function useAutocomplete ({ root: { $axios, $router, $store } }: SetupContext) {
  type Result = {
    name: string
    placeId: string
  } | {
    name: string
    id: string
  }

  const items = ref<Result[]>([])
  const search = ref('')
  const isLoading = ref(false)
  let geocode: (req: google.maps.GeocoderRequest) => Promise<google.maps.GeocoderResult[]>

  if (process.client) {
    useGoogle((google) => {
      const service = new google.places.AutocompleteService()
      const geocoder = new google.Geocoder()
      geocode = req => new Promise<google.maps.GeocoderResult[]>(resolve => geocoder.geocode(req, resolve))
      function getPlacePredictions (options: google.maps.places.AutocompletionRequest) {
        return new Promise<google.maps.places.AutocompletePrediction[]>(resolve => service.getPlacePredictions(options, resolve))
      }

      if (!sessionToken) {
        sessionToken = new google.places.AutocompleteSessionToken()
      }

      watch(search, debounce(async () => {
        if (!search.value || search.value.length < 2) { return }

        isLoading.value = true

        const bounds = new google.LatLngBounds(
          new google.LatLng(25.83706, 36.5004529),
          new google.LatLng(-106.6458459, -93.5078217)
        )

        const placeResults = await getPlacePredictions({
          sessionToken,
          input: search.value,
          componentRestrictions: { country: 'us' },
          bounds,
          types: ['geocode']
        })

        items.value = placeResults
          ?.filter(v => v.terms.some(t => t.value === 'TX'))
          ?.map(v => ({ name: v.description.replace(', USA', ''), placeId: v.place_id })) ?? []
        let where = {}

        if (search.value.match(/^[0-9]{5}(-[0-9]{4})?$/)) {
          where = {
            'address.zipCode': search.value
          }
        } else {
          where = {
            name: {
              like: search.value.split('').join('.*'),
              options: 'i'
            }
          }
        }
        const rentsResults: IRentProperty[] = await $axios.$get('/properties', {
          params: {
            filter: {
              limit: 3,
              where,
              fields: {
                id: true,
                name: true
              },
              order: [
                'priority DESC',
                'createdAt DESC'
              ]
            }
          }
        })
        items.value.push(...rentsResults.map(v => ({ name: v.name, id: v.id })))

        isLoading.value = false
      }, 800), { lazy: true })
    })
  }
  async function setSearch (selected: Result) {
    if ('id' in selected) {
      await $router.push({ name: 'apartment-id', params: { id: selected.id } })
    } else {
      // A place was selected
      selected = {
        placeId: 'ChIJS5dFe_cZTIYRj2dH9qSb7Lk',
        name: 'Dallas, TX',
        ...selected
      }
      const result = await geocode({ placeId: selected.placeId })
      const exclude = [
        'administrative_area_level_2',
        'country'
      ]
      const components = result[0].address_components
        .filter(a => !exclude.some(e => a.types.includes(e)))
        .sort((a, _) => a.types.includes('postal_code') ? -1 : 0)
        .map(a => a.long_name)
        .reverse()

      $store.commit('updateFilter', {
        search: selected.name,
        locationComp: components.map(component => component.toLowerCase()),
        view: null
      })
    }
  }
  return { items, search, isLoading, setSearch }
}
// export function installDrawing (ob: MapInstance) {

// }
