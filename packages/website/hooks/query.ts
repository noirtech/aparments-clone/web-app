import { onUnmounted, computed, SetupContext, watch, onMounted } from '@vue/composition-api'
import pickBy from 'lodash.pickby'
import { debounce } from 'ts-debounce'
import { IRentProperty } from 'apartments-shared'

import { InternalFilters } from '@/utils/query'

let initialize = false
export function useQuery ({ root: { $store, $router, $route } }: SetupContext) {
  const filters = computed<InternalFilters>(() => $store.state.filters)

  function setFilter<T extends keyof InternalFilters> (key: T, value: InternalFilters[T], options = { reset: false }) {
    if (options.reset) {
      $store.commit('resetFilter')
    }
    $store.commit('updateFilter', { [key]: value })
  }

  function resetFilters () {
    $store.commit('resetFilter')
  }

  type Filt<T extends keyof InternalFilters = any> = { key: T, value: InternalFilters[T], options?: { reset: boolean }}

  function setFilters (filters: Filt[]) {
    filters.map(v => setFilter(v.key, v.value, v.options))
  }

  const properties = computed<IRentProperty[]>(() => $store.getters['properties/getItems'])
  const coordinates = computed<IRentProperty[]>(() => $store.getters['properties/getCoordinates'])
  const location = computed(() => {
    const { locationComp, ...rest } = filters.value
    return {
      path: '/apartments/' + locationComp?.join('/').replace(/[ ]/, '-').toLowerCase(),
      query: {
        ...JSON.parse(JSON.stringify(pickBy(rest)))
      }
    }
  })
  if (initialize) { return { filters, location, properties, setFilter, setFilters: debounce(setFilters, 1000), coordinates, resetFilters } }
  initialize = true

  watch(() => JSON.stringify(location.value), async () => {
    await $router.push(location.value)
  }, { deep: true, lazy: true })
  onMounted(() => {
    const pathMatch = $route.params.pathMatch
    if (!pathMatch) { return }
    const filters = { locationComp: pathMatch.split('/') }
    if (Object.keys($route.query).length) {
      Object.assign(filters, $route.query)
    }
    if (Object.keys(filters).length) {
      $store.commit('updateFilter', filters)
    }
  })

  onUnmounted(() => {
    initialize = false
  })

  return { filters, properties, setFilter, location, setFilters: debounce(setFilters, 1000), coordinates, resetFilters }
}
