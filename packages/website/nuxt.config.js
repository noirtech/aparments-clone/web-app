import colors from 'vuetify/es5/util/colors'

const isDev = process.env.NODE_ENV !== 'production'

export default {
  mode: 'universal',
  server: {
    host: '0.0.0.0',
    port: 3001,
    timing: {
      total: true
    }
  },
  /*
   ** Headers of the page
   */
  head: {
    title: 'FindYourNextApartment',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, minimum-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: 'Saving Thousands of Dollars on Apartment Rents'
      },
      { name: 'mobile-web-app-capable', content: 'yes' }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
    script: [
      { src: 'https://widget.manychat.com/103335811135106.js', async: true, defer: true }
    ]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#FFF' },
  /*
   ** Global CSS
   */
  css: [],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: ['@/plugins/composition-api'],
  /*
   ** Nuxt.js dev-modules
   */
  build: {
    extractCSS: true,
    /**
       * @param {{ devtool: string; }} config
       * @param {{ isDev: boolean; isClient: boolean; }} ctx
       */
    extend (config, ctx) {
      if (ctx.isDev) {
        config.devtool = ctx.isClient ? 'eval-cheap-module-source-map' : 'inline-cheap-module-source-map'
      }
    }
  },
  modern: !isDev,
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    '@nuxtjs/vuetify',
    '@aceforth/nuxt-optimized-images',
    '@nuxtjs/google-analytics',
    ['@nuxt/typescript-build', {
      typeCheck: !isDev
    }]
  ],
  googleAnalytics: {
    id: 'UA-164477952-1'
  },
  optimizedImages: {
    optimizeImages: true
  },
  pwa: {
    meta: {
      name: 'Find Your Next Apartment',
      description: 'Saving Thousands of Dollars on Apartment Rents'
    },
    manifest: {
      name: 'Find Your Next Apartment',
      lang: 'en'
    },
    icon: {
      iconSrc: './assets/img/brand-icon.png'
    }
  },
  workbox: {
    runtimeCaching: [
      {
        urlPattern: 'https://api.findyournextapartment.com/.*',
        handler: 'networkFirst',
        strategyOptions: {
          cacheName: 'api-cache',
          cacheExpiration: {
            maxEntries: 1000,
            maxAgeSeconds: 60 * 60 * 24
          }
        }
      },
      {
        urlPattern: 'https://images.findyournextapartment.com/.*',
        handler: 'cacheFirst',
        strategyOptions: {
          cacheName: 'image-cache',
          cacheExpiration: {
            maxEntries: 300,
            maxAgeSeconds: 360000
          }
        }
      }
    ]
  },
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    'nuxt-webfontloader',
    '@nuxtjs/pwa'
  ],
  webfontloader: {
    google: {
      families: ['Poppins:n1,i4,n4,n5,n7']
    }
  },
  watchers: {
    chokidar: {
      usePolling: true,
      interval: 1000,
      depth: 4
    }
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    host: 'api.findyournextapartment.com',
    port: 443,
    browserBaseURL: 'https://api.findyournextapartment.com/',
    baseURL: 'https://api.findyournextapartment.com/'
  },
  /*
   ** vuetify module configuration
   ** https://github.com/nuxt-community/vuetify-module
   */
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    treeShake: true,
    defaultAssets: {
      icons: 'mdiSvg',
      font: {
        family: 'Poppins'
      }
    },
    theme: {
      options: {
        customProperties: true
      },
      themes: {
        light: {
          primary: '#adb8b5',
          accent: '#f9754f',
          shades: '#eef1f4',
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        },
        dark: {
          primary: '#dd8f62',
          accent: '#f9754f',
          shades: '#334851',
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  }
}
