import { InjectionKey, SetupContext } from '@vue/composition-api'
import { NuxtAxiosInstance } from '@nuxtjs/axios'

export namespace AppKeys {
  export const Axios: InjectionKey<NuxtAxiosInstance> = Symbol('AppKeys.Axios')
  export const Context: InjectionKey<SetupContext> = Symbol('AppKeys.Context')
}
