import { IRentProperty, IFloorPlan } from 'apartments-shared'
type Stringify<T, R extends keyof T> = Omit<T, R> & Record<keyof Pick<T, R>, string>
export const numberFormater = new Intl.NumberFormat().format

type Summaries = {
  rooms: string
  prices: string
  sizes: string
  count: string
}
export function formatRangePrice ({ summary }: IRentProperty): string | null {
  if (!summary?.floorPlans || !Object.keys(summary.floorPlans).length) { return '' }
  const bedOpts = Object.keys(summary.floorPlans).map(Number)

  const minB = Math.min(...bedOpts)
  const maxB = Math.max(...bedOpts)

  const min = summary.floorPlans[minB].priceRanges[0] ?? null
  const max = summary.floorPlans[maxB].priceRanges[1] ?? null

  if (max === 0) { return 'Call for rent' }
  if (min === max) { return '$' + String(min) }
  if (!min || !max) { return null }
  return `$${numberFormater(min)} - $${numberFormater(max)}`
}

export function formatRangeBeds ({ summary }: IRentProperty): string | null {
  if (!summary?.floorPlans || !Object.keys(summary.floorPlans).length) { return '' }
  const bedOpts = Object.keys(summary.floorPlans).map(Number)

  const min = Math.min(...bedOpts) || 'Studio'
  const max = Math.max(...bedOpts) || 'Studio'
  return min === max ? String(min) : [min, max].join(' - ')
}
export function formatSummary ({ summary }: IRentProperty): Summaries[] {
  if (!summary || Object.keys(summary.floorPlans).length === 0) { return [] }
  const summaries: Summaries[] = []
  const summs = Object.entries(summary.floorPlans)
  let idx = 0
  for (const [nBeds, fpAg] of summs) {
    if (idx < 3) {
      idx++
    } else {
      break
    }
    const bed = nBeds === '0' ? 'Studio' : nBeds + ' Bed' + (+nBeds > 1 ? 's' : '')
    let bath = fpAg.bathRanges.join('-') + ' Baths'
    if (fpAg.bathRanges[0] === fpAg.bathRanges[1]) {
      if (fpAg.bathRanges[0] !== 0) {
        bath = String(fpAg.bathRanges[0])
        bath += fpAg.bathRanges[0] > 1 ? ' Baths' : ' Bath'
      }
    }

    let sizes = fpAg.sizeRanges.map(numberFormater).join('-') + ' Sqft'
    if (fpAg.sizeRanges[0] === fpAg.sizeRanges[1]) {
      sizes = fpAg.sizeRanges[0] + ' Sqft'
    }

    let prices = '$' + fpAg.priceRanges.join('-')
    if (fpAg.priceRanges[0] === fpAg.priceRanges[1]) {
      if (fpAg.priceRanges[0] !== 0) {
        prices = '$' + fpAg.priceRanges[0]
      } else {
        prices = 'Contact for Price'
      }
    } else if (fpAg.priceRanges[0] === 0) {
      prices = '$' + fpAg.priceRanges[1]
    }
    summaries.push({
      count: fpAg.count + ' Floor Plan' + (fpAg.count > 1 ? 's' : ''),
      rooms: bed + (bath !== '' ? ' | ' + bath : ''),
      prices,
      sizes
    })
  }
  return summaries
}

export function formatAvailable ({ summary }: IRentProperty) {
  return convertFromKebab(summary?.minAvailability ?? 'available-now')
}
export function convertFromKebab (text: string) {
  return text.split('-').map(w => w.charAt(0).toUpperCase() + w.slice(1)).join(' ')
}
export function formatFullAddress ({ address }: IRentProperty) {
  return address.formatted || ''
}
function pluralize (val: number, word: string) {
  if (val === 1) { return val + ' ' + word } else { return val + ' ' + word + 's' }
}
export function formatFloorPlan (property: IRentProperty): Stringify<IFloorPlan, 'bathrooms' | 'price' | 'bedrooms'>[] {
  const { floorPlans } = property
  return floorPlans.map(({ bedrooms, bathrooms, availability, price, ...rest }) => {
    const prices = price[0] === price[1] ? `$${price[0]}` : `$${price[0]} - $${price[1]}`
    return {
      bedrooms: bedrooms ? pluralize(bedrooms, 'Bed') : 'Studio',
      bathrooms: pluralize(bathrooms, 'Bath'),
      availability: convertFromKebab(availability),
      price: prices,
      ...rest
    }
  })
}
export function convertTo12Hours (time: string): string {
  // Check correct time format and split into components
  let timeArr = time.match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time]

  if (timeArr.length > 1) { // If timeArr format correct
    timeArr = timeArr.slice(1) // Remove full string match value
    timeArr[3] = +timeArr[0] < 12 ? 'AM' : 'PM' // Set AM/PM
    timeArr[0] = String(+timeArr[0] % 12 || 12) // Adjust hours
  }
  return timeArr.join('') // return adjusted time or original string
}
