import { inject, SetupContext } from '@vue/composition-api'
import { AppKeys } from './keys'

export const onAsyncData = (fn: SetupContext['root']['$options']['asyncData']) => {
  const context = inject(AppKeys.Context)
  if (!context) {
    throw new Error('Context not injected')
  }
  context.root.$options.asyncData = fn
}

export const onFetch = (fn: SetupContext['root']['$options']['fetch']) => {
  const context = inject(AppKeys.Context)
  if (!context) {
    throw new Error('Context not injected')
  }
  context.root.$options.fetch = fn
}
