import { IRentProperty, Where, Filter } from 'apartments-shared'
import pickBy from 'lodash.pickby'
import { polygon } from '@turf/helpers'
import cleanCords from '@turf/clean-coords'
import simplify from '@turf/simplify'
import truncate from '@turf/truncate'
import flip from '@turf/flip'

import unkink from '@turf/unkink-polygon'
import combine from '@turf/combine'

export type CheckFilters =
  | 'hasAir'
  | 'hasPool'
  | 'hasWasher'
  | 'hasUtilsIncluded'
  | 'hasWiFi'
  | 'hasMaintenance'
  | 'hasCourtyard'
  | 'hasPicnic'
  | 'hasBike'

export const CHECK_FILTERS = [
  'hasAir',
  'hasPool',
  'hasWasher',
  'hasUtilsIncluded'
]
export type PriceFilters =
  | 'minPrice'
  | 'maxPrice'

export type BedsFilters =
  | 'minBeds'
  | 'maxBeds'
export type SizeFilters =
  | 'minSize'
  | 'maxSize'
export type BathsFilters =
  | 'minBaths'
  | 'maxBaths'
export type AvailableFilters =
  | CheckFilters
  | PriceFilters
  | BedsFilters
  | SizeFilters
  | BathsFilters
  | 'fromCity'
  | 'mapPolygon'
  | 'search'
  | 'view'
  | 'locationComp'

export type InternalFilters = {
  hasAir: boolean | undefined
  hasPool: boolean | undefined
  hasWasher: boolean | undefined
  hasUtilsIncluded: boolean | undefined
  hasWiFi: boolean | undefined
  hasMaintenance: boolean | undefined
  hasCourtyard: boolean | undefined
  hasPicnic: boolean | undefined
  hasBike: boolean | undefined
  minPrice: number | undefined
  maxPrice: number | undefined
  minBeds: number | undefined
  maxBeds: number | undefined
  minSize: number | undefined
  maxSize: number | undefined
  minBaths: number | undefined
  maxBaths: number | undefined
  search: string | undefined
  searchValue: string | undefined
  searchType: string[] | undefined
  mapPolygon: [number, number][][] | undefined
  fromCity: string | undefined

  view: [number, number][] | undefined
  locationComp: string[] | undefined
}
export const buildFilter = {
  locationComp: (components: string[]) => ({
    and: [
      ...components.map(component => ({
        'address.components.name': {
          like: component.replace(/[-]/g, ' ') + '$',
          options: 'i'
        }
      }))
    ]
  }),
  view: (bound: [number, number][]) => ({
    'address.location': {
      geoWithin: {
        $box: bound.map(([lat, lng]) => [lng, lat])
      }
    }
  }),
  hasAir: () => ({ 'amenities.name': 'air-conditioning' }),
  hasPool: () => ({ 'amenities.name': 'pool' }),
  hasWasher: () => ({ 'amenities.name': 'washerdryer-hookup' }),
  hasUtilsIncluded: () => ({ 'expenses.type': 'utilities-included' }),
  hasWiFi: () => ({ 'amenities.name': 'wi-fi-at-pool-and-clubhouse' }),
  hasMaintenance: () => ({ 'amenities.name': 'maintenance-on-site' }),
  hasCourtyard: () => ({ 'amenities.name': 'courtyard' }),
  hasPicnic: () => ({ 'amenities.name': 'picnic-area' }),
  hasBike: () => ({ 'amenities.name': 'bike-storage' }),
  fromCity: (city: string) => ({ 'address.city': city }),
  minPrice: (gte: number) => ({ 'floorPlans.price': { gte } }),
  maxPrice: (lte: number) => ({ 'floorPlans.price': { lte } }),
  minBeds: (gte: number) => ({ 'floorPlans.bedrooms': { gte } }),
  maxBeds: (lte: number) => ({ 'floorPlans.bedrooms': { lte } }),
  minSize: (gte: number) => ({ 'floorPlans.size': { gte } }),
  maxSize: (lte: number) => ({ 'floorPlans.size': { lte } }),
  minBaths: (gte: number) => ({ 'floorPlans.bathrooms': { gte } }),
  maxBaths: (lte: number) => ({ 'floorPlans.bathrooms': { lte } }),
  search: () => ({}),
  searchValue: (value: string) => ({
    'address.components.name': value
  }),
  searchType: (types: string[]) => ({
    or: types.map(type => ({ 'address.components.types': type }))
  }),
  mapPolygon: (latLngs: [number, number][][]) => {
    const poly = polygon(latLngs)
    const cleaned = cleanCords(poly, { mutate: true })
    const simplified = simplify(cleaned, { mutate: true, highQuality: true, tolerance: 0.005 })
    const truncated = truncate(simplified, { mutate: true, precision: 6 })
    const fliped = flip(truncated, { mutate: true })
    const feature = combine(unkink(fliped))

    return {
      'address.location': {
        geoWithin: {
          $geometry: feature.features[0].geometry as any
        }
      }
    }
  }
}
export type InternalQueries = Partial<Record<AvailableFilters, Where<IRentProperty>>>

export function convertToLBFilter (obj: InternalFilters): Filter<IRentProperty> {
  type Entries<T> = [keyof T, T[keyof T]][]

  const cleanFilters = Object.entries(pickBy(obj)) as Entries<Required<InternalFilters>>
  if (cleanFilters.length === 0) { return {} }
  return {
    where: {
      and: cleanFilters.map(([key, value]) => {
        const fn = buildFilter[key] as any
        return fn(value)
      })
    },
    order: ['priority DESC']
  }
}
