# 0.5.0
- Close More filter when clear button is pressed
- Change in app bar Rewards to Reward
- Make bigger sign in form
- Change to orange social sign in buttons
- Add "Or" word to search
- Remove phone validation in connect with an expert
- Reduce icon space in property detail
- Connect with expert now send to home

# 0.4.0
## Features
- Add favorite button
- Add Sign in button
- Add Register button

## Fix
- School with rating 0 or null
- Missing images in properties
- Property card phone button
- Now when is loading isn't showed anymore for one second the bar options (referral, my favorites)
- Micro scroll in google chrome for mobile in the navbar

## Visual improvements
- Change color of app bar to white looking more clear
- Logo is displayed always (isn't not loaded anymore)
- Main cities now show a placeholder
- Home icons show a loader
- Changes as demo
- Font family changed to Poppins
- Change property detail page to match the new style
- Stick property detail bar so the user can mark as favorite a property anywhere
- Reduce margin in home page
- About image in mobile phone it's more responsive
- Reduce contact form size
- Search input in listing as box