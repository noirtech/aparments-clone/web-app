require('dotenv').config()

import { filter } from './packages/zipcode'
import { generateUrl } from './packages/utils'
import { scraper } from './packages/scraper'
import { main } from 'apify'

async function mainCities(): Promise<string[]> {
  const ZIP_CODES = await filter(
    [
      { lat: 32.779167, lng: -96.808891 }, // Dallas
      { lat: 29.749907, lng: -95.358421 }, // Houston
      { lat: 30.2575, lng: -97.7374 }, // Austin
      { lat: 29.424349, lng: -98.491142 }, // San Antonio
      { lat: 39.742043, lng: -104.991531 }, // Denver
      { lat: 33.753746, lng: -84.38633 } // Atlanta
    ],
    128748
  )
  return generateUrl(ZIP_CODES)
}

main(async () => {
  await scraper(await mainCities())
})
