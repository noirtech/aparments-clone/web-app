/* eslint-disable @typescript-eslint/camelcase */
import csv from 'csv-parser'
import { createReadStream } from 'fs'
import { LatLng, distanceTo } from 'geolocation-utils'
import unzip from 'unzip-stream'

export type ZipCode = {
  zip: number
  location: LatLng
  city: string
  state: string
}

export function loadZipCodes(): Promise<Array<ZipCode>> {
  return new Promise(resolve => {
    const result: Array<ZipCode> = []

    createReadStream('./assets/uszips.zip')
      .pipe(unzip.Parse())
      .on('entry', function(entry) {
        entry
          .pipe(csv())
          .on('data', ({ zip, lat, lng, city, state_id }) => {
            result.push({
              zip: Number(zip),
              location: { lat: Number(lat), lng: Number(lng) },
              city,
              state: state_id
            })
          })
          .on('end', () => {
            resolve(result)
          })
      })
  })
}

export async function filter(
  coords: LatLng[],
  radius: number
): Promise<ZipCode[]> {
  const data = await loadZipCodes()

  return data.filter(({ location }) =>
    coords.some(coord => distanceTo(coord, location) <= radius)
  )
}
