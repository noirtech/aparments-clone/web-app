import {
  IExpense,
  IAmenity,
  IOfficeHour,
  IPhoto,
  ISchool,
  INearby,
  IGeoLocation,
  IFloorPlan,
  IRentProperty,
  IRentAggregate
} from 'apartments-shared'
import {
  Media,
  PROPERTY_REQUEST,
  IRentPropertyBuilder
} from '../types'
import {
  pipe,
  Observable,
  UnaryFunction,
  of,
  from,
  combineLatest
} from 'rxjs'
import { mergeMap, map } from 'rxjs/operators'
import { Request } from 'apify'
import { groupBy, minBy, maxBy } from 'lodash'

type ParseMedia = UnaryFunction<Observable<Media>, Observable<Partial<IPhoto>>>

export const parseMedia: ParseMedia = map(media => ({
  src: media.Uri,
  description: media.Caption || '',
  altText: media.AltText || '',
}))

export const parseListing: () => UnaryFunction<
  Observable<CheerioStatic>,
  Observable<[CheerioStatic, CheerioElement]>
> = () => pipe(
  mergeMap($ =>
    combineLatest([
      of($),
      from($('#placardContainer > ul > li > article').toArray())
    ])
  )
)

function calculateSummary(property: Pick<IRentProperty, 'floorPlans'>) {
  const summary: IRentAggregate = {
    floorPlans: {},
    minAvailability: 'available-now'
  }

  if (!property.floorPlans.length) {
    return summary
  }

  const grouped = groupBy(property.floorPlans, 'bedrooms')
  for (const [key, floorPlans] of Object.entries(grouped)) {
    const minBath = minBy(floorPlans, 'bathrooms')?.bathrooms
    const maxBath = maxBy(floorPlans, 'bathrooms')?.bathrooms

    const minPrice = minBy(floorPlans, fp => fp.price[0])?.price[0]
    const maxPrice = maxBy(floorPlans, fp => fp.price[1])?.price[1]

    const minSize = minBy(floorPlans, 'size')?.size
    const maxSize = maxBy(floorPlans, 'size')?.size

    summary.floorPlans[+key] = {
      count: floorPlans.length,
      bathRanges: [minBath ?? maxBath ?? 0, maxBath ?? minBath ?? 0],
      sizeRanges: [minSize ?? maxSize ?? 0, maxSize ?? minSize ?? 0],
      priceRanges: [minPrice ?? maxPrice ?? 0, maxPrice ?? minPrice ?? 0]
    }
  }
  return summary
}

export function parseProperty(
  $: CheerioStatic,
  request: Request<PROPERTY_REQUEST>
): IRentPropertyBuilder {
  function getText(sel: string, el?: CheerioElement): string {
    const element = !!el ? $(el).find(sel) : $(sel)
    return element.text().trim()
  }
  function castNumber(text: string, pos = 0) {
    const result = text
      .replace(/[,]/g, '')
      .match(/\d+/g)
    return result ? Number(result[pos]) : 0
  }
  function getNumber(sel: string, el?: CheerioElement, pos = 0): number {
    const element = !!el ? $(el).find(sel) : $(sel)
    return castNumber(element.text(), pos)
  }

  const id = $('main').attr('data-listingid') as string
  const floorPlanSel = '#availabilitySection > div[data-tab-content-id=all] > .pricingGridItem'
  const floorPlans: Array<IFloorPlan & {
    originAttId: number
    originId: string
  }> = []
  $(floorPlanSel)
    .each((_, el) => {
      const price: [number, number] = [
        getNumber('.rentLabel', el, 0),
        getNumber('.rentLabel', el, 1) ?? getNumber('.rentLabel', el, 0)
      ]
      const originAttId = $(el).find('#priceGridModelWrapper').attr('data-attachmentid')?.match(/\d+/)?.[0]
      floorPlans.push({
        bedrooms: getNumber('span.detailsTextWrapper > span:nth-child(1)', el),
        bathrooms: getNumber('span.detailsTextWrapper > span:nth-child(2)', el),
        originAttId: Number(originAttId),
        originId: $(el).find('#priceGridModelWrapper').attr('data-rentalkey') || '',
        photos: [],
        name: getText('h3.modelLabel > span.modelName', el),
        unit: '',
        availability: getText('li.unitContainer:nth-child(1) .dateAvailable', el)
          .toLowerCase()
          .replace('availibility', '')
          .trim()
          .replace(/[ ]/g, '-')
          .replace(/[\.]/g, ''),
        size: getNumber('span.detailsTextWrapper > span:nth-child(3)', el),
        deposit: getNumber('.leaseDepositLabel', el),
        price
      })
    })

  const expensesSel = '.feesPoliciesCard .component-frame'
  const expenses: IExpense[] = []
  $(expensesSel).each((idx, el) => {
    $(el)
      .find('.component-row')
      .each((idx, ch) => {
        const name = getText('.column', ch)
          .toLowerCase()
          .replace(/[\.]/g, '')
        const price = getText('.column-right', ch)
        if (price.trim() === '' && name.trim() === '') return
        expenses.push({
          type: getText('.component-header .header-column', el)
            .toLowerCase()
            .replace(/[\.]/g, '')
            .replace(/[ ]/g, '-') as IExpense['type'],
          name,
          price
        })
      })
  })

  const schools: ISchool[] = []
  $('#schoolsModule .tabContent').each((idx, el) => {
    $(el)
      .find('.schoolCard')
      .each((idx, ch) => {
        const category = $(el).attr('data-tab-content-id')
        schools.push({
          grades: getText('.details .grades', ch).slice(7),
          type: getText('.schoolType', ch),
          name: getText('.schoolName a', ch),
          rating: +(
            $(ch)
              ?.find('.iconContainer .schoolRating i')
              ?.attr('class')
              ?.match(/\d+/)?.[0] ?? 0
          ),
          category:
            category === 'private' || category === 'public'
              ? category
              : 'public',
          students: getNumber('.numberOfStudents', ch),
          phone: getText('.schoolPhone', ch)
        })
      })
  })

  const nearby: INearby[] = []
  $('#pointsofInterestSection .transportationDetail table').each((idx, el) => {
    $(el)
      .find('tbody > tr')
      .each((idx, ch) => {
        nearby.push({
          category: getText('.longLabel tr th:nth-child(1)', el)
            .toLowerCase()
            .replace(/[ ]/g, '-'),
          name: getText('td:nth-child(1)', ch),
          driving: getText('td:nth-child(2)', ch),
          distance: getText('td:nth-child(3)', ch)
        })
      })
  })

  const amenities: IAmenity[] = []
  $('#descriptionSection .specGroup .specInfo.uniqueAmenity')
    .each((_, el) => {
      const category = 'unique-features'
      amenities.push({
        category,
        name: 'custom',
        title: $(el)
          .text()
          .trim()
      })
    })
  let category = ''
  $('#amenitiesSection > *')
    .find('.sectionTitle, .spec')
    .each((_, el) => {
      if (el.tagName == 'h2') {
        category = $(el).text().toLowerCase().replace(/[ ]/g, '-')
        return
      }
      $(el).find('.specInfo').map((_, iEl) => {
        amenities.push({
          category,
          name: 'custom',
          title: $(iEl)
            .text()
            .trim()
        })
      })
    })

  const script = $('script[type="application/ld+json"]').html()
  let officeHours: IOfficeHour[] = []
  try {
    if (script) {
      if (script) {
        const scriptJson = JSON.parse(script)
        const ohs = scriptJson['mainEntity']?.[0]['openingHoursSpecification']
        officeHours =
          ohs?.map(({ dayOfWeek, opens, closes }) => ({
            dayOfWeek,
            opens,
            closes
          })) ?? []
      }
    }
  } catch (error) { }

  const coordinates: [number, number] = [
    Number($('meta[property="place:location:longitude"]').attr('content')),
    Number($('meta[property="place:location:latitude"]').attr('content'))
  ]
  const scam = $('scamUrl').length > 0
  const verified = $('.verifedText').length > 0
  const priority =
    'priority' in request.userData ? request.userData.priority : 0
  const property: IRentPropertyBuilder = {
    originId: id,
    name: getText('h1.propertyName'),
    origin: request.url,
    description: getText('.descriptionSection > p'),
    phone: castNumber($('.phoneNumber > a').attr('href') ?? ''),
    website: $('.container .mortar-wrapper a').attr('href'),
    floorPlans,
    amenities,
    expenses,
    cover: '',
    scam,
    verified,
    nearby,
    priority,
    schools,
    photos: [],
    languages:
      $('#officeHoursSection .languages')
        .text()
        .trim()
        .slice(10)
        .match(/([A-Z])\w+/g) || [],
    officeHours,
    address: {
      location: {
        type: 'Point',
        coordinates
      }
    } as IGeoLocation
  }
  property.summary = calculateSummary(property)
  return property
}
