import { RequestQueue } from 'apify'
import { ApifyContext, UserData } from '../types'

import { of, merge, throwError } from 'rxjs'
import {
  addressHandler,
  listingHandler,
  pageLinkHandler,
  photosHandler,
  propertyHandler,
  resourceHandler
} from './handlers'
import { map, mergeMap } from 'rxjs/operators'

export function requestObservables(
  requestQueue: RequestQueue<UserData>,
  ctx: ApifyContext
) {
  const linksGenerators = of(
    listingHandler,
    pageLinkHandler,
    propertyHandler,
    resourceHandler
  ).pipe(
    mergeMap(handler => handler(of(ctx))),
    map(newRequest => requestQueue.addRequest(newRequest))
  )

  const extraOperations = of(photosHandler, addressHandler).pipe(
    mergeMap(handler => handler(of(ctx)))
  )

  return merge(linksGenerators, extraOperations).toPromise()
}
