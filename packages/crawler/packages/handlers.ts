import {
  concatMap,
  filter,
  map,
  mapTo,
  pluck,
  shareReplay,
  toArray,
  withLatestFrom
} from 'rxjs/operators'
import { pipe, UnaryFunction, Observable, of, merge } from 'rxjs'
import { load } from 'cheerio'
import { parseListing, parseProperty } from './parsers'
import { truthy, makeRequest, debug } from './utils'
import { Request } from 'apify'
import {
  ApifyContext,
  PROPERTY_REQUEST,
  REQUEST_TYPE,
  PROPERTY_PHOTO_REQUEST,
  FLOOR_PLAN_REQUEST,
  LISTING_REQUEST,
  RESOURCE_REQUEST,
  ProfileMediaResponse,
  ADDRESS_REQUEST,
  GeocoderResponse
} from '../types'
import { from, forkJoin } from 'rxjs'
import { findIndex, mergeMap } from 'rxjs/operators'
import { get, processRequest } from './axios'
import { Storage } from '@google-cloud/storage'
import { v4 as uuid } from 'uuid'

import { read, MIME_JPEG } from 'jimp'
import { MongoClient, ObjectId } from 'mongodb'
import { IFloorPlan, IRentProperty } from 'apartments-shared'

const DATABASE_URL = 'mongodb://127.0.0.1:27017/'
const DATABASE_NAME = 'apartments'
const COLLECTION_NAME = 'Property'
type MongoProperty = Partial<IRentProperty & { _id: ObjectId }>
const client = new MongoClient(DATABASE_URL)
const mongodb = from(client.connect()).pipe(
  map(() =>
    client.db(DATABASE_NAME).collection<MongoProperty>(COLLECTION_NAME)
  ),
  shareReplay(1)
)
const MEDIA_RESOURCE =
  'https://www.apartments.com/services/property/profilemedia/'
const storage = new Storage({
  credentials: {
    // eslint-disable-next-line @typescript-eslint/camelcase
    client_email: 'image-writer@the-axiom-268521.iam.gserviceaccount.com',
    // eslint-disable-next-line @typescript-eslint/camelcase
    private_key:
      '-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDPm3y8MRPw+PFP\nFCtTRZPQPsciqvd4nLuKpsyVMrv/NRQLe+fpB65oM1i8DG9tdMW6XsJc07iLxQKa\nYdWttx/kzLYGTcIXBiwSrwj6qhh+cXefhNQrJqYs+5Asr5sFu4PCITvD5YkJVG8h\n+EtJrh4w68Su5k9mPBxegm1uv9SMxJWxd+xBuY0Gy8f4CxlqNdEfekiZsjMdwTYn\nPf0WUJnBqvjvlSeLNeP5wHB3c/uhJg6YXVnM18maqgoU8iiJMluttgQjpknXelD1\nDQRYvUzg6PjLhAn1JaRzWlMafrGeWDNssfirKDuwmLa69LxJjvnLb5nYY09NXbMM\nYv9H+mmdAgMBAAECggEADbGNEvGuxcp5yjRDnlC8zPPTGTmLLLCz2xOg82dfCJiU\n66P+2jtIOPXIENFGKUNWRnJ5IfvSFKAISawR1s05JJOPHw/ScUfWMN5MOpJtHqv3\nzrUJdJYuSFHIgvYLRN/HM08TXNq/RPcFhIlW1/OM6L+oX7vvuUfWPDKgqrkq+STg\nyexiqf1z4nh99W50Olr6fq0UivnrM0x6KlKNNlLrAMaplhjCAurvXvZbseEAA6ko\njPE1CUjx0t+cqIaZopiyhUu3QW/rr97dZdYdDAWyIj9AICGYytJaJs+pYd/BaxPr\nAvUmQWlxGzX05+aDcrgGdOekl7ageI1oXy4U2TZXSQKBgQD9NqWi6tf7fPkCZiWp\njvDmSkBax4jVAdH3q6E4s5Jd5RDrpw64eDkqmC1SiPc2946G/dnCv3uue7woGDuz\nETVi6JdHxaxjNBitqUqUaJXSLELwBXkquBNlFWy0UKoZ+fySbxnYzxMLdyUIcfYJ\nNqWfQMWD+47Hz9iqFYZ804lwJQKBgQDR5FvWk0uF9xCWpC+h0jnHGS23ZJqeTqW1\nNE/DIncW4ZTVb9PvBtmWsr0zB7B9YTpNbV826A0kcbXA7vF2r5ejALZp+xhoUPEh\ncNTgewtEDCuKwdMXAB+tdwUEIRIIPzPP15vfVYyfVHA5Z7D3IqLtyRAu+ydHzT00\nKUbH7xe+GQKBgBpmhTSMScKV0ByY69pW5fUMztLoNPMIgozRHzkbN+qERJRdUaKY\nHlW7khYl46iefQTEGN0+O7YAfqirhiXwaGGMoKFCEewx9VS97uG0WdKU/NGrsGts\nz9DdP/0439Nfhh76aHZ7eJ/cZ0cPIrjpMrF8/lr5cnSQziVFo0UGhTDlAoGBAJsF\nYp804w7qz6GzWkrWjj3y5sUQ1PIffFw9arvg9Lw9mEOb+JgOIx8enIbYIw43hWwy\nX6SZ9zr3kRoSYN0cC48ks5XLEtfZEH92aoPx3PxBUNeHGHVgbgUcZuQJ2NumcooL\n4OAuu1Hid5CueRHOzy941S8d6IEroQypf9GjUZHhAoGBALGiHEDNosuQa1KjK0oK\n/JfgiE72oWQei85B/yYetMlqSC/FirAWwpQN4WSv1HdCHu71NuCNVdEhzWbft3s6\nIbOGdUpO4ZVprf92EQHbCBpQIodLlCGrh2sbeZjc/WUL4YvAcr+aMDuFUC4lXJB8\nIn98CLAVQHsWKHbr3VSiP3A9\n-----END PRIVATE KEY-----\n'
  }
})

export const GOOGLE_KEY = 'AIzaSyAhRJu8PMeIu87WwsKb9baMPTly5DqpERo'
export const GOOGLE_GEO =
  'https://maps.googleapis.com/maps/api/geocode/json?key='
export const GOOGLE_GEO_URL = GOOGLE_GEO + GOOGLE_KEY + '&latlng='

const PLANS = [
  'basic',
  'tiertwo',
  'prosumer',
  'premium',
  'premiumplus',
  'silver',
  'gold',
  'platinum',
  'diamond'
] as const

export const listingHandler: UnaryFunction<
  Observable<ApifyContext>,
  Observable<Request<PROPERTY_REQUEST>>
> = pipe(
  filter(
    (ctx): ctx is ApifyContext<LISTING_REQUEST> =>
      ctx.request.userData.type === REQUEST_TYPE.PROPERTIES_LISTING
  ),
  processRequest(),
  map(data => load(data, { decodeEntities: false })),
  parseListing(),
  mergeMap(([$, el]) =>
    forkJoin({
      url: of(el.attribs['data-url']),
      userData: from(PLANS).pipe(
        findIndex(v => $(el).hasClass('js-' + v)),
        map(priority => ({ priority, type: REQUEST_TYPE.PROPERTY } as const))
      )
    })
  ),
  filter(({ url, userData }) => !!url && userData.priority > 2),
  map(v => new Request<PROPERTY_REQUEST>(v))
)

export const pageLinkHandler: UnaryFunction<
  Observable<ApifyContext>,
  Observable<Request<LISTING_REQUEST>>
> = pipe(
  filter(
    (ctx): ctx is ApifyContext<LISTING_REQUEST> =>
      ctx.request.userData.type === REQUEST_TYPE.PROPERTIES_LISTING
  ),
  processRequest(),
  map(data => load(data, { decodeEntities: false })),
  map($ => $('a.next').attr('href')),
  truthy(),
  filter(url => url.startsWith('https://www.apartments.com/')),
  map(
    url =>
      new Request<LISTING_REQUEST>({
        url: url.replace(
          'https://www.apartments.com/',
          'https://www.apartments.com/apartments/'
        ),
        userData: {
          type: REQUEST_TYPE.PROPERTIES_LISTING
        }
      })
  )
)

export const propertyHandler: UnaryFunction<
  Observable<ApifyContext>,
  Observable<Request<RESOURCE_REQUEST | ADDRESS_REQUEST>>
> = pipe(
  filter(
    (ctx): ctx is ApifyContext<PROPERTY_REQUEST> =>
      ctx.request.userData.type === REQUEST_TYPE.PROPERTY
  ),
  mergeMap(ctx => {
    return of(ctx).pipe(
      processRequest<string>(),
      map(
        data =>
          [
            load(data, { decodeEntities: false }),
            data.match(/antiWebCrawlerToken: '(.*)'/)?.[1] ?? ''
          ] as const
      ),
      map(([$, token]) => [parseProperty($, ctx.request), token] as const)
    )
  }),
  mergeMap(([{ photos, cover, floorPlans, address, ...property }, token]) => {
    return mongodb.pipe(
      mergeMap(collection =>
        collection.findOneAndUpdate(
          { originId: property.originId },
          {
            $set: {
              ...property,
              floorPlans: floorPlans.map(
                ({ photos, ...floorPlan }) => floorPlan
              ) as IFloorPlan[],
              'address.location': address.location
            }
          },
          { upsert: true, returnOriginal: true }
        )
      ),
      pluck('value'),
      truthy(),
      debug('Created Property: ', p => ({ id: String(p._id), name: p.name })),
      mergeMap(dbProperty => {
        const requests: Request<RESOURCE_REQUEST | ADDRESS_REQUEST>[] = [
          makeRequest<RESOURCE_REQUEST>({
            url: MEDIA_RESOURCE,
            payload: {
              ListingKey: property.originId
            },
            headers: {
              referer: property.origin,
              x_awc_token: token
            },
            userData: {
              type: REQUEST_TYPE.PROPERTY_RESOURCES,
              propertyId: String(dbProperty._id),
              propertyOriginId: property.originId
            }
          })
        ]
        if (!dbProperty.address?.formatted) {
          requests.push(
            new Request<ADDRESS_REQUEST>({
              url:
                GOOGLE_GEO_URL + String(address.location.coordinates.reverse()),
              userData: {
                type: REQUEST_TYPE.PROPERTY_ADDRESS,
                propertyId: String(dbProperty._id)
              }
            })
          )
        }
        return from(requests)
      })
    )
  })
)

export const resourceHandler: UnaryFunction<
  Observable<ApifyContext>,
  Observable<Request<PROPERTY_PHOTO_REQUEST | FLOOR_PLAN_REQUEST>>
> = pipe(
  filter(
    (ctx): ctx is ApifyContext<RESOURCE_REQUEST> =>
      ctx.request.userData.type === REQUEST_TYPE.PROPERTY_RESOURCES
  ),
  mergeMap(ctx =>
    of(ctx).pipe(
      processRequest<ProfileMediaResponse>(),
      pluck('Media'),
      truthy(),
      withLatestFrom(
        mongodb.pipe(
          mergeMap(collection =>
            collection.findOne({
              _id: new ObjectId(ctx.request.userData.propertyId)
            })
          )
        )
      ),
      mergeMap(([[propertyPhotos, floorPlansPhotos], property]) =>
        merge(
          from(propertyPhotos ?? []).pipe(
            filter(() => !property?.photos?.length),
            map(
              media =>
                new Request<PROPERTY_PHOTO_REQUEST>({
                  url: media.Uri,
                  userData: {
                    type: REQUEST_TYPE.PROPERTY_PHOTO,
                    propertyId: ctx.request.userData.propertyId,
                    media
                  }
                })
            )
          ),
          from(floorPlansPhotos ?? []).pipe(
            filter(
              media =>
                !property?.floorPlans?.find(fp => fp.originAttId === media.Id)
                  ?.photos?.length
            ),
            map(
              media =>
                new Request<FLOOR_PLAN_REQUEST>({
                  url: media.Uri,
                  userData: {
                    type: REQUEST_TYPE.FLOOR_PLAN_PHOTO,
                    propertyId: ctx.request.userData.propertyId,
                    media
                  }
                })
            )
          )
        )
      )
    )
  )
)

export const pushToBucket = (bucketName: string) =>
  mergeMap((buffer: Buffer) => {
    const imageName = uuid() + '.jpg'
    return from(
      storage
        .bucket(bucketName)
        .file(imageName)
        .save(buffer, {
          contentType: 'image/jpg',
          metadata: {
            'Cache-Control': 'public, max-age= 360000'
          }
        })
    ).pipe(mapTo(`https://${bucketName}/${imageName}`))
  })

export const saveToDatabase = (
  data: FLOOR_PLAN_REQUEST | PROPERTY_PHOTO_REQUEST
) =>
  mergeMap((src: string) =>
    mongodb.pipe(
      mergeMap(collection => {
        const _id = new ObjectId(data.propertyId)
        if (data.type === REQUEST_TYPE.PROPERTY_PHOTO)
          return collection.updateOne(
            { _id },
            {
              ...(data.media.AttachmentType === 3
                ? { $set: { cover: src } }
                : {}),
              $push: {
                photos: {
                  src,
                  mediaType: data.media.MediaType,
                  altText: data.media.AltText ?? '',
                  attachmentType: data.media.AttachmentType,
                  description: data.media.Caption ?? ''
                }
              }
            }
          )

        return collection.updateOne(
          { _id, 'floorPlans.originAttId': data.media.Id },
          {
            $push: {
              ['floorPlans.$.photos']: {
                src,
                mediaType: data.media.MediaType,
                altText: data.media.AltText ?? '',
                attachmentType: data.media.AttachmentType,
                description: data.media.Caption ?? ''
              }
            }
          }
        )
      })
    )
  )

export const photosHandler: UnaryFunction<
  Observable<ApifyContext>,
  Observable<boolean>
> = pipe(
  filter(
    (ctx): ctx is ApifyContext<PROPERTY_PHOTO_REQUEST | FLOOR_PLAN_REQUEST> =>
      ctx.request.userData.type === REQUEST_TYPE.FLOOR_PLAN_PHOTO ||
      ctx.request.userData.type === REQUEST_TYPE.PROPERTY_PHOTO
  ),
  mergeMap(ctx =>
    get<Buffer>(ctx.request.url, { responseType: 'arraybuffer' }).pipe(
      concatMap(buffer => read(buffer)),
      concatMap(image => {
        const height = image.getHeight()
        const width = image.getWidth()
        return image.crop(0, 0, width, height * 0.9).getBufferAsync(MIME_JPEG)
      }),
      pushToBucket('images.findyournextapartment.com'),
      debug('Pushed image', url => ({
        id: ctx.request.userData.propertyId,
        url
      })),
      saveToDatabase(ctx.request.userData),
      mapTo(true)
    )
  )
)

export const addressHandler: UnaryFunction<
  Observable<ApifyContext>,
  Observable<boolean>
> = pipe(
  filter(
    (ctx): ctx is ApifyContext<ADDRESS_REQUEST> =>
      ctx.request.userData.type === REQUEST_TYPE.PROPERTY_ADDRESS
  ),
  mergeMap(ctx =>
    of(ctx).pipe(
      processRequest<GeocoderResponse>(),
      filter(response => response.results.length > 0),
      map(response => response.results[0]),
      concatMap(result =>
        forkJoin({
          'address.formatted': of(result.formatted_address),
          'address.components': from(result.address_components).pipe(
            map(component => ({
              name: component.long_name,
              type: component.types[0]
            })),
            toArray()
          )
        })
      ),
      debug('Saved address for', () => ({
        id: ctx.request.userData.propertyId
      })),
      withLatestFrom(mongodb),
      mergeMap(([update, collection]) =>
        collection.updateOne(
          { _id: new ObjectId(ctx.request.userData.propertyId) },
          { $set: update }
        )
      )
    )
  ),
  mapTo(true)
)
