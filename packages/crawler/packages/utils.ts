import { Request, RequestOptions, utils } from 'apify'
import {
  MonoTypeOperatorFunction,
} from 'rxjs'
import { filter, tap } from 'rxjs/operators'

import { ZipCode } from './zipcode'

export function truthy<T>(): MonoTypeOperatorFunction<NonNullable<T>> {
  return filter(v => !!v)
}

export function makeRequest<T>(
  request: Partial<
    Omit<Request<T>, 'payload'> & { payload: Record<string, any> }
  >
): Request<T> {
  return new Request({
    method: 'POST',
    useExtendedUniqueKey: true,
    ...request,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      ...request.headers ?? {}
    },
    payload: JSON.stringify(request?.payload ?? {})
  } as RequestOptions<T>)
}

export function generateUrl(zipCodes: ZipCode[]): string[] {
  return zipCodes
    .map(({ city, state, zip }) =>
      (city.toLowerCase() + ' ' + state.toLowerCase() + ' ' + zip).replace(
        /[ ]/g,
        '-'
      )
    )
    .map(code => `https://www.apartments.com/apartments/${code}/`)
}

export function debug<T>(
  scope: string,
  fn: (value: T) => unknown = value =>
    typeof value === 'object' ? value : { value }
): MonoTypeOperatorFunction<T> {
  return tap(val => utils.log.debug(scope.toUpperCase(), fn(val)))
}

export const LISTING_URL = 'https://www.apartments.com/apartments'
export const PROPERTY_MEDIA =
  'https://www.apartments.com/services/property/profilemedia/'
export const FLOOR_PLAN_MEDIA =
  'https://www.apartments.com/services/rental/attachments'
