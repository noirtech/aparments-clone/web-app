import Axios, { AxiosRequestConfig, AxiosResponse, AxiosError } from 'axios'
import HttpsProxyAgent from 'https-proxy-agent'
import { Observable, defer } from 'rxjs'
import { map, mergeMap, retry } from 'rxjs/operators'
import { utils } from 'apify'
import { ApifyContext } from '../types'

export const axiosRequestConfiguration: AxiosRequestConfig = {
  responseType: 'json',
  proxy: false,
  httpsAgent: HttpsProxyAgent(process.env.AXIOS_APIFY_PROXY_URL as string),
  headers: {
    'User-Agent':
      'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36',
    Accept:
      'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9'
  }
}
export const axios = Axios.create(axiosRequestConfiguration)

function createObservable<T>(
  fn: Function,
  ...args: [string, unknown, AxiosRequestConfig] | [string, AxiosRequestConfig]
): Observable<AxiosResponse<T>> {
  return new Observable(subscriber => {
    fn(...args)
      .then((response: AxiosResponse<T>) => {
        if (response.status >= 200 && response.status < 300) {
          subscriber.next(response)
          subscriber.complete()
        } else {
          subscriber.error(response.data)
          subscriber.complete()
          utils.log.error('Axios error', { message: response.data })
        }
      })
      .catch((err: AxiosError) => {
        utils.log.error('Axios catch error', { message: err.message, args: args })
        subscriber.error(err.message)
        subscriber.complete()
      })
  })
}

export const get = <T>(
  url: string,
  config: AxiosRequestConfig = {},
  axiosInstance = axios
): Observable<T> => {
  return defer(() =>
    createObservable<T>(axiosInstance.get, url, config).pipe(
      retry(3),
      map(result => result.data)
    )
  )
}

export const processRequest = <T = string>() => mergeMap((ctx: ApifyContext) => {
  if(ctx.request.method === 'POST') {
    return post<T>(ctx.request.url, JSON.parse(ctx.request.payload), {
      headers: ctx.request.headers
    })
  } else {
    return get<T>(ctx.request.url)
  }
})

export const post = <T>(
  url: string,
  data: unknown,
  config: AxiosRequestConfig = {},
  axiosInstance = axios
): Observable<T> => {
  return defer(() =>
    createObservable<T>(axiosInstance.post, url, data, config).pipe(
      retry(10),
      map(result => result.data)
    )
  )
}
