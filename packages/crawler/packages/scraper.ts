import {
  RequestList,
  openRequestQueue,
  BasicCrawler,
  getInput,
  utils
} from 'apify'
import { UserData, REQUEST_TYPE } from '../types'
import { requestObservables } from './processor'

export async function scraper(urls: string[]): Promise<void> {
  const requestQueue = await openRequestQueue<UserData>()
  const input = await getInput<{ limits?: [number, number] }>()
  const limits = input?.limits ?? [0]

  const initialRequestList = new RequestList<UserData>({
    sources: urls.slice(...limits).map(url => ({
      url, method: 'GET',
      userData: {
        type: REQUEST_TYPE.PROPERTIES_LISTING
      }
    })),
  })

  await initialRequestList.initialize()

  const crawler = new BasicCrawler({
    requestList: initialRequestList,
    requestQueue,
    maxConcurrency: 5,
    maxRequestRetries: 5,
    handleRequestTimeoutSecs: 120,
    handleRequestFunction: async ctx => requestObservables(requestQueue, ctx),
    handleFailedRequestFunction: function (error) {
      utils.log.error(JSON.stringify(error))
    }
  })
  await crawler.run()
}
