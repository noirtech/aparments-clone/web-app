module.exports = {
    bracketSpacing: true,
    semi: false,
    trailingComma: "none",
    singleQuote: true,
    printWidth: 80,
    tabWidth: 2
};