import { IRentProperty, IFloorPlan, IPhoto, ComponentTypes } from 'apartments-shared'
import { BaseHandlePageObj } from 'apify'

export type URLData = { priority: number }
export type FPData = { propertyId: string; floorPlanId: string }

export const enum REQUEST_TYPE {
  PROPERTIES_LISTING,
  PROPERTY,
  PROPERTY_PHOTO,
  FLOOR_PLAN_PHOTO,
  PROPERTY_RESOURCES,
  PROPERTY_ADDRESS
}
export type LISTING_REQUEST = {
  type: REQUEST_TYPE.PROPERTIES_LISTING
}
export type PROPERTY_REQUEST = {
  type: REQUEST_TYPE.PROPERTY
  priority: number
}
export type PROPERTY_PHOTO_REQUEST = {
  propertyId: string
  media: Media
  type: REQUEST_TYPE.PROPERTY_PHOTO
}
export type FLOOR_PLAN_REQUEST = {
  propertyId: string
  media: Media
  type: REQUEST_TYPE.FLOOR_PLAN_PHOTO
}
export type RESOURCE_REQUEST = {
  propertyId: string
  propertyOriginId: string
  type: REQUEST_TYPE.PROPERTY_RESOURCES
}
export type ADDRESS_REQUEST = {
  propertyId: string
  type: REQUEST_TYPE.PROPERTY_ADDRESS
}
export interface GeocoderAddressComponent {
  long_name: string
  short_name: string
  types: ComponentTypes[]
}
export interface GeocoderResult {
  address_components: GeocoderAddressComponent[]
  formatted_address: string
  partial_match: boolean
  place_id: string
  postcode_localities: string[]
  types: string[]
}
export interface GeocoderResponse {
  results: GeocoderResult[]
}
export type UserData = LISTING_REQUEST
  | PROPERTY_REQUEST
  | FLOOR_PLAN_REQUEST
  | PROPERTY_PHOTO_REQUEST
  | RESOURCE_REQUEST
  | ADDRESS_REQUEST

export type Media = {
  Id: number
  EntityId: number
  Width?: number
  Height?: number
  Uri: string
  ThumbUri: string
  AttachmentType: number
  MediaType: number
  Caption?: string
  AltText?: string
}
export type IRentPropertyBuilder = Omit<IRentProperty, 'id'>

export type SerializedFloorplan = Omit<IFloorPlan, 'photos'> & {
  photos: string[]
}

export type SerializedProperty = Omit<
  IRentPropertyBuilder,
  'photos' | 'cover' | 'floorPlans'
> & {
  photos: string[]
  cover: string
  floorPlans: SerializedFloorplan[]
}

export type ProfileMediaResponse = {
  Carousel?: Media[]
  Gallery?: [property: Media[], floorPlans: Media[]]
  Media?: [property: Media[], floorPlans: Media[], others: Media[]]
}

export type ApifyContext<T = UserData> = BaseHandlePageObj<T>
