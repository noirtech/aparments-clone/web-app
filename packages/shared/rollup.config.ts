import camelCase from 'lodash.camelcase'
import typescript from 'rollup-plugin-typescript2'
import sourceMaps from 'rollup-plugin-sourcemaps'
import commonjs from "@rollup/plugin-commonjs";
import json from "@rollup/plugin-json";
import resolve from "@rollup/plugin-node-resolve";
import builtins from 'rollup-plugin-node-builtins'
import globals from 'rollup-plugin-node-globals';

const pkg = require('./package.json')

const libraryName = 'apartments-shared'

export default {
  input: `src/${libraryName}.ts`,
  output: [
    { file: pkg.main, name: camelCase(libraryName), format: 'umd', sourcemap: true },
    { file: pkg.module, format: 'es', sourcemap: true },
  ],
  // Indicate here external modules you don't wanna include in your bundle (i.e.: 'lodash')
  external: [],
  watch: {
    include: 'src/**',
  },
  plugins: [
    json(),
    typescript({ useTsconfigDeclarationDir: true, typescript: require('typescript'), tsconfig: 'tsconfig.json' }),
    commonjs(),
    resolve({ preferBuiltins: true }),
    globals(),
    builtins(),
    sourceMaps()
  ],
}