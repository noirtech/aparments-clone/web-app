import { IUser, IUserCredentials } from "../types";
export declare const givenUserData: () => Partial<IUser>;
export declare const givenUserCredentialsData: () => IUserCredentials;
