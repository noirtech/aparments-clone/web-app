export declare namespace Configuration {
    interface ContactUs {
        key: 'contact-us';
        description: string;
        groups: Array<{
            title: string;
            content: string;
        }>;
    }
}
