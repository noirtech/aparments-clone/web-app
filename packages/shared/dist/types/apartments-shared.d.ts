export * from './validators';
export * from './types';
export * from './test-utils';
export declare function encryptQuery<T>(value: T): string;
export declare function decodeQuery<T>(text: string): T;
