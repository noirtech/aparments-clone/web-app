(function (global, factory) {
            typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
            typeof define === 'function' && define.amd ? define(['exports'], factory) :
            (global = global || self, factory(global.apartmentsShared = {}));
}(this, (function (exports) { 'use strict';

            var global$1 = (typeof global !== "undefined" ? global :
                        typeof self !== "undefined" ? self :
                        typeof window !== "undefined" ? window : {});

            var lookup = [];
            var revLookup = [];
            var Arr = typeof Uint8Array !== 'undefined' ? Uint8Array : Array;
            var inited = false;
            function init () {
              inited = true;
              var code = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
              for (var i = 0, len = code.length; i < len; ++i) {
                lookup[i] = code[i];
                revLookup[code.charCodeAt(i)] = i;
              }

              revLookup['-'.charCodeAt(0)] = 62;
              revLookup['_'.charCodeAt(0)] = 63;
            }

            function toByteArray (b64) {
              if (!inited) {
                init();
              }
              var i, j, l, tmp, placeHolders, arr;
              var len = b64.length;

              if (len % 4 > 0) {
                throw new Error('Invalid string. Length must be a multiple of 4')
              }

              // the number of equal signs (place holders)
              // if there are two placeholders, than the two characters before it
              // represent one byte
              // if there is only one, then the three characters before it represent 2 bytes
              // this is just a cheap hack to not do indexOf twice
              placeHolders = b64[len - 2] === '=' ? 2 : b64[len - 1] === '=' ? 1 : 0;

              // base64 is 4/3 + up to two characters of the original data
              arr = new Arr(len * 3 / 4 - placeHolders);

              // if there are placeholders, only get up to the last complete 4 chars
              l = placeHolders > 0 ? len - 4 : len;

              var L = 0;

              for (i = 0, j = 0; i < l; i += 4, j += 3) {
                tmp = (revLookup[b64.charCodeAt(i)] << 18) | (revLookup[b64.charCodeAt(i + 1)] << 12) | (revLookup[b64.charCodeAt(i + 2)] << 6) | revLookup[b64.charCodeAt(i + 3)];
                arr[L++] = (tmp >> 16) & 0xFF;
                arr[L++] = (tmp >> 8) & 0xFF;
                arr[L++] = tmp & 0xFF;
              }

              if (placeHolders === 2) {
                tmp = (revLookup[b64.charCodeAt(i)] << 2) | (revLookup[b64.charCodeAt(i + 1)] >> 4);
                arr[L++] = tmp & 0xFF;
              } else if (placeHolders === 1) {
                tmp = (revLookup[b64.charCodeAt(i)] << 10) | (revLookup[b64.charCodeAt(i + 1)] << 4) | (revLookup[b64.charCodeAt(i + 2)] >> 2);
                arr[L++] = (tmp >> 8) & 0xFF;
                arr[L++] = tmp & 0xFF;
              }

              return arr
            }

            function tripletToBase64 (num) {
              return lookup[num >> 18 & 0x3F] + lookup[num >> 12 & 0x3F] + lookup[num >> 6 & 0x3F] + lookup[num & 0x3F]
            }

            function encodeChunk (uint8, start, end) {
              var tmp;
              var output = [];
              for (var i = start; i < end; i += 3) {
                tmp = (uint8[i] << 16) + (uint8[i + 1] << 8) + (uint8[i + 2]);
                output.push(tripletToBase64(tmp));
              }
              return output.join('')
            }

            function fromByteArray (uint8) {
              if (!inited) {
                init();
              }
              var tmp;
              var len = uint8.length;
              var extraBytes = len % 3; // if we have 1 byte left, pad 2 bytes
              var output = '';
              var parts = [];
              var maxChunkLength = 16383; // must be multiple of 3

              // go through the array every three bytes, we'll deal with trailing stuff later
              for (var i = 0, len2 = len - extraBytes; i < len2; i += maxChunkLength) {
                parts.push(encodeChunk(uint8, i, (i + maxChunkLength) > len2 ? len2 : (i + maxChunkLength)));
              }

              // pad the end with zeros, but make sure to not forget the extra bytes
              if (extraBytes === 1) {
                tmp = uint8[len - 1];
                output += lookup[tmp >> 2];
                output += lookup[(tmp << 4) & 0x3F];
                output += '==';
              } else if (extraBytes === 2) {
                tmp = (uint8[len - 2] << 8) + (uint8[len - 1]);
                output += lookup[tmp >> 10];
                output += lookup[(tmp >> 4) & 0x3F];
                output += lookup[(tmp << 2) & 0x3F];
                output += '=';
              }

              parts.push(output);

              return parts.join('')
            }

            function read (buffer, offset, isLE, mLen, nBytes) {
              var e, m;
              var eLen = nBytes * 8 - mLen - 1;
              var eMax = (1 << eLen) - 1;
              var eBias = eMax >> 1;
              var nBits = -7;
              var i = isLE ? (nBytes - 1) : 0;
              var d = isLE ? -1 : 1;
              var s = buffer[offset + i];

              i += d;

              e = s & ((1 << (-nBits)) - 1);
              s >>= (-nBits);
              nBits += eLen;
              for (; nBits > 0; e = e * 256 + buffer[offset + i], i += d, nBits -= 8) {}

              m = e & ((1 << (-nBits)) - 1);
              e >>= (-nBits);
              nBits += mLen;
              for (; nBits > 0; m = m * 256 + buffer[offset + i], i += d, nBits -= 8) {}

              if (e === 0) {
                e = 1 - eBias;
              } else if (e === eMax) {
                return m ? NaN : ((s ? -1 : 1) * Infinity)
              } else {
                m = m + Math.pow(2, mLen);
                e = e - eBias;
              }
              return (s ? -1 : 1) * m * Math.pow(2, e - mLen)
            }

            function write (buffer, value, offset, isLE, mLen, nBytes) {
              var e, m, c;
              var eLen = nBytes * 8 - mLen - 1;
              var eMax = (1 << eLen) - 1;
              var eBias = eMax >> 1;
              var rt = (mLen === 23 ? Math.pow(2, -24) - Math.pow(2, -77) : 0);
              var i = isLE ? 0 : (nBytes - 1);
              var d = isLE ? 1 : -1;
              var s = value < 0 || (value === 0 && 1 / value < 0) ? 1 : 0;

              value = Math.abs(value);

              if (isNaN(value) || value === Infinity) {
                m = isNaN(value) ? 1 : 0;
                e = eMax;
              } else {
                e = Math.floor(Math.log(value) / Math.LN2);
                if (value * (c = Math.pow(2, -e)) < 1) {
                  e--;
                  c *= 2;
                }
                if (e + eBias >= 1) {
                  value += rt / c;
                } else {
                  value += rt * Math.pow(2, 1 - eBias);
                }
                if (value * c >= 2) {
                  e++;
                  c /= 2;
                }

                if (e + eBias >= eMax) {
                  m = 0;
                  e = eMax;
                } else if (e + eBias >= 1) {
                  m = (value * c - 1) * Math.pow(2, mLen);
                  e = e + eBias;
                } else {
                  m = value * Math.pow(2, eBias - 1) * Math.pow(2, mLen);
                  e = 0;
                }
              }

              for (; mLen >= 8; buffer[offset + i] = m & 0xff, i += d, m /= 256, mLen -= 8) {}

              e = (e << mLen) | m;
              eLen += mLen;
              for (; eLen > 0; buffer[offset + i] = e & 0xff, i += d, e /= 256, eLen -= 8) {}

              buffer[offset + i - d] |= s * 128;
            }

            var toString = {}.toString;

            var isArray = Array.isArray || function (arr) {
              return toString.call(arr) == '[object Array]';
            };

            var INSPECT_MAX_BYTES = 50;

            /**
             * If `Buffer.TYPED_ARRAY_SUPPORT`:
             *   === true    Use Uint8Array implementation (fastest)
             *   === false   Use Object implementation (most compatible, even IE6)
             *
             * Browsers that support typed arrays are IE 10+, Firefox 4+, Chrome 7+, Safari 5.1+,
             * Opera 11.6+, iOS 4.2+.
             *
             * Due to various browser bugs, sometimes the Object implementation will be used even
             * when the browser supports typed arrays.
             *
             * Note:
             *
             *   - Firefox 4-29 lacks support for adding new properties to `Uint8Array` instances,
             *     See: https://bugzilla.mozilla.org/show_bug.cgi?id=695438.
             *
             *   - Chrome 9-10 is missing the `TypedArray.prototype.subarray` function.
             *
             *   - IE10 has a broken `TypedArray.prototype.subarray` function which returns arrays of
             *     incorrect length in some situations.

             * We detect these buggy browsers and set `Buffer.TYPED_ARRAY_SUPPORT` to `false` so they
             * get the Object implementation, which is slower but behaves correctly.
             */
            Buffer$1.TYPED_ARRAY_SUPPORT = global$1.TYPED_ARRAY_SUPPORT !== undefined
              ? global$1.TYPED_ARRAY_SUPPORT
              : true;

            function kMaxLength () {
              return Buffer$1.TYPED_ARRAY_SUPPORT
                ? 0x7fffffff
                : 0x3fffffff
            }

            function createBuffer (that, length) {
              if (kMaxLength() < length) {
                throw new RangeError('Invalid typed array length')
              }
              if (Buffer$1.TYPED_ARRAY_SUPPORT) {
                // Return an augmented `Uint8Array` instance, for best performance
                that = new Uint8Array(length);
                that.__proto__ = Buffer$1.prototype;
              } else {
                // Fallback: Return an object instance of the Buffer class
                if (that === null) {
                  that = new Buffer$1(length);
                }
                that.length = length;
              }

              return that
            }

            /**
             * The Buffer constructor returns instances of `Uint8Array` that have their
             * prototype changed to `Buffer.prototype`. Furthermore, `Buffer` is a subclass of
             * `Uint8Array`, so the returned instances will have all the node `Buffer` methods
             * and the `Uint8Array` methods. Square bracket notation works as expected -- it
             * returns a single octet.
             *
             * The `Uint8Array` prototype remains unmodified.
             */

            function Buffer$1 (arg, encodingOrOffset, length) {
              if (!Buffer$1.TYPED_ARRAY_SUPPORT && !(this instanceof Buffer$1)) {
                return new Buffer$1(arg, encodingOrOffset, length)
              }

              // Common case.
              if (typeof arg === 'number') {
                if (typeof encodingOrOffset === 'string') {
                  throw new Error(
                    'If encoding is specified then the first argument must be a string'
                  )
                }
                return allocUnsafe(this, arg)
              }
              return from(this, arg, encodingOrOffset, length)
            }

            Buffer$1.poolSize = 8192; // not used by this implementation

            // TODO: Legacy, not needed anymore. Remove in next major version.
            Buffer$1._augment = function (arr) {
              arr.__proto__ = Buffer$1.prototype;
              return arr
            };

            function from (that, value, encodingOrOffset, length) {
              if (typeof value === 'number') {
                throw new TypeError('"value" argument must not be a number')
              }

              if (typeof ArrayBuffer !== 'undefined' && value instanceof ArrayBuffer) {
                return fromArrayBuffer(that, value, encodingOrOffset, length)
              }

              if (typeof value === 'string') {
                return fromString(that, value, encodingOrOffset)
              }

              return fromObject(that, value)
            }

            /**
             * Functionally equivalent to Buffer(arg, encoding) but throws a TypeError
             * if value is a number.
             * Buffer.from(str[, encoding])
             * Buffer.from(array)
             * Buffer.from(buffer)
             * Buffer.from(arrayBuffer[, byteOffset[, length]])
             **/
            Buffer$1.from = function (value, encodingOrOffset, length) {
              return from(null, value, encodingOrOffset, length)
            };

            if (Buffer$1.TYPED_ARRAY_SUPPORT) {
              Buffer$1.prototype.__proto__ = Uint8Array.prototype;
              Buffer$1.__proto__ = Uint8Array;
            }

            function assertSize (size) {
              if (typeof size !== 'number') {
                throw new TypeError('"size" argument must be a number')
              } else if (size < 0) {
                throw new RangeError('"size" argument must not be negative')
              }
            }

            function alloc (that, size, fill, encoding) {
              assertSize(size);
              if (size <= 0) {
                return createBuffer(that, size)
              }
              if (fill !== undefined) {
                // Only pay attention to encoding if it's a string. This
                // prevents accidentally sending in a number that would
                // be interpretted as a start offset.
                return typeof encoding === 'string'
                  ? createBuffer(that, size).fill(fill, encoding)
                  : createBuffer(that, size).fill(fill)
              }
              return createBuffer(that, size)
            }

            /**
             * Creates a new filled Buffer instance.
             * alloc(size[, fill[, encoding]])
             **/
            Buffer$1.alloc = function (size, fill, encoding) {
              return alloc(null, size, fill, encoding)
            };

            function allocUnsafe (that, size) {
              assertSize(size);
              that = createBuffer(that, size < 0 ? 0 : checked(size) | 0);
              if (!Buffer$1.TYPED_ARRAY_SUPPORT) {
                for (var i = 0; i < size; ++i) {
                  that[i] = 0;
                }
              }
              return that
            }

            /**
             * Equivalent to Buffer(num), by default creates a non-zero-filled Buffer instance.
             * */
            Buffer$1.allocUnsafe = function (size) {
              return allocUnsafe(null, size)
            };
            /**
             * Equivalent to SlowBuffer(num), by default creates a non-zero-filled Buffer instance.
             */
            Buffer$1.allocUnsafeSlow = function (size) {
              return allocUnsafe(null, size)
            };

            function fromString (that, string, encoding) {
              if (typeof encoding !== 'string' || encoding === '') {
                encoding = 'utf8';
              }

              if (!Buffer$1.isEncoding(encoding)) {
                throw new TypeError('"encoding" must be a valid string encoding')
              }

              var length = byteLength(string, encoding) | 0;
              that = createBuffer(that, length);

              var actual = that.write(string, encoding);

              if (actual !== length) {
                // Writing a hex string, for example, that contains invalid characters will
                // cause everything after the first invalid character to be ignored. (e.g.
                // 'abxxcd' will be treated as 'ab')
                that = that.slice(0, actual);
              }

              return that
            }

            function fromArrayLike (that, array) {
              var length = array.length < 0 ? 0 : checked(array.length) | 0;
              that = createBuffer(that, length);
              for (var i = 0; i < length; i += 1) {
                that[i] = array[i] & 255;
              }
              return that
            }

            function fromArrayBuffer (that, array, byteOffset, length) {
              array.byteLength; // this throws if `array` is not a valid ArrayBuffer

              if (byteOffset < 0 || array.byteLength < byteOffset) {
                throw new RangeError('\'offset\' is out of bounds')
              }

              if (array.byteLength < byteOffset + (length || 0)) {
                throw new RangeError('\'length\' is out of bounds')
              }

              if (byteOffset === undefined && length === undefined) {
                array = new Uint8Array(array);
              } else if (length === undefined) {
                array = new Uint8Array(array, byteOffset);
              } else {
                array = new Uint8Array(array, byteOffset, length);
              }

              if (Buffer$1.TYPED_ARRAY_SUPPORT) {
                // Return an augmented `Uint8Array` instance, for best performance
                that = array;
                that.__proto__ = Buffer$1.prototype;
              } else {
                // Fallback: Return an object instance of the Buffer class
                that = fromArrayLike(that, array);
              }
              return that
            }

            function fromObject (that, obj) {
              if (internalIsBuffer(obj)) {
                var len = checked(obj.length) | 0;
                that = createBuffer(that, len);

                if (that.length === 0) {
                  return that
                }

                obj.copy(that, 0, 0, len);
                return that
              }

              if (obj) {
                if ((typeof ArrayBuffer !== 'undefined' &&
                    obj.buffer instanceof ArrayBuffer) || 'length' in obj) {
                  if (typeof obj.length !== 'number' || isnan(obj.length)) {
                    return createBuffer(that, 0)
                  }
                  return fromArrayLike(that, obj)
                }

                if (obj.type === 'Buffer' && isArray(obj.data)) {
                  return fromArrayLike(that, obj.data)
                }
              }

              throw new TypeError('First argument must be a string, Buffer, ArrayBuffer, Array, or array-like object.')
            }

            function checked (length) {
              // Note: cannot use `length < kMaxLength()` here because that fails when
              // length is NaN (which is otherwise coerced to zero.)
              if (length >= kMaxLength()) {
                throw new RangeError('Attempt to allocate Buffer larger than maximum ' +
                                     'size: 0x' + kMaxLength().toString(16) + ' bytes')
              }
              return length | 0
            }
            Buffer$1.isBuffer = isBuffer;
            function internalIsBuffer (b) {
              return !!(b != null && b._isBuffer)
            }

            Buffer$1.compare = function compare (a, b) {
              if (!internalIsBuffer(a) || !internalIsBuffer(b)) {
                throw new TypeError('Arguments must be Buffers')
              }

              if (a === b) return 0

              var x = a.length;
              var y = b.length;

              for (var i = 0, len = Math.min(x, y); i < len; ++i) {
                if (a[i] !== b[i]) {
                  x = a[i];
                  y = b[i];
                  break
                }
              }

              if (x < y) return -1
              if (y < x) return 1
              return 0
            };

            Buffer$1.isEncoding = function isEncoding (encoding) {
              switch (String(encoding).toLowerCase()) {
                case 'hex':
                case 'utf8':
                case 'utf-8':
                case 'ascii':
                case 'latin1':
                case 'binary':
                case 'base64':
                case 'ucs2':
                case 'ucs-2':
                case 'utf16le':
                case 'utf-16le':
                  return true
                default:
                  return false
              }
            };

            Buffer$1.concat = function concat (list, length) {
              if (!isArray(list)) {
                throw new TypeError('"list" argument must be an Array of Buffers')
              }

              if (list.length === 0) {
                return Buffer$1.alloc(0)
              }

              var i;
              if (length === undefined) {
                length = 0;
                for (i = 0; i < list.length; ++i) {
                  length += list[i].length;
                }
              }

              var buffer = Buffer$1.allocUnsafe(length);
              var pos = 0;
              for (i = 0; i < list.length; ++i) {
                var buf = list[i];
                if (!internalIsBuffer(buf)) {
                  throw new TypeError('"list" argument must be an Array of Buffers')
                }
                buf.copy(buffer, pos);
                pos += buf.length;
              }
              return buffer
            };

            function byteLength (string, encoding) {
              if (internalIsBuffer(string)) {
                return string.length
              }
              if (typeof ArrayBuffer !== 'undefined' && typeof ArrayBuffer.isView === 'function' &&
                  (ArrayBuffer.isView(string) || string instanceof ArrayBuffer)) {
                return string.byteLength
              }
              if (typeof string !== 'string') {
                string = '' + string;
              }

              var len = string.length;
              if (len === 0) return 0

              // Use a for loop to avoid recursion
              var loweredCase = false;
              for (;;) {
                switch (encoding) {
                  case 'ascii':
                  case 'latin1':
                  case 'binary':
                    return len
                  case 'utf8':
                  case 'utf-8':
                  case undefined:
                    return utf8ToBytes(string).length
                  case 'ucs2':
                  case 'ucs-2':
                  case 'utf16le':
                  case 'utf-16le':
                    return len * 2
                  case 'hex':
                    return len >>> 1
                  case 'base64':
                    return base64ToBytes(string).length
                  default:
                    if (loweredCase) return utf8ToBytes(string).length // assume utf8
                    encoding = ('' + encoding).toLowerCase();
                    loweredCase = true;
                }
              }
            }
            Buffer$1.byteLength = byteLength;

            function slowToString (encoding, start, end) {
              var loweredCase = false;

              // No need to verify that "this.length <= MAX_UINT32" since it's a read-only
              // property of a typed array.

              // This behaves neither like String nor Uint8Array in that we set start/end
              // to their upper/lower bounds if the value passed is out of range.
              // undefined is handled specially as per ECMA-262 6th Edition,
              // Section 13.3.3.7 Runtime Semantics: KeyedBindingInitialization.
              if (start === undefined || start < 0) {
                start = 0;
              }
              // Return early if start > this.length. Done here to prevent potential uint32
              // coercion fail below.
              if (start > this.length) {
                return ''
              }

              if (end === undefined || end > this.length) {
                end = this.length;
              }

              if (end <= 0) {
                return ''
              }

              // Force coersion to uint32. This will also coerce falsey/NaN values to 0.
              end >>>= 0;
              start >>>= 0;

              if (end <= start) {
                return ''
              }

              if (!encoding) encoding = 'utf8';

              while (true) {
                switch (encoding) {
                  case 'hex':
                    return hexSlice(this, start, end)

                  case 'utf8':
                  case 'utf-8':
                    return utf8Slice(this, start, end)

                  case 'ascii':
                    return asciiSlice(this, start, end)

                  case 'latin1':
                  case 'binary':
                    return latin1Slice(this, start, end)

                  case 'base64':
                    return base64Slice(this, start, end)

                  case 'ucs2':
                  case 'ucs-2':
                  case 'utf16le':
                  case 'utf-16le':
                    return utf16leSlice(this, start, end)

                  default:
                    if (loweredCase) throw new TypeError('Unknown encoding: ' + encoding)
                    encoding = (encoding + '').toLowerCase();
                    loweredCase = true;
                }
              }
            }

            // The property is used by `Buffer.isBuffer` and `is-buffer` (in Safari 5-7) to detect
            // Buffer instances.
            Buffer$1.prototype._isBuffer = true;

            function swap (b, n, m) {
              var i = b[n];
              b[n] = b[m];
              b[m] = i;
            }

            Buffer$1.prototype.swap16 = function swap16 () {
              var len = this.length;
              if (len % 2 !== 0) {
                throw new RangeError('Buffer size must be a multiple of 16-bits')
              }
              for (var i = 0; i < len; i += 2) {
                swap(this, i, i + 1);
              }
              return this
            };

            Buffer$1.prototype.swap32 = function swap32 () {
              var len = this.length;
              if (len % 4 !== 0) {
                throw new RangeError('Buffer size must be a multiple of 32-bits')
              }
              for (var i = 0; i < len; i += 4) {
                swap(this, i, i + 3);
                swap(this, i + 1, i + 2);
              }
              return this
            };

            Buffer$1.prototype.swap64 = function swap64 () {
              var len = this.length;
              if (len % 8 !== 0) {
                throw new RangeError('Buffer size must be a multiple of 64-bits')
              }
              for (var i = 0; i < len; i += 8) {
                swap(this, i, i + 7);
                swap(this, i + 1, i + 6);
                swap(this, i + 2, i + 5);
                swap(this, i + 3, i + 4);
              }
              return this
            };

            Buffer$1.prototype.toString = function toString () {
              var length = this.length | 0;
              if (length === 0) return ''
              if (arguments.length === 0) return utf8Slice(this, 0, length)
              return slowToString.apply(this, arguments)
            };

            Buffer$1.prototype.equals = function equals (b) {
              if (!internalIsBuffer(b)) throw new TypeError('Argument must be a Buffer')
              if (this === b) return true
              return Buffer$1.compare(this, b) === 0
            };

            Buffer$1.prototype.inspect = function inspect () {
              var str = '';
              var max = INSPECT_MAX_BYTES;
              if (this.length > 0) {
                str = this.toString('hex', 0, max).match(/.{2}/g).join(' ');
                if (this.length > max) str += ' ... ';
              }
              return '<Buffer ' + str + '>'
            };

            Buffer$1.prototype.compare = function compare (target, start, end, thisStart, thisEnd) {
              if (!internalIsBuffer(target)) {
                throw new TypeError('Argument must be a Buffer')
              }

              if (start === undefined) {
                start = 0;
              }
              if (end === undefined) {
                end = target ? target.length : 0;
              }
              if (thisStart === undefined) {
                thisStart = 0;
              }
              if (thisEnd === undefined) {
                thisEnd = this.length;
              }

              if (start < 0 || end > target.length || thisStart < 0 || thisEnd > this.length) {
                throw new RangeError('out of range index')
              }

              if (thisStart >= thisEnd && start >= end) {
                return 0
              }
              if (thisStart >= thisEnd) {
                return -1
              }
              if (start >= end) {
                return 1
              }

              start >>>= 0;
              end >>>= 0;
              thisStart >>>= 0;
              thisEnd >>>= 0;

              if (this === target) return 0

              var x = thisEnd - thisStart;
              var y = end - start;
              var len = Math.min(x, y);

              var thisCopy = this.slice(thisStart, thisEnd);
              var targetCopy = target.slice(start, end);

              for (var i = 0; i < len; ++i) {
                if (thisCopy[i] !== targetCopy[i]) {
                  x = thisCopy[i];
                  y = targetCopy[i];
                  break
                }
              }

              if (x < y) return -1
              if (y < x) return 1
              return 0
            };

            // Finds either the first index of `val` in `buffer` at offset >= `byteOffset`,
            // OR the last index of `val` in `buffer` at offset <= `byteOffset`.
            //
            // Arguments:
            // - buffer - a Buffer to search
            // - val - a string, Buffer, or number
            // - byteOffset - an index into `buffer`; will be clamped to an int32
            // - encoding - an optional encoding, relevant is val is a string
            // - dir - true for indexOf, false for lastIndexOf
            function bidirectionalIndexOf (buffer, val, byteOffset, encoding, dir) {
              // Empty buffer means no match
              if (buffer.length === 0) return -1

              // Normalize byteOffset
              if (typeof byteOffset === 'string') {
                encoding = byteOffset;
                byteOffset = 0;
              } else if (byteOffset > 0x7fffffff) {
                byteOffset = 0x7fffffff;
              } else if (byteOffset < -0x80000000) {
                byteOffset = -0x80000000;
              }
              byteOffset = +byteOffset;  // Coerce to Number.
              if (isNaN(byteOffset)) {
                // byteOffset: it it's undefined, null, NaN, "foo", etc, search whole buffer
                byteOffset = dir ? 0 : (buffer.length - 1);
              }

              // Normalize byteOffset: negative offsets start from the end of the buffer
              if (byteOffset < 0) byteOffset = buffer.length + byteOffset;
              if (byteOffset >= buffer.length) {
                if (dir) return -1
                else byteOffset = buffer.length - 1;
              } else if (byteOffset < 0) {
                if (dir) byteOffset = 0;
                else return -1
              }

              // Normalize val
              if (typeof val === 'string') {
                val = Buffer$1.from(val, encoding);
              }

              // Finally, search either indexOf (if dir is true) or lastIndexOf
              if (internalIsBuffer(val)) {
                // Special case: looking for empty string/buffer always fails
                if (val.length === 0) {
                  return -1
                }
                return arrayIndexOf(buffer, val, byteOffset, encoding, dir)
              } else if (typeof val === 'number') {
                val = val & 0xFF; // Search for a byte value [0-255]
                if (Buffer$1.TYPED_ARRAY_SUPPORT &&
                    typeof Uint8Array.prototype.indexOf === 'function') {
                  if (dir) {
                    return Uint8Array.prototype.indexOf.call(buffer, val, byteOffset)
                  } else {
                    return Uint8Array.prototype.lastIndexOf.call(buffer, val, byteOffset)
                  }
                }
                return arrayIndexOf(buffer, [ val ], byteOffset, encoding, dir)
              }

              throw new TypeError('val must be string, number or Buffer')
            }

            function arrayIndexOf (arr, val, byteOffset, encoding, dir) {
              var indexSize = 1;
              var arrLength = arr.length;
              var valLength = val.length;

              if (encoding !== undefined) {
                encoding = String(encoding).toLowerCase();
                if (encoding === 'ucs2' || encoding === 'ucs-2' ||
                    encoding === 'utf16le' || encoding === 'utf-16le') {
                  if (arr.length < 2 || val.length < 2) {
                    return -1
                  }
                  indexSize = 2;
                  arrLength /= 2;
                  valLength /= 2;
                  byteOffset /= 2;
                }
              }

              function read (buf, i) {
                if (indexSize === 1) {
                  return buf[i]
                } else {
                  return buf.readUInt16BE(i * indexSize)
                }
              }

              var i;
              if (dir) {
                var foundIndex = -1;
                for (i = byteOffset; i < arrLength; i++) {
                  if (read(arr, i) === read(val, foundIndex === -1 ? 0 : i - foundIndex)) {
                    if (foundIndex === -1) foundIndex = i;
                    if (i - foundIndex + 1 === valLength) return foundIndex * indexSize
                  } else {
                    if (foundIndex !== -1) i -= i - foundIndex;
                    foundIndex = -1;
                  }
                }
              } else {
                if (byteOffset + valLength > arrLength) byteOffset = arrLength - valLength;
                for (i = byteOffset; i >= 0; i--) {
                  var found = true;
                  for (var j = 0; j < valLength; j++) {
                    if (read(arr, i + j) !== read(val, j)) {
                      found = false;
                      break
                    }
                  }
                  if (found) return i
                }
              }

              return -1
            }

            Buffer$1.prototype.includes = function includes (val, byteOffset, encoding) {
              return this.indexOf(val, byteOffset, encoding) !== -1
            };

            Buffer$1.prototype.indexOf = function indexOf (val, byteOffset, encoding) {
              return bidirectionalIndexOf(this, val, byteOffset, encoding, true)
            };

            Buffer$1.prototype.lastIndexOf = function lastIndexOf (val, byteOffset, encoding) {
              return bidirectionalIndexOf(this, val, byteOffset, encoding, false)
            };

            function hexWrite (buf, string, offset, length) {
              offset = Number(offset) || 0;
              var remaining = buf.length - offset;
              if (!length) {
                length = remaining;
              } else {
                length = Number(length);
                if (length > remaining) {
                  length = remaining;
                }
              }

              // must be an even number of digits
              var strLen = string.length;
              if (strLen % 2 !== 0) throw new TypeError('Invalid hex string')

              if (length > strLen / 2) {
                length = strLen / 2;
              }
              for (var i = 0; i < length; ++i) {
                var parsed = parseInt(string.substr(i * 2, 2), 16);
                if (isNaN(parsed)) return i
                buf[offset + i] = parsed;
              }
              return i
            }

            function utf8Write (buf, string, offset, length) {
              return blitBuffer(utf8ToBytes(string, buf.length - offset), buf, offset, length)
            }

            function asciiWrite (buf, string, offset, length) {
              return blitBuffer(asciiToBytes(string), buf, offset, length)
            }

            function latin1Write (buf, string, offset, length) {
              return asciiWrite(buf, string, offset, length)
            }

            function base64Write (buf, string, offset, length) {
              return blitBuffer(base64ToBytes(string), buf, offset, length)
            }

            function ucs2Write (buf, string, offset, length) {
              return blitBuffer(utf16leToBytes(string, buf.length - offset), buf, offset, length)
            }

            Buffer$1.prototype.write = function write (string, offset, length, encoding) {
              // Buffer#write(string)
              if (offset === undefined) {
                encoding = 'utf8';
                length = this.length;
                offset = 0;
              // Buffer#write(string, encoding)
              } else if (length === undefined && typeof offset === 'string') {
                encoding = offset;
                length = this.length;
                offset = 0;
              // Buffer#write(string, offset[, length][, encoding])
              } else if (isFinite(offset)) {
                offset = offset | 0;
                if (isFinite(length)) {
                  length = length | 0;
                  if (encoding === undefined) encoding = 'utf8';
                } else {
                  encoding = length;
                  length = undefined;
                }
              // legacy write(string, encoding, offset, length) - remove in v0.13
              } else {
                throw new Error(
                  'Buffer.write(string, encoding, offset[, length]) is no longer supported'
                )
              }

              var remaining = this.length - offset;
              if (length === undefined || length > remaining) length = remaining;

              if ((string.length > 0 && (length < 0 || offset < 0)) || offset > this.length) {
                throw new RangeError('Attempt to write outside buffer bounds')
              }

              if (!encoding) encoding = 'utf8';

              var loweredCase = false;
              for (;;) {
                switch (encoding) {
                  case 'hex':
                    return hexWrite(this, string, offset, length)

                  case 'utf8':
                  case 'utf-8':
                    return utf8Write(this, string, offset, length)

                  case 'ascii':
                    return asciiWrite(this, string, offset, length)

                  case 'latin1':
                  case 'binary':
                    return latin1Write(this, string, offset, length)

                  case 'base64':
                    // Warning: maxLength not taken into account in base64Write
                    return base64Write(this, string, offset, length)

                  case 'ucs2':
                  case 'ucs-2':
                  case 'utf16le':
                  case 'utf-16le':
                    return ucs2Write(this, string, offset, length)

                  default:
                    if (loweredCase) throw new TypeError('Unknown encoding: ' + encoding)
                    encoding = ('' + encoding).toLowerCase();
                    loweredCase = true;
                }
              }
            };

            Buffer$1.prototype.toJSON = function toJSON () {
              return {
                type: 'Buffer',
                data: Array.prototype.slice.call(this._arr || this, 0)
              }
            };

            function base64Slice (buf, start, end) {
              if (start === 0 && end === buf.length) {
                return fromByteArray(buf)
              } else {
                return fromByteArray(buf.slice(start, end))
              }
            }

            function utf8Slice (buf, start, end) {
              end = Math.min(buf.length, end);
              var res = [];

              var i = start;
              while (i < end) {
                var firstByte = buf[i];
                var codePoint = null;
                var bytesPerSequence = (firstByte > 0xEF) ? 4
                  : (firstByte > 0xDF) ? 3
                  : (firstByte > 0xBF) ? 2
                  : 1;

                if (i + bytesPerSequence <= end) {
                  var secondByte, thirdByte, fourthByte, tempCodePoint;

                  switch (bytesPerSequence) {
                    case 1:
                      if (firstByte < 0x80) {
                        codePoint = firstByte;
                      }
                      break
                    case 2:
                      secondByte = buf[i + 1];
                      if ((secondByte & 0xC0) === 0x80) {
                        tempCodePoint = (firstByte & 0x1F) << 0x6 | (secondByte & 0x3F);
                        if (tempCodePoint > 0x7F) {
                          codePoint = tempCodePoint;
                        }
                      }
                      break
                    case 3:
                      secondByte = buf[i + 1];
                      thirdByte = buf[i + 2];
                      if ((secondByte & 0xC0) === 0x80 && (thirdByte & 0xC0) === 0x80) {
                        tempCodePoint = (firstByte & 0xF) << 0xC | (secondByte & 0x3F) << 0x6 | (thirdByte & 0x3F);
                        if (tempCodePoint > 0x7FF && (tempCodePoint < 0xD800 || tempCodePoint > 0xDFFF)) {
                          codePoint = tempCodePoint;
                        }
                      }
                      break
                    case 4:
                      secondByte = buf[i + 1];
                      thirdByte = buf[i + 2];
                      fourthByte = buf[i + 3];
                      if ((secondByte & 0xC0) === 0x80 && (thirdByte & 0xC0) === 0x80 && (fourthByte & 0xC0) === 0x80) {
                        tempCodePoint = (firstByte & 0xF) << 0x12 | (secondByte & 0x3F) << 0xC | (thirdByte & 0x3F) << 0x6 | (fourthByte & 0x3F);
                        if (tempCodePoint > 0xFFFF && tempCodePoint < 0x110000) {
                          codePoint = tempCodePoint;
                        }
                      }
                  }
                }

                if (codePoint === null) {
                  // we did not generate a valid codePoint so insert a
                  // replacement char (U+FFFD) and advance only 1 byte
                  codePoint = 0xFFFD;
                  bytesPerSequence = 1;
                } else if (codePoint > 0xFFFF) {
                  // encode to utf16 (surrogate pair dance)
                  codePoint -= 0x10000;
                  res.push(codePoint >>> 10 & 0x3FF | 0xD800);
                  codePoint = 0xDC00 | codePoint & 0x3FF;
                }

                res.push(codePoint);
                i += bytesPerSequence;
              }

              return decodeCodePointsArray(res)
            }

            // Based on http://stackoverflow.com/a/22747272/680742, the browser with
            // the lowest limit is Chrome, with 0x10000 args.
            // We go 1 magnitude less, for safety
            var MAX_ARGUMENTS_LENGTH = 0x1000;

            function decodeCodePointsArray (codePoints) {
              var len = codePoints.length;
              if (len <= MAX_ARGUMENTS_LENGTH) {
                return String.fromCharCode.apply(String, codePoints) // avoid extra slice()
              }

              // Decode in chunks to avoid "call stack size exceeded".
              var res = '';
              var i = 0;
              while (i < len) {
                res += String.fromCharCode.apply(
                  String,
                  codePoints.slice(i, i += MAX_ARGUMENTS_LENGTH)
                );
              }
              return res
            }

            function asciiSlice (buf, start, end) {
              var ret = '';
              end = Math.min(buf.length, end);

              for (var i = start; i < end; ++i) {
                ret += String.fromCharCode(buf[i] & 0x7F);
              }
              return ret
            }

            function latin1Slice (buf, start, end) {
              var ret = '';
              end = Math.min(buf.length, end);

              for (var i = start; i < end; ++i) {
                ret += String.fromCharCode(buf[i]);
              }
              return ret
            }

            function hexSlice (buf, start, end) {
              var len = buf.length;

              if (!start || start < 0) start = 0;
              if (!end || end < 0 || end > len) end = len;

              var out = '';
              for (var i = start; i < end; ++i) {
                out += toHex(buf[i]);
              }
              return out
            }

            function utf16leSlice (buf, start, end) {
              var bytes = buf.slice(start, end);
              var res = '';
              for (var i = 0; i < bytes.length; i += 2) {
                res += String.fromCharCode(bytes[i] + bytes[i + 1] * 256);
              }
              return res
            }

            Buffer$1.prototype.slice = function slice (start, end) {
              var len = this.length;
              start = ~~start;
              end = end === undefined ? len : ~~end;

              if (start < 0) {
                start += len;
                if (start < 0) start = 0;
              } else if (start > len) {
                start = len;
              }

              if (end < 0) {
                end += len;
                if (end < 0) end = 0;
              } else if (end > len) {
                end = len;
              }

              if (end < start) end = start;

              var newBuf;
              if (Buffer$1.TYPED_ARRAY_SUPPORT) {
                newBuf = this.subarray(start, end);
                newBuf.__proto__ = Buffer$1.prototype;
              } else {
                var sliceLen = end - start;
                newBuf = new Buffer$1(sliceLen, undefined);
                for (var i = 0; i < sliceLen; ++i) {
                  newBuf[i] = this[i + start];
                }
              }

              return newBuf
            };

            /*
             * Need to make sure that buffer isn't trying to write out of bounds.
             */
            function checkOffset (offset, ext, length) {
              if ((offset % 1) !== 0 || offset < 0) throw new RangeError('offset is not uint')
              if (offset + ext > length) throw new RangeError('Trying to access beyond buffer length')
            }

            Buffer$1.prototype.readUIntLE = function readUIntLE (offset, byteLength, noAssert) {
              offset = offset | 0;
              byteLength = byteLength | 0;
              if (!noAssert) checkOffset(offset, byteLength, this.length);

              var val = this[offset];
              var mul = 1;
              var i = 0;
              while (++i < byteLength && (mul *= 0x100)) {
                val += this[offset + i] * mul;
              }

              return val
            };

            Buffer$1.prototype.readUIntBE = function readUIntBE (offset, byteLength, noAssert) {
              offset = offset | 0;
              byteLength = byteLength | 0;
              if (!noAssert) {
                checkOffset(offset, byteLength, this.length);
              }

              var val = this[offset + --byteLength];
              var mul = 1;
              while (byteLength > 0 && (mul *= 0x100)) {
                val += this[offset + --byteLength] * mul;
              }

              return val
            };

            Buffer$1.prototype.readUInt8 = function readUInt8 (offset, noAssert) {
              if (!noAssert) checkOffset(offset, 1, this.length);
              return this[offset]
            };

            Buffer$1.prototype.readUInt16LE = function readUInt16LE (offset, noAssert) {
              if (!noAssert) checkOffset(offset, 2, this.length);
              return this[offset] | (this[offset + 1] << 8)
            };

            Buffer$1.prototype.readUInt16BE = function readUInt16BE (offset, noAssert) {
              if (!noAssert) checkOffset(offset, 2, this.length);
              return (this[offset] << 8) | this[offset + 1]
            };

            Buffer$1.prototype.readUInt32LE = function readUInt32LE (offset, noAssert) {
              if (!noAssert) checkOffset(offset, 4, this.length);

              return ((this[offset]) |
                  (this[offset + 1] << 8) |
                  (this[offset + 2] << 16)) +
                  (this[offset + 3] * 0x1000000)
            };

            Buffer$1.prototype.readUInt32BE = function readUInt32BE (offset, noAssert) {
              if (!noAssert) checkOffset(offset, 4, this.length);

              return (this[offset] * 0x1000000) +
                ((this[offset + 1] << 16) |
                (this[offset + 2] << 8) |
                this[offset + 3])
            };

            Buffer$1.prototype.readIntLE = function readIntLE (offset, byteLength, noAssert) {
              offset = offset | 0;
              byteLength = byteLength | 0;
              if (!noAssert) checkOffset(offset, byteLength, this.length);

              var val = this[offset];
              var mul = 1;
              var i = 0;
              while (++i < byteLength && (mul *= 0x100)) {
                val += this[offset + i] * mul;
              }
              mul *= 0x80;

              if (val >= mul) val -= Math.pow(2, 8 * byteLength);

              return val
            };

            Buffer$1.prototype.readIntBE = function readIntBE (offset, byteLength, noAssert) {
              offset = offset | 0;
              byteLength = byteLength | 0;
              if (!noAssert) checkOffset(offset, byteLength, this.length);

              var i = byteLength;
              var mul = 1;
              var val = this[offset + --i];
              while (i > 0 && (mul *= 0x100)) {
                val += this[offset + --i] * mul;
              }
              mul *= 0x80;

              if (val >= mul) val -= Math.pow(2, 8 * byteLength);

              return val
            };

            Buffer$1.prototype.readInt8 = function readInt8 (offset, noAssert) {
              if (!noAssert) checkOffset(offset, 1, this.length);
              if (!(this[offset] & 0x80)) return (this[offset])
              return ((0xff - this[offset] + 1) * -1)
            };

            Buffer$1.prototype.readInt16LE = function readInt16LE (offset, noAssert) {
              if (!noAssert) checkOffset(offset, 2, this.length);
              var val = this[offset] | (this[offset + 1] << 8);
              return (val & 0x8000) ? val | 0xFFFF0000 : val
            };

            Buffer$1.prototype.readInt16BE = function readInt16BE (offset, noAssert) {
              if (!noAssert) checkOffset(offset, 2, this.length);
              var val = this[offset + 1] | (this[offset] << 8);
              return (val & 0x8000) ? val | 0xFFFF0000 : val
            };

            Buffer$1.prototype.readInt32LE = function readInt32LE (offset, noAssert) {
              if (!noAssert) checkOffset(offset, 4, this.length);

              return (this[offset]) |
                (this[offset + 1] << 8) |
                (this[offset + 2] << 16) |
                (this[offset + 3] << 24)
            };

            Buffer$1.prototype.readInt32BE = function readInt32BE (offset, noAssert) {
              if (!noAssert) checkOffset(offset, 4, this.length);

              return (this[offset] << 24) |
                (this[offset + 1] << 16) |
                (this[offset + 2] << 8) |
                (this[offset + 3])
            };

            Buffer$1.prototype.readFloatLE = function readFloatLE (offset, noAssert) {
              if (!noAssert) checkOffset(offset, 4, this.length);
              return read(this, offset, true, 23, 4)
            };

            Buffer$1.prototype.readFloatBE = function readFloatBE (offset, noAssert) {
              if (!noAssert) checkOffset(offset, 4, this.length);
              return read(this, offset, false, 23, 4)
            };

            Buffer$1.prototype.readDoubleLE = function readDoubleLE (offset, noAssert) {
              if (!noAssert) checkOffset(offset, 8, this.length);
              return read(this, offset, true, 52, 8)
            };

            Buffer$1.prototype.readDoubleBE = function readDoubleBE (offset, noAssert) {
              if (!noAssert) checkOffset(offset, 8, this.length);
              return read(this, offset, false, 52, 8)
            };

            function checkInt (buf, value, offset, ext, max, min) {
              if (!internalIsBuffer(buf)) throw new TypeError('"buffer" argument must be a Buffer instance')
              if (value > max || value < min) throw new RangeError('"value" argument is out of bounds')
              if (offset + ext > buf.length) throw new RangeError('Index out of range')
            }

            Buffer$1.prototype.writeUIntLE = function writeUIntLE (value, offset, byteLength, noAssert) {
              value = +value;
              offset = offset | 0;
              byteLength = byteLength | 0;
              if (!noAssert) {
                var maxBytes = Math.pow(2, 8 * byteLength) - 1;
                checkInt(this, value, offset, byteLength, maxBytes, 0);
              }

              var mul = 1;
              var i = 0;
              this[offset] = value & 0xFF;
              while (++i < byteLength && (mul *= 0x100)) {
                this[offset + i] = (value / mul) & 0xFF;
              }

              return offset + byteLength
            };

            Buffer$1.prototype.writeUIntBE = function writeUIntBE (value, offset, byteLength, noAssert) {
              value = +value;
              offset = offset | 0;
              byteLength = byteLength | 0;
              if (!noAssert) {
                var maxBytes = Math.pow(2, 8 * byteLength) - 1;
                checkInt(this, value, offset, byteLength, maxBytes, 0);
              }

              var i = byteLength - 1;
              var mul = 1;
              this[offset + i] = value & 0xFF;
              while (--i >= 0 && (mul *= 0x100)) {
                this[offset + i] = (value / mul) & 0xFF;
              }

              return offset + byteLength
            };

            Buffer$1.prototype.writeUInt8 = function writeUInt8 (value, offset, noAssert) {
              value = +value;
              offset = offset | 0;
              if (!noAssert) checkInt(this, value, offset, 1, 0xff, 0);
              if (!Buffer$1.TYPED_ARRAY_SUPPORT) value = Math.floor(value);
              this[offset] = (value & 0xff);
              return offset + 1
            };

            function objectWriteUInt16 (buf, value, offset, littleEndian) {
              if (value < 0) value = 0xffff + value + 1;
              for (var i = 0, j = Math.min(buf.length - offset, 2); i < j; ++i) {
                buf[offset + i] = (value & (0xff << (8 * (littleEndian ? i : 1 - i)))) >>>
                  (littleEndian ? i : 1 - i) * 8;
              }
            }

            Buffer$1.prototype.writeUInt16LE = function writeUInt16LE (value, offset, noAssert) {
              value = +value;
              offset = offset | 0;
              if (!noAssert) checkInt(this, value, offset, 2, 0xffff, 0);
              if (Buffer$1.TYPED_ARRAY_SUPPORT) {
                this[offset] = (value & 0xff);
                this[offset + 1] = (value >>> 8);
              } else {
                objectWriteUInt16(this, value, offset, true);
              }
              return offset + 2
            };

            Buffer$1.prototype.writeUInt16BE = function writeUInt16BE (value, offset, noAssert) {
              value = +value;
              offset = offset | 0;
              if (!noAssert) checkInt(this, value, offset, 2, 0xffff, 0);
              if (Buffer$1.TYPED_ARRAY_SUPPORT) {
                this[offset] = (value >>> 8);
                this[offset + 1] = (value & 0xff);
              } else {
                objectWriteUInt16(this, value, offset, false);
              }
              return offset + 2
            };

            function objectWriteUInt32 (buf, value, offset, littleEndian) {
              if (value < 0) value = 0xffffffff + value + 1;
              for (var i = 0, j = Math.min(buf.length - offset, 4); i < j; ++i) {
                buf[offset + i] = (value >>> (littleEndian ? i : 3 - i) * 8) & 0xff;
              }
            }

            Buffer$1.prototype.writeUInt32LE = function writeUInt32LE (value, offset, noAssert) {
              value = +value;
              offset = offset | 0;
              if (!noAssert) checkInt(this, value, offset, 4, 0xffffffff, 0);
              if (Buffer$1.TYPED_ARRAY_SUPPORT) {
                this[offset + 3] = (value >>> 24);
                this[offset + 2] = (value >>> 16);
                this[offset + 1] = (value >>> 8);
                this[offset] = (value & 0xff);
              } else {
                objectWriteUInt32(this, value, offset, true);
              }
              return offset + 4
            };

            Buffer$1.prototype.writeUInt32BE = function writeUInt32BE (value, offset, noAssert) {
              value = +value;
              offset = offset | 0;
              if (!noAssert) checkInt(this, value, offset, 4, 0xffffffff, 0);
              if (Buffer$1.TYPED_ARRAY_SUPPORT) {
                this[offset] = (value >>> 24);
                this[offset + 1] = (value >>> 16);
                this[offset + 2] = (value >>> 8);
                this[offset + 3] = (value & 0xff);
              } else {
                objectWriteUInt32(this, value, offset, false);
              }
              return offset + 4
            };

            Buffer$1.prototype.writeIntLE = function writeIntLE (value, offset, byteLength, noAssert) {
              value = +value;
              offset = offset | 0;
              if (!noAssert) {
                var limit = Math.pow(2, 8 * byteLength - 1);

                checkInt(this, value, offset, byteLength, limit - 1, -limit);
              }

              var i = 0;
              var mul = 1;
              var sub = 0;
              this[offset] = value & 0xFF;
              while (++i < byteLength && (mul *= 0x100)) {
                if (value < 0 && sub === 0 && this[offset + i - 1] !== 0) {
                  sub = 1;
                }
                this[offset + i] = ((value / mul) >> 0) - sub & 0xFF;
              }

              return offset + byteLength
            };

            Buffer$1.prototype.writeIntBE = function writeIntBE (value, offset, byteLength, noAssert) {
              value = +value;
              offset = offset | 0;
              if (!noAssert) {
                var limit = Math.pow(2, 8 * byteLength - 1);

                checkInt(this, value, offset, byteLength, limit - 1, -limit);
              }

              var i = byteLength - 1;
              var mul = 1;
              var sub = 0;
              this[offset + i] = value & 0xFF;
              while (--i >= 0 && (mul *= 0x100)) {
                if (value < 0 && sub === 0 && this[offset + i + 1] !== 0) {
                  sub = 1;
                }
                this[offset + i] = ((value / mul) >> 0) - sub & 0xFF;
              }

              return offset + byteLength
            };

            Buffer$1.prototype.writeInt8 = function writeInt8 (value, offset, noAssert) {
              value = +value;
              offset = offset | 0;
              if (!noAssert) checkInt(this, value, offset, 1, 0x7f, -0x80);
              if (!Buffer$1.TYPED_ARRAY_SUPPORT) value = Math.floor(value);
              if (value < 0) value = 0xff + value + 1;
              this[offset] = (value & 0xff);
              return offset + 1
            };

            Buffer$1.prototype.writeInt16LE = function writeInt16LE (value, offset, noAssert) {
              value = +value;
              offset = offset | 0;
              if (!noAssert) checkInt(this, value, offset, 2, 0x7fff, -0x8000);
              if (Buffer$1.TYPED_ARRAY_SUPPORT) {
                this[offset] = (value & 0xff);
                this[offset + 1] = (value >>> 8);
              } else {
                objectWriteUInt16(this, value, offset, true);
              }
              return offset + 2
            };

            Buffer$1.prototype.writeInt16BE = function writeInt16BE (value, offset, noAssert) {
              value = +value;
              offset = offset | 0;
              if (!noAssert) checkInt(this, value, offset, 2, 0x7fff, -0x8000);
              if (Buffer$1.TYPED_ARRAY_SUPPORT) {
                this[offset] = (value >>> 8);
                this[offset + 1] = (value & 0xff);
              } else {
                objectWriteUInt16(this, value, offset, false);
              }
              return offset + 2
            };

            Buffer$1.prototype.writeInt32LE = function writeInt32LE (value, offset, noAssert) {
              value = +value;
              offset = offset | 0;
              if (!noAssert) checkInt(this, value, offset, 4, 0x7fffffff, -0x80000000);
              if (Buffer$1.TYPED_ARRAY_SUPPORT) {
                this[offset] = (value & 0xff);
                this[offset + 1] = (value >>> 8);
                this[offset + 2] = (value >>> 16);
                this[offset + 3] = (value >>> 24);
              } else {
                objectWriteUInt32(this, value, offset, true);
              }
              return offset + 4
            };

            Buffer$1.prototype.writeInt32BE = function writeInt32BE (value, offset, noAssert) {
              value = +value;
              offset = offset | 0;
              if (!noAssert) checkInt(this, value, offset, 4, 0x7fffffff, -0x80000000);
              if (value < 0) value = 0xffffffff + value + 1;
              if (Buffer$1.TYPED_ARRAY_SUPPORT) {
                this[offset] = (value >>> 24);
                this[offset + 1] = (value >>> 16);
                this[offset + 2] = (value >>> 8);
                this[offset + 3] = (value & 0xff);
              } else {
                objectWriteUInt32(this, value, offset, false);
              }
              return offset + 4
            };

            function checkIEEE754 (buf, value, offset, ext, max, min) {
              if (offset + ext > buf.length) throw new RangeError('Index out of range')
              if (offset < 0) throw new RangeError('Index out of range')
            }

            function writeFloat (buf, value, offset, littleEndian, noAssert) {
              if (!noAssert) {
                checkIEEE754(buf, value, offset, 4);
              }
              write(buf, value, offset, littleEndian, 23, 4);
              return offset + 4
            }

            Buffer$1.prototype.writeFloatLE = function writeFloatLE (value, offset, noAssert) {
              return writeFloat(this, value, offset, true, noAssert)
            };

            Buffer$1.prototype.writeFloatBE = function writeFloatBE (value, offset, noAssert) {
              return writeFloat(this, value, offset, false, noAssert)
            };

            function writeDouble (buf, value, offset, littleEndian, noAssert) {
              if (!noAssert) {
                checkIEEE754(buf, value, offset, 8);
              }
              write(buf, value, offset, littleEndian, 52, 8);
              return offset + 8
            }

            Buffer$1.prototype.writeDoubleLE = function writeDoubleLE (value, offset, noAssert) {
              return writeDouble(this, value, offset, true, noAssert)
            };

            Buffer$1.prototype.writeDoubleBE = function writeDoubleBE (value, offset, noAssert) {
              return writeDouble(this, value, offset, false, noAssert)
            };

            // copy(targetBuffer, targetStart=0, sourceStart=0, sourceEnd=buffer.length)
            Buffer$1.prototype.copy = function copy (target, targetStart, start, end) {
              if (!start) start = 0;
              if (!end && end !== 0) end = this.length;
              if (targetStart >= target.length) targetStart = target.length;
              if (!targetStart) targetStart = 0;
              if (end > 0 && end < start) end = start;

              // Copy 0 bytes; we're done
              if (end === start) return 0
              if (target.length === 0 || this.length === 0) return 0

              // Fatal error conditions
              if (targetStart < 0) {
                throw new RangeError('targetStart out of bounds')
              }
              if (start < 0 || start >= this.length) throw new RangeError('sourceStart out of bounds')
              if (end < 0) throw new RangeError('sourceEnd out of bounds')

              // Are we oob?
              if (end > this.length) end = this.length;
              if (target.length - targetStart < end - start) {
                end = target.length - targetStart + start;
              }

              var len = end - start;
              var i;

              if (this === target && start < targetStart && targetStart < end) {
                // descending copy from end
                for (i = len - 1; i >= 0; --i) {
                  target[i + targetStart] = this[i + start];
                }
              } else if (len < 1000 || !Buffer$1.TYPED_ARRAY_SUPPORT) {
                // ascending copy from start
                for (i = 0; i < len; ++i) {
                  target[i + targetStart] = this[i + start];
                }
              } else {
                Uint8Array.prototype.set.call(
                  target,
                  this.subarray(start, start + len),
                  targetStart
                );
              }

              return len
            };

            // Usage:
            //    buffer.fill(number[, offset[, end]])
            //    buffer.fill(buffer[, offset[, end]])
            //    buffer.fill(string[, offset[, end]][, encoding])
            Buffer$1.prototype.fill = function fill (val, start, end, encoding) {
              // Handle string cases:
              if (typeof val === 'string') {
                if (typeof start === 'string') {
                  encoding = start;
                  start = 0;
                  end = this.length;
                } else if (typeof end === 'string') {
                  encoding = end;
                  end = this.length;
                }
                if (val.length === 1) {
                  var code = val.charCodeAt(0);
                  if (code < 256) {
                    val = code;
                  }
                }
                if (encoding !== undefined && typeof encoding !== 'string') {
                  throw new TypeError('encoding must be a string')
                }
                if (typeof encoding === 'string' && !Buffer$1.isEncoding(encoding)) {
                  throw new TypeError('Unknown encoding: ' + encoding)
                }
              } else if (typeof val === 'number') {
                val = val & 255;
              }

              // Invalid ranges are not set to a default, so can range check early.
              if (start < 0 || this.length < start || this.length < end) {
                throw new RangeError('Out of range index')
              }

              if (end <= start) {
                return this
              }

              start = start >>> 0;
              end = end === undefined ? this.length : end >>> 0;

              if (!val) val = 0;

              var i;
              if (typeof val === 'number') {
                for (i = start; i < end; ++i) {
                  this[i] = val;
                }
              } else {
                var bytes = internalIsBuffer(val)
                  ? val
                  : utf8ToBytes(new Buffer$1(val, encoding).toString());
                var len = bytes.length;
                for (i = 0; i < end - start; ++i) {
                  this[i + start] = bytes[i % len];
                }
              }

              return this
            };

            // HELPER FUNCTIONS
            // ================

            var INVALID_BASE64_RE = /[^+\/0-9A-Za-z-_]/g;

            function base64clean (str) {
              // Node strips out invalid characters like \n and \t from the string, base64-js does not
              str = stringtrim(str).replace(INVALID_BASE64_RE, '');
              // Node converts strings with length < 2 to ''
              if (str.length < 2) return ''
              // Node allows for non-padded base64 strings (missing trailing ===), base64-js does not
              while (str.length % 4 !== 0) {
                str = str + '=';
              }
              return str
            }

            function stringtrim (str) {
              if (str.trim) return str.trim()
              return str.replace(/^\s+|\s+$/g, '')
            }

            function toHex (n) {
              if (n < 16) return '0' + n.toString(16)
              return n.toString(16)
            }

            function utf8ToBytes (string, units) {
              units = units || Infinity;
              var codePoint;
              var length = string.length;
              var leadSurrogate = null;
              var bytes = [];

              for (var i = 0; i < length; ++i) {
                codePoint = string.charCodeAt(i);

                // is surrogate component
                if (codePoint > 0xD7FF && codePoint < 0xE000) {
                  // last char was a lead
                  if (!leadSurrogate) {
                    // no lead yet
                    if (codePoint > 0xDBFF) {
                      // unexpected trail
                      if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD);
                      continue
                    } else if (i + 1 === length) {
                      // unpaired lead
                      if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD);
                      continue
                    }

                    // valid lead
                    leadSurrogate = codePoint;

                    continue
                  }

                  // 2 leads in a row
                  if (codePoint < 0xDC00) {
                    if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD);
                    leadSurrogate = codePoint;
                    continue
                  }

                  // valid surrogate pair
                  codePoint = (leadSurrogate - 0xD800 << 10 | codePoint - 0xDC00) + 0x10000;
                } else if (leadSurrogate) {
                  // valid bmp char, but last char was a lead
                  if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD);
                }

                leadSurrogate = null;

                // encode utf8
                if (codePoint < 0x80) {
                  if ((units -= 1) < 0) break
                  bytes.push(codePoint);
                } else if (codePoint < 0x800) {
                  if ((units -= 2) < 0) break
                  bytes.push(
                    codePoint >> 0x6 | 0xC0,
                    codePoint & 0x3F | 0x80
                  );
                } else if (codePoint < 0x10000) {
                  if ((units -= 3) < 0) break
                  bytes.push(
                    codePoint >> 0xC | 0xE0,
                    codePoint >> 0x6 & 0x3F | 0x80,
                    codePoint & 0x3F | 0x80
                  );
                } else if (codePoint < 0x110000) {
                  if ((units -= 4) < 0) break
                  bytes.push(
                    codePoint >> 0x12 | 0xF0,
                    codePoint >> 0xC & 0x3F | 0x80,
                    codePoint >> 0x6 & 0x3F | 0x80,
                    codePoint & 0x3F | 0x80
                  );
                } else {
                  throw new Error('Invalid code point')
                }
              }

              return bytes
            }

            function asciiToBytes (str) {
              var byteArray = [];
              for (var i = 0; i < str.length; ++i) {
                // Node's code seems to be doing this and not & 0x7F..
                byteArray.push(str.charCodeAt(i) & 0xFF);
              }
              return byteArray
            }

            function utf16leToBytes (str, units) {
              var c, hi, lo;
              var byteArray = [];
              for (var i = 0; i < str.length; ++i) {
                if ((units -= 2) < 0) break

                c = str.charCodeAt(i);
                hi = c >> 8;
                lo = c % 256;
                byteArray.push(lo);
                byteArray.push(hi);
              }

              return byteArray
            }


            function base64ToBytes (str) {
              return toByteArray(base64clean(str))
            }

            function blitBuffer (src, dst, offset, length) {
              for (var i = 0; i < length; ++i) {
                if ((i + offset >= dst.length) || (i >= src.length)) break
                dst[i + offset] = src[i];
              }
              return i
            }

            function isnan (val) {
              return val !== val // eslint-disable-line no-self-compare
            }


            // the following is from is-buffer, also by Feross Aboukhadijeh and with same lisence
            // The _isBuffer check is for Safari 5-7 support, because it's missing
            // Object.prototype.constructor. Remove this eventually
            function isBuffer(obj) {
              return obj != null && (!!obj._isBuffer || isFastBuffer(obj) || isSlowBuffer(obj))
            }

            function isFastBuffer (obj) {
              return !!obj.constructor && typeof obj.constructor.isBuffer === 'function' && obj.constructor.isBuffer(obj)
            }

            // For Node v0.10 support. Remove this eventually.
            function isSlowBuffer (obj) {
              return typeof obj.readFloatLE === 'function' && typeof obj.slice === 'function' && isFastBuffer(obj.slice(0, 0))
            }

            /*! https://mths.be/punycode v1.4.1 by @mathias */


            /** Highest positive signed 32-bit float value */
            var maxInt = 2147483647; // aka. 0x7FFFFFFF or 2^31-1

            /** Bootstring parameters */
            var base = 36;
            var tMin = 1;
            var tMax = 26;
            var skew = 38;
            var damp = 700;
            var initialBias = 72;
            var initialN = 128; // 0x80
            var delimiter = '-'; // '\x2D'

            /** Regular expressions */
            var regexPunycode = /^xn--/;
            var regexNonASCII = /[^\x20-\x7E]/; // unprintable ASCII chars + non-ASCII chars
            var regexSeparators = /[\x2E\u3002\uFF0E\uFF61]/g; // RFC 3490 separators

            /** Error messages */
            var errors = {
              'overflow': 'Overflow: input needs wider integers to process',
              'not-basic': 'Illegal input >= 0x80 (not a basic code point)',
              'invalid-input': 'Invalid input'
            };

            /** Convenience shortcuts */
            var baseMinusTMin = base - tMin;
            var floor = Math.floor;
            var stringFromCharCode = String.fromCharCode;

            /*--------------------------------------------------------------------------*/

            /**
             * A generic error utility function.
             * @private
             * @param {String} type The error type.
             * @returns {Error} Throws a `RangeError` with the applicable error message.
             */
            function error(type) {
              throw new RangeError(errors[type]);
            }

            /**
             * A generic `Array#map` utility function.
             * @private
             * @param {Array} array The array to iterate over.
             * @param {Function} callback The function that gets called for every array
             * item.
             * @returns {Array} A new array of values returned by the callback function.
             */
            function map(array, fn) {
              var length = array.length;
              var result = [];
              while (length--) {
                result[length] = fn(array[length]);
              }
              return result;
            }

            /**
             * A simple `Array#map`-like wrapper to work with domain name strings or email
             * addresses.
             * @private
             * @param {String} domain The domain name or email address.
             * @param {Function} callback The function that gets called for every
             * character.
             * @returns {Array} A new string of characters returned by the callback
             * function.
             */
            function mapDomain(string, fn) {
              var parts = string.split('@');
              var result = '';
              if (parts.length > 1) {
                // In email addresses, only the domain name should be punycoded. Leave
                // the local part (i.e. everything up to `@`) intact.
                result = parts[0] + '@';
                string = parts[1];
              }
              // Avoid `split(regex)` for IE8 compatibility. See #17.
              string = string.replace(regexSeparators, '\x2E');
              var labels = string.split('.');
              var encoded = map(labels, fn).join('.');
              return result + encoded;
            }

            /**
             * Creates an array containing the numeric code points of each Unicode
             * character in the string. While JavaScript uses UCS-2 internally,
             * this function will convert a pair of surrogate halves (each of which
             * UCS-2 exposes as separate characters) into a single code point,
             * matching UTF-16.
             * @see `punycode.ucs2.encode`
             * @see <https://mathiasbynens.be/notes/javascript-encoding>
             * @memberOf punycode.ucs2
             * @name decode
             * @param {String} string The Unicode input string (UCS-2).
             * @returns {Array} The new array of code points.
             */
            function ucs2decode(string) {
              var output = [],
                counter = 0,
                length = string.length,
                value,
                extra;
              while (counter < length) {
                value = string.charCodeAt(counter++);
                if (value >= 0xD800 && value <= 0xDBFF && counter < length) {
                  // high surrogate, and there is a next character
                  extra = string.charCodeAt(counter++);
                  if ((extra & 0xFC00) == 0xDC00) { // low surrogate
                    output.push(((value & 0x3FF) << 10) + (extra & 0x3FF) + 0x10000);
                  } else {
                    // unmatched surrogate; only append this code unit, in case the next
                    // code unit is the high surrogate of a surrogate pair
                    output.push(value);
                    counter--;
                  }
                } else {
                  output.push(value);
                }
              }
              return output;
            }

            /**
             * Creates a string based on an array of numeric code points.
             * @see `punycode.ucs2.decode`
             * @memberOf punycode.ucs2
             * @name encode
             * @param {Array} codePoints The array of numeric code points.
             * @returns {String} The new Unicode string (UCS-2).
             */
            function ucs2encode(array) {
              return map(array, function(value) {
                var output = '';
                if (value > 0xFFFF) {
                  value -= 0x10000;
                  output += stringFromCharCode(value >>> 10 & 0x3FF | 0xD800);
                  value = 0xDC00 | value & 0x3FF;
                }
                output += stringFromCharCode(value);
                return output;
              }).join('');
            }

            /**
             * Converts a basic code point into a digit/integer.
             * @see `digitToBasic()`
             * @private
             * @param {Number} codePoint The basic numeric code point value.
             * @returns {Number} The numeric value of a basic code point (for use in
             * representing integers) in the range `0` to `base - 1`, or `base` if
             * the code point does not represent a value.
             */
            function basicToDigit(codePoint) {
              if (codePoint - 48 < 10) {
                return codePoint - 22;
              }
              if (codePoint - 65 < 26) {
                return codePoint - 65;
              }
              if (codePoint - 97 < 26) {
                return codePoint - 97;
              }
              return base;
            }

            /**
             * Converts a digit/integer into a basic code point.
             * @see `basicToDigit()`
             * @private
             * @param {Number} digit The numeric value of a basic code point.
             * @returns {Number} The basic code point whose value (when used for
             * representing integers) is `digit`, which needs to be in the range
             * `0` to `base - 1`. If `flag` is non-zero, the uppercase form is
             * used; else, the lowercase form is used. The behavior is undefined
             * if `flag` is non-zero and `digit` has no uppercase form.
             */
            function digitToBasic(digit, flag) {
              //  0..25 map to ASCII a..z or A..Z
              // 26..35 map to ASCII 0..9
              return digit + 22 + 75 * (digit < 26) - ((flag != 0) << 5);
            }

            /**
             * Bias adaptation function as per section 3.4 of RFC 3492.
             * https://tools.ietf.org/html/rfc3492#section-3.4
             * @private
             */
            function adapt(delta, numPoints, firstTime) {
              var k = 0;
              delta = firstTime ? floor(delta / damp) : delta >> 1;
              delta += floor(delta / numPoints);
              for ( /* no initialization */ ; delta > baseMinusTMin * tMax >> 1; k += base) {
                delta = floor(delta / baseMinusTMin);
              }
              return floor(k + (baseMinusTMin + 1) * delta / (delta + skew));
            }

            /**
             * Converts a Punycode string of ASCII-only symbols to a string of Unicode
             * symbols.
             * @memberOf punycode
             * @param {String} input The Punycode string of ASCII-only symbols.
             * @returns {String} The resulting string of Unicode symbols.
             */
            function decode(input) {
              // Don't use UCS-2
              var output = [],
                inputLength = input.length,
                out,
                i = 0,
                n = initialN,
                bias = initialBias,
                basic,
                j,
                index,
                oldi,
                w,
                k,
                digit,
                t,
                /** Cached calculation results */
                baseMinusT;

              // Handle the basic code points: let `basic` be the number of input code
              // points before the last delimiter, or `0` if there is none, then copy
              // the first basic code points to the output.

              basic = input.lastIndexOf(delimiter);
              if (basic < 0) {
                basic = 0;
              }

              for (j = 0; j < basic; ++j) {
                // if it's not a basic code point
                if (input.charCodeAt(j) >= 0x80) {
                  error('not-basic');
                }
                output.push(input.charCodeAt(j));
              }

              // Main decoding loop: start just after the last delimiter if any basic code
              // points were copied; start at the beginning otherwise.

              for (index = basic > 0 ? basic + 1 : 0; index < inputLength; /* no final expression */ ) {

                // `index` is the index of the next character to be consumed.
                // Decode a generalized variable-length integer into `delta`,
                // which gets added to `i`. The overflow checking is easier
                // if we increase `i` as we go, then subtract off its starting
                // value at the end to obtain `delta`.
                for (oldi = i, w = 1, k = base; /* no condition */ ; k += base) {

                  if (index >= inputLength) {
                    error('invalid-input');
                  }

                  digit = basicToDigit(input.charCodeAt(index++));

                  if (digit >= base || digit > floor((maxInt - i) / w)) {
                    error('overflow');
                  }

                  i += digit * w;
                  t = k <= bias ? tMin : (k >= bias + tMax ? tMax : k - bias);

                  if (digit < t) {
                    break;
                  }

                  baseMinusT = base - t;
                  if (w > floor(maxInt / baseMinusT)) {
                    error('overflow');
                  }

                  w *= baseMinusT;

                }

                out = output.length + 1;
                bias = adapt(i - oldi, out, oldi == 0);

                // `i` was supposed to wrap around from `out` to `0`,
                // incrementing `n` each time, so we'll fix that now:
                if (floor(i / out) > maxInt - n) {
                  error('overflow');
                }

                n += floor(i / out);
                i %= out;

                // Insert `n` at position `i` of the output
                output.splice(i++, 0, n);

              }

              return ucs2encode(output);
            }

            /**
             * Converts a string of Unicode symbols (e.g. a domain name label) to a
             * Punycode string of ASCII-only symbols.
             * @memberOf punycode
             * @param {String} input The string of Unicode symbols.
             * @returns {String} The resulting Punycode string of ASCII-only symbols.
             */
            function encode(input) {
              var n,
                delta,
                handledCPCount,
                basicLength,
                bias,
                j,
                m,
                q,
                k,
                t,
                currentValue,
                output = [],
                /** `inputLength` will hold the number of code points in `input`. */
                inputLength,
                /** Cached calculation results */
                handledCPCountPlusOne,
                baseMinusT,
                qMinusT;

              // Convert the input in UCS-2 to Unicode
              input = ucs2decode(input);

              // Cache the length
              inputLength = input.length;

              // Initialize the state
              n = initialN;
              delta = 0;
              bias = initialBias;

              // Handle the basic code points
              for (j = 0; j < inputLength; ++j) {
                currentValue = input[j];
                if (currentValue < 0x80) {
                  output.push(stringFromCharCode(currentValue));
                }
              }

              handledCPCount = basicLength = output.length;

              // `handledCPCount` is the number of code points that have been handled;
              // `basicLength` is the number of basic code points.

              // Finish the basic string - if it is not empty - with a delimiter
              if (basicLength) {
                output.push(delimiter);
              }

              // Main encoding loop:
              while (handledCPCount < inputLength) {

                // All non-basic code points < n have been handled already. Find the next
                // larger one:
                for (m = maxInt, j = 0; j < inputLength; ++j) {
                  currentValue = input[j];
                  if (currentValue >= n && currentValue < m) {
                    m = currentValue;
                  }
                }

                // Increase `delta` enough to advance the decoder's <n,i> state to <m,0>,
                // but guard against overflow
                handledCPCountPlusOne = handledCPCount + 1;
                if (m - n > floor((maxInt - delta) / handledCPCountPlusOne)) {
                  error('overflow');
                }

                delta += (m - n) * handledCPCountPlusOne;
                n = m;

                for (j = 0; j < inputLength; ++j) {
                  currentValue = input[j];

                  if (currentValue < n && ++delta > maxInt) {
                    error('overflow');
                  }

                  if (currentValue == n) {
                    // Represent delta as a generalized variable-length integer
                    for (q = delta, k = base; /* no condition */ ; k += base) {
                      t = k <= bias ? tMin : (k >= bias + tMax ? tMax : k - bias);
                      if (q < t) {
                        break;
                      }
                      qMinusT = q - t;
                      baseMinusT = base - t;
                      output.push(
                        stringFromCharCode(digitToBasic(t + qMinusT % baseMinusT, 0))
                      );
                      q = floor(qMinusT / baseMinusT);
                    }

                    output.push(stringFromCharCode(digitToBasic(q, 0)));
                    bias = adapt(delta, handledCPCountPlusOne, handledCPCount == basicLength);
                    delta = 0;
                    ++handledCPCount;
                  }
                }

                ++delta;
                ++n;

              }
              return output.join('');
            }

            /**
             * Converts a Punycode string representing a domain name or an email address
             * to Unicode. Only the Punycoded parts of the input will be converted, i.e.
             * it doesn't matter if you call it on a string that has already been
             * converted to Unicode.
             * @memberOf punycode
             * @param {String} input The Punycoded domain name or email address to
             * convert to Unicode.
             * @returns {String} The Unicode representation of the given Punycode
             * string.
             */
            function toUnicode(input) {
              return mapDomain(input, function(string) {
                return regexPunycode.test(string) ?
                  decode(string.slice(4).toLowerCase()) :
                  string;
              });
            }

            /**
             * Converts a Unicode string representing a domain name or an email address to
             * Punycode. Only the non-ASCII parts of the domain name will be converted,
             * i.e. it doesn't matter if you call it with a domain that's already in
             * ASCII.
             * @memberOf punycode
             * @param {String} input The domain name or email address to convert, as a
             * Unicode string.
             * @returns {String} The Punycode representation of the given domain name or
             * email address.
             */
            function toASCII(input) {
              return mapDomain(input, function(string) {
                return regexNonASCII.test(string) ?
                  'xn--' + encode(string) :
                  string;
              });
            }
            var version = '1.4.1';
            /**
             * An object of methods to convert from JavaScript's internal character
             * representation (UCS-2) to Unicode code points, and back.
             * @see <https://mathiasbynens.be/notes/javascript-encoding>
             * @memberOf punycode
             * @type Object
             */

            var ucs2 = {
              decode: ucs2decode,
              encode: ucs2encode
            };
            var Punycode = {
              version: version,
              ucs2: ucs2,
              toASCII: toASCII,
              toUnicode: toUnicode,
              encode: encode,
              decode: decode
            };

            // from https://github.com/kumavis/browser-process-hrtime/blob/master/index.js
            var performance = global$1.performance || {};
            var performanceNow =
              performance.now        ||
              performance.mozNow     ||
              performance.msNow      ||
              performance.oNow       ||
              performance.webkitNow  ||
              function(){ return (new Date()).getTime() };

            // Load modules




            // Declare internals

            const internals = {
                hasOwn: Object.prototype.hasOwnProperty,
                indexOf: Array.prototype.indexOf,
                defaultThreshold: 16,
                maxIPv6Groups: 8,

                categories: {
                    valid: 1,
                    dnsWarn: 7,
                    rfc5321: 15,
                    cfws: 31,
                    deprecated: 63,
                    rfc5322: 127,
                    error: 255
                },

                diagnoses: {

                    // Address is valid

                    valid: 0,

                    // Address is valid for SMTP but has unusual elements

                    rfc5321TLD: 9,
                    rfc5321TLDNumeric: 10,
                    rfc5321QuotedString: 11,
                    rfc5321AddressLiteral: 12,

                    // Address is valid for message, but must be modified for envelope

                    cfwsComment: 17,
                    cfwsFWS: 18,

                    // Address contains non-ASCII when the allowUnicode option is false
                    // Has to be > internals.defaultThreshold so that it's rejected
                    // without an explicit errorLevel:
                    undesiredNonAscii: 25,

                    // Address contains deprecated elements, but may still be valid in some contexts

                    deprecatedLocalPart: 33,
                    deprecatedFWS: 34,
                    deprecatedQTEXT: 35,
                    deprecatedQP: 36,
                    deprecatedComment: 37,
                    deprecatedCTEXT: 38,
                    deprecatedIPv6: 39,
                    deprecatedCFWSNearAt: 49,

                    // Address is only valid according to broad definition in RFC 5322, but is otherwise invalid

                    rfc5322Domain: 65,
                    rfc5322TooLong: 66,
                    rfc5322LocalTooLong: 67,
                    rfc5322DomainTooLong: 68,
                    rfc5322LabelTooLong: 69,
                    rfc5322DomainLiteral: 70,
                    rfc5322DomainLiteralOBSDText: 71,
                    rfc5322IPv6GroupCount: 72,
                    rfc5322IPv62x2xColon: 73,
                    rfc5322IPv6BadCharacter: 74,
                    rfc5322IPv6MaxGroups: 75,
                    rfc5322IPv6ColonStart: 76,
                    rfc5322IPv6ColonEnd: 77,

                    // Address is invalid for any purpose

                    errExpectingDTEXT: 129,
                    errNoLocalPart: 130,
                    errNoDomain: 131,
                    errConsecutiveDots: 132,
                    errATEXTAfterCFWS: 133,
                    errATEXTAfterQS: 134,
                    errATEXTAfterDomainLiteral: 135,
                    errExpectingQPair: 136,
                    errExpectingATEXT: 137,
                    errExpectingQTEXT: 138,
                    errExpectingCTEXT: 139,
                    errBackslashEnd: 140,
                    errDotStart: 141,
                    errDotEnd: 142,
                    errDomainHyphenStart: 143,
                    errDomainHyphenEnd: 144,
                    errUnclosedQuotedString: 145,
                    errUnclosedComment: 146,
                    errUnclosedDomainLiteral: 147,
                    errFWSCRLFx2: 148,
                    errFWSCRLFEnd: 149,
                    errCRNoLF: 150,
                    errUnknownTLD: 160,
                    errDomainTooShort: 161,
                    errDotAfterDomainLiteral: 162
                },

                components: {
                    localpart: 0,
                    domain: 1,
                    literal: 2,
                    contextComment: 3,
                    contextFWS: 4,
                    contextQuotedString: 5,
                    contextQuotedPair: 6
                }
            };


            internals.specials = function () {

                const specials = '()<>[]:;@\\,."';        // US-ASCII visible characters not valid for atext (http://tools.ietf.org/html/rfc5322#section-3.2.3)
                const lookup = new Array(0x100);
                lookup.fill(false);

                for (let i = 0; i < specials.length; ++i) {
                    lookup[specials.codePointAt(i)] = true;
                }

                return function (code) {

                    return lookup[code];
                };
            }();

            internals.c0Controls = function () {

                const lookup = new Array(0x100);
                lookup.fill(false);

                // add C0 control characters

                for (let i = 0; i < 33; ++i) {
                    lookup[i] = true;
                }

                return function (code) {

                    return lookup[code];
                };
            }();

            internals.c1Controls = function () {

                const lookup = new Array(0x100);
                lookup.fill(false);

                // add C1 control characters

                for (let i = 127; i < 160; ++i) {
                    lookup[i] = true;
                }

                return function (code) {

                    return lookup[code];
                };
            }();

            internals.regex = {
                ipV4: /\b(?:(?:25[0-5]|2[0-4]\d|[01]?\d\d?)\.){3}(?:25[0-5]|2[0-4]\d|[01]?\d\d?)$/,
                ipV6: /^[a-fA-F\d]{0,4}$/
            };

            internals.normalizeSupportsNul = '\0'.normalize('NFC') === '\0';


            // $lab:coverage:off$
            internals.nulNormalize = function (email) {

                return email.split('\0').map((part) => part.normalize('NFC')).join('\0');
            };
            // $lab:coverage:on$


            internals.normalize = function (email) {

                return email.normalize('NFC');
            };


            // $lab:coverage:off$
            if (!internals.normalizeSupportsNul) {
                internals.normalize = function (email) {

                    if (email.indexOf('\0') >= 0) {
                        return internals.nulNormalize(email);
                    }

                    return email.normalize('NFC');
                };
            }
            // $lab:coverage:on$


            internals.checkIpV6 = function (items) {

                return items.every((value) => internals.regex.ipV6.test(value));
            };


            internals.isIterable = Array.isArray;


            /* $lab:coverage:off$ */
            if (typeof Symbol !== 'undefined') {
                internals.isIterable = (value) => Array.isArray(value) || (!!value && typeof value === 'object' && typeof value[Symbol.iterator] === 'function');
            }
            /* $lab:coverage:on$ */


            // Node 10 introduced isSet and isMap, which are useful for cross-context type
            // checking.
            // $lab:coverage:off$
            internals._isSet = (value) => value instanceof Set;
            internals._isMap = (value) => value instanceof Map;
            internals.isSet =  internals._isSet;
            internals.isMap =  internals._isMap;
            // $lab:coverage:on$


            /**
             * Normalize the given lookup "table" to an iterator. Outputs items in arrays
             * and sets, keys from maps (regardless of the corresponding value), and own
             * enumerable keys from all other objects (intended to be plain objects).
             *
             * @param {*} table The table to convert.
             * @returns {Iterable<*>} The converted table.
             */
            internals.normalizeTable = function (table) {

                if (internals.isSet(table) || Array.isArray(table)) {
                    return table;
                }

                if (internals.isMap(table)) {
                    return table.keys();
                }

                return Object.keys(table);
            };


            /**
             * Convert the given domain atom to its canonical form using Nameprep and string
             * lowercasing. Domain atoms that are all-ASCII will not undergo any changes via
             * Nameprep, and domain atoms that have already been canonicalized will not be
             * altered.
             *
             * @param {string} atom The atom to canonicalize.
             * @returns {string} The canonicalized atom.
             */
            internals.canonicalizeAtom = function (atom) {

                return Punycode.toASCII(atom).toLowerCase();
            };


            /**
             * Check whether any of the values in the given iterable, when passed through
             * the iteratee function, are equal to the given value.
             *
             * @param {Iterable<*>} iterable The iterable to check.
             * @param {function(*): *} iteratee The iteratee that receives each item from
             *   the iterable.
             * @param {*} value The reference value.
             * @returns {boolean} Whether the given value matches any of the items in the
             *   iterable per the iteratee.
             */
            internals.includesMapped = function (iterable, iteratee, value) {

                for (const item of iterable) {
                    if (value === iteratee(item)) {
                        return true;
                    }
                }

                return false;
            };


            /**
             * Check whether the given top-level domain atom is valid based on the
             * configured blacklist/whitelist.
             *
             * @param {string} tldAtom The atom to check.
             * @param {Object} options
             *   {*} tldBlacklist The set of domains to consider invalid.
             *   {*} tldWhitelist The set of domains to consider valid.
             * @returns {boolean} Whether the given domain atom is valid per the blacklist/
             *   whitelist.
             */
            internals.validDomain = function (tldAtom, options) {

                // Nameprep handles case-sensitive unicode stuff, but doesn't touch
                // uppercase ASCII characters.
                const canonicalTldAtom = internals.canonicalizeAtom(tldAtom);

                if (options.tldBlacklist) {
                    return !internals.includesMapped(
                        internals.normalizeTable(options.tldBlacklist),
                        internals.canonicalizeAtom, canonicalTldAtom);
                }

                return internals.includesMapped(
                    internals.normalizeTable(options.tldWhitelist),
                    internals.canonicalizeAtom, canonicalTldAtom);
            };


            /**
             * Check whether the domain atoms has an address literal part followed by a
             * normal domain atom part. For example, [127.0.0.1].com.
             *
             * @param {string[]} domainAtoms The parsed domain atoms.
             * @returns {boolean} Whether there exists both a normal domain atom and an
             *   address literal.
             */
            internals.hasDomainLiteralThenAtom = function (domainAtoms) {

                let hasDomainLiteral = false;
                for (let i = 0; i < domainAtoms.length; ++i) {
                    if (domainAtoms[i][0] === '[') {
                        hasDomainLiteral = true;
                    }
                    else if (hasDomainLiteral) {
                        return true;
                    }
                }

                return false;
            };


            /**
             * Check that an email address conforms to RFCs 5321, 5322, 6530 and others
             *
             * We distinguish clearly between a Mailbox as defined by RFC 5321 and an
             * addr-spec as defined by RFC 5322. Depending on the context, either can be
             * regarded as a valid email address. The RFC 5321 Mailbox specification is
             * more restrictive (comments, white space and obsolete forms are not allowed).
             *
             * @param {string} email The email address to check. See README for specifics.
             * @param {Object} options The (optional) options:
             *   {*} errorLevel Determines the boundary between valid and invalid
             *     addresses.
             *   {*} tldBlacklist The set of domains to consider invalid.
             *   {*} tldWhitelist The set of domains to consider valid.
             *   {*} allowUnicode Whether to allow non-ASCII characters, defaults to true.
             *   {*} minDomainAtoms The minimum number of domain atoms which must be present
             *     for the address to be valid.
             * @param {function(number|boolean)} callback The (optional) callback handler.
             * @return {*}
             */

            var validate = internals.validate = function (email, options, callback) {

                options = options || {};

                if (typeof email !== 'string') {
                    throw new TypeError('expected string email');
                }

                email = internals.normalize(email);

                // The callback function is deprecated.
                // $lab:coverage:off$
                if (typeof options === 'function') {
                    callback = options;
                    options = {};
                }

                if (typeof callback !== 'function') {
                    callback = null;
                }
                // $lab:coverage:on$

                let diagnose;
                let threshold;

                if (typeof options.errorLevel === 'number') {
                    diagnose = true;
                    threshold = options.errorLevel;
                }
                else {
                    diagnose = !!options.errorLevel;
                    threshold = internals.diagnoses.valid;
                }

                if (options.tldWhitelist) {
                    if (typeof options.tldWhitelist === 'string') {
                        options.tldWhitelist = [options.tldWhitelist];
                    }
                    else if (typeof options.tldWhitelist !== 'object') {
                        throw new TypeError('expected array or object tldWhitelist');
                    }
                }

                if (options.tldBlacklist) {
                    if (typeof options.tldBlacklist === 'string') {
                        options.tldBlacklist = [options.tldBlacklist];
                    }
                    else if (typeof options.tldBlacklist !== 'object') {
                        throw new TypeError('expected array or object tldBlacklist');
                    }
                }

                if (options.minDomainAtoms && (options.minDomainAtoms !== ((+options.minDomainAtoms) | 0) || options.minDomainAtoms < 0)) {
                    throw new TypeError('expected positive integer minDomainAtoms');
                }

                // Normalize the set of excluded diagnoses.
                if (options.excludeDiagnoses) {
                    if (!internals.isIterable(options.excludeDiagnoses)) {
                        throw new TypeError('expected iterable excludeDiagnoses');
                    }

                    // This won't catch cross-realm Sets pre-Node 10, but it will cast the
                    // value to an in-realm Set representation.
                    if (!internals.isSet(options.excludeDiagnoses)) {
                        options.excludeDiagnoses = new Set(options.excludeDiagnoses);
                    }
                }

                let maxResult = internals.diagnoses.valid;
                const updateResult = (value) => {

                    if (value > maxResult && (!options.excludeDiagnoses || !options.excludeDiagnoses.has(value))) {
                        maxResult = value;
                    }
                };

                const allowUnicode = options.allowUnicode === undefined || !!options.allowUnicode;
                if (!allowUnicode && /[^\x00-\x7f]/.test(email)) {
                    updateResult(internals.diagnoses.undesiredNonAscii);
                }

                const context = {
                    now: internals.components.localpart,
                    prev: internals.components.localpart,
                    stack: [internals.components.localpart]
                };

                let prevToken = '';

                const parseData = {
                    local: '',
                    domain: ''
                };
                const atomData = {
                    locals: [''],
                    domains: ['']
                };

                let elementCount = 0;
                let elementLength = 0;
                let crlfCount = 0;
                let charCode;

                let hyphenFlag = false;
                let assertEnd = false;

                const emailLength = email.length;

                let token;                                      // Token is used outside the loop, must declare similarly
                for (let i = 0; i < emailLength; i += token.length) {
                    // Utilize codepoints to account for Unicode surrogate pairs
                    token = String.fromCodePoint(email.codePointAt(i));

                    switch (context.now) {
                        // Local-part
                        case internals.components.localpart:
                            // http://tools.ietf.org/html/rfc5322#section-3.4.1
                            //   local-part      =   dot-atom / quoted-string / obs-local-part
                            //
                            //   dot-atom        =   [CFWS] dot-atom-text [CFWS]
                            //
                            //   dot-atom-text   =   1*atext *("." 1*atext)
                            //
                            //   quoted-string   =   [CFWS]
                            //                       DQUOTE *([FWS] qcontent) [FWS] DQUOTE
                            //                       [CFWS]
                            //
                            //   obs-local-part  =   word *("." word)
                            //
                            //   word            =   atom / quoted-string
                            //
                            //   atom            =   [CFWS] 1*atext [CFWS]
                            switch (token) {
                                // Comment
                                case '(':
                                    if (elementLength === 0) {
                                        // Comments are OK at the beginning of an element
                                        updateResult(elementCount === 0 ? internals.diagnoses.cfwsComment : internals.diagnoses.deprecatedComment);
                                    }
                                    else {
                                        updateResult(internals.diagnoses.cfwsComment);
                                        // Cannot start a comment in an element, should be end
                                        assertEnd = true;
                                    }

                                    context.stack.push(context.now);
                                    context.now = internals.components.contextComment;
                                    break;

                                    // Next dot-atom element
                                case '.':
                                    if (elementLength === 0) {
                                        // Another dot, already?
                                        updateResult(elementCount === 0 ? internals.diagnoses.errDotStart : internals.diagnoses.errConsecutiveDots);
                                    }
                                    else {
                                        // The entire local-part can be a quoted string for RFC 5321; if one atom is quoted it's an RFC 5322 obsolete form
                                        if (assertEnd) {
                                            updateResult(internals.diagnoses.deprecatedLocalPart);
                                        }

                                        // CFWS & quoted strings are OK again now we're at the beginning of an element (although they are obsolete forms)
                                        assertEnd = false;
                                        elementLength = 0;
                                        ++elementCount;
                                        parseData.local += token;
                                        atomData.locals[elementCount] = '';
                                    }

                                    break;

                                    // Quoted string
                                case '"':
                                    if (elementLength === 0) {
                                        // The entire local-part can be a quoted string for RFC 5321; if one atom is quoted it's an RFC 5322 obsolete form
                                        updateResult(elementCount === 0 ? internals.diagnoses.rfc5321QuotedString : internals.diagnoses.deprecatedLocalPart);

                                        parseData.local += token;
                                        atomData.locals[elementCount] += token;
                                        elementLength += Buffer$1.byteLength(token, 'utf8');

                                        // Quoted string must be the entire element
                                        assertEnd = true;
                                        context.stack.push(context.now);
                                        context.now = internals.components.contextQuotedString;
                                    }
                                    else {
                                        updateResult(internals.diagnoses.errExpectingATEXT);
                                    }

                                    break;

                                    // Folding white space
                                case '\r':
                                    if (emailLength === ++i || email[i] !== '\n') {
                                        // Fatal error
                                        updateResult(internals.diagnoses.errCRNoLF);
                                        break;
                                    }

                                    // Fallthrough

                                case ' ':
                                case '\t':
                                    if (elementLength === 0) {
                                        updateResult(elementCount === 0 ? internals.diagnoses.cfwsFWS : internals.diagnoses.deprecatedFWS);
                                    }
                                    else {
                                        // We can't start FWS in the middle of an element, better be end
                                        assertEnd = true;
                                    }

                                    context.stack.push(context.now);
                                    context.now = internals.components.contextFWS;
                                    prevToken = token;
                                    break;

                                case '@':
                                    // At this point we should have a valid local-part
                                    // $lab:coverage:off$
                                    if (context.stack.length !== 1) {
                                        throw new Error('unexpected item on context stack');
                                    }
                                    // $lab:coverage:on$

                                    if (parseData.local.length === 0) {
                                        // Fatal error
                                        updateResult(internals.diagnoses.errNoLocalPart);
                                    }
                                    else if (elementLength === 0) {
                                        // Fatal error
                                        updateResult(internals.diagnoses.errDotEnd);
                                    }
                                    // http://tools.ietf.org/html/rfc5321#section-4.5.3.1.1 the maximum total length of a user name or other local-part is 64
                                    //    octets
                                    else if (Buffer$1.byteLength(parseData.local, 'utf8') > 64) {
                                        updateResult(internals.diagnoses.rfc5322LocalTooLong);
                                    }
                                    // http://tools.ietf.org/html/rfc5322#section-3.4.1 comments and folding white space SHOULD NOT be used around "@" in the
                                    //    addr-spec
                                    //
                                    // http://tools.ietf.org/html/rfc2119
                                    // 4. SHOULD NOT this phrase, or the phrase "NOT RECOMMENDED" mean that there may exist valid reasons in particular
                                    //    circumstances when the particular behavior is acceptable or even useful, but the full implications should be understood
                                    //    and the case carefully weighed before implementing any behavior described with this label.
                                    else if (context.prev === internals.components.contextComment || context.prev === internals.components.contextFWS) {
                                        updateResult(internals.diagnoses.deprecatedCFWSNearAt);
                                    }

                                    // Clear everything down for the domain parsing
                                    context.now = internals.components.domain;
                                    context.stack[0] = internals.components.domain;
                                    elementCount = 0;
                                    elementLength = 0;
                                    assertEnd = false; // CFWS can only appear at the end of the element
                                    break;

                                    // ATEXT
                                default:
                                    // http://tools.ietf.org/html/rfc5322#section-3.2.3
                                    //    atext = ALPHA / DIGIT / ; Printable US-ASCII
                                    //            "!" / "#" /     ;  characters not including
                                    //            "$" / "%" /     ;  specials.  Used for atoms.
                                    //            "&" / "'" /
                                    //            "*" / "+" /
                                    //            "-" / "/" /
                                    //            "=" / "?" /
                                    //            "^" / "_" /
                                    //            "`" / "{" /
                                    //            "|" / "}" /
                                    //            "~"
                                    if (assertEnd) {
                                        // We have encountered atext where it is no longer valid
                                        switch (context.prev) {
                                            case internals.components.contextComment:
                                            case internals.components.contextFWS:
                                                updateResult(internals.diagnoses.errATEXTAfterCFWS);
                                                break;

                                            case internals.components.contextQuotedString:
                                                updateResult(internals.diagnoses.errATEXTAfterQS);
                                                break;

                                                // $lab:coverage:off$
                                            default:
                                                throw new Error('more atext found where none is allowed, but unrecognized prev context: ' + context.prev);
                                                // $lab:coverage:on$
                                        }
                                    }
                                    else {
                                        context.prev = context.now;
                                        charCode = token.codePointAt(0);

                                        // Especially if charCode == 10
                                        if (internals.specials(charCode) || internals.c0Controls(charCode) || internals.c1Controls(charCode)) {

                                            // Fatal error
                                            updateResult(internals.diagnoses.errExpectingATEXT);
                                        }

                                        parseData.local += token;
                                        atomData.locals[elementCount] += token;
                                        elementLength += Buffer$1.byteLength(token, 'utf8');
                                    }
                            }

                            break;

                        case internals.components.domain:
                            // http://tools.ietf.org/html/rfc5322#section-3.4.1
                            //   domain          =   dot-atom / domain-literal / obs-domain
                            //
                            //   dot-atom        =   [CFWS] dot-atom-text [CFWS]
                            //
                            //   dot-atom-text   =   1*atext *("." 1*atext)
                            //
                            //   domain-literal  =   [CFWS] "[" *([FWS] dtext) [FWS] "]" [CFWS]
                            //
                            //   dtext           =   %d33-90 /          ; Printable US-ASCII
                            //                       %d94-126 /         ;  characters not including
                            //                       obs-dtext          ;  "[", "]", or "\"
                            //
                            //   obs-domain      =   atom *("." atom)
                            //
                            //   atom            =   [CFWS] 1*atext [CFWS]

                            // http://tools.ietf.org/html/rfc5321#section-4.1.2
                            //   Mailbox        = Local-part "@" ( Domain / address-literal )
                            //
                            //   Domain         = sub-domain *("." sub-domain)
                            //
                            //   address-literal  = "[" ( IPv4-address-literal /
                            //                    IPv6-address-literal /
                            //                    General-address-literal ) "]"
                            //                    ; See Section 4.1.3

                            // http://tools.ietf.org/html/rfc5322#section-3.4.1
                            //      Note: A liberal syntax for the domain portion of addr-spec is
                            //      given here.  However, the domain portion contains addressing
                            //      information specified by and used in other protocols (e.g.,
                            //      [RFC1034], [RFC1035], [RFC1123], [RFC5321]).  It is therefore
                            //      incumbent upon implementations to conform to the syntax of
                            //      addresses for the context in which they are used.
                            //
                            // is_email() author's note: it's not clear how to interpret this in
                            // he context of a general email address validator. The conclusion I
                            // have reached is this: "addressing information" must comply with
                            // RFC 5321 (and in turn RFC 1035), anything that is "semantically
                            // invisible" must comply only with RFC 5322.
                            switch (token) {
                                // Comment
                                case '(':
                                    if (elementLength === 0) {
                                        // Comments at the start of the domain are deprecated in the text, comments at the start of a subdomain are obs-domain
                                        // http://tools.ietf.org/html/rfc5322#section-3.4.1
                                        updateResult(elementCount === 0 ? internals.diagnoses.deprecatedCFWSNearAt : internals.diagnoses.deprecatedComment);
                                    }
                                    else {
                                        // We can't start a comment mid-element, better be at the end
                                        assertEnd = true;
                                        updateResult(internals.diagnoses.cfwsComment);
                                    }

                                    context.stack.push(context.now);
                                    context.now = internals.components.contextComment;
                                    break;

                                    // Next dot-atom element
                                case '.':
                                    const punycodeLength = Punycode.toASCII(atomData.domains[elementCount]).length;
                                    if (elementLength === 0) {
                                        // Another dot, already? Fatal error.
                                        updateResult(elementCount === 0 ? internals.diagnoses.errDotStart : internals.diagnoses.errConsecutiveDots);
                                    }
                                    else if (hyphenFlag) {
                                        // Previous subdomain ended in a hyphen. Fatal error.
                                        updateResult(internals.diagnoses.errDomainHyphenEnd);
                                    }
                                    else if (punycodeLength > 63) {
                                        // RFC 5890 specifies that domain labels that are encoded using the Punycode algorithm
                                        // must adhere to the <= 63 octet requirement.
                                        // This includes string prefixes from the Punycode algorithm.
                                        //
                                        // https://tools.ietf.org/html/rfc5890#section-2.3.2.1
                                        // labels          63 octets or less

                                        updateResult(internals.diagnoses.rfc5322LabelTooLong);
                                    }

                                    // CFWS is OK again now we're at the beginning of an element (although
                                    // it may be obsolete CFWS)
                                    assertEnd = false;
                                    elementLength = 0;
                                    ++elementCount;
                                    atomData.domains[elementCount] = '';
                                    parseData.domain += token;

                                    break;

                                    // Domain literal
                                case '[':
                                    if (atomData.domains[elementCount].length === 0) {
                                        if (parseData.domain.length) {
                                            // Domain literal interspersed with domain refs.
                                            updateResult(internals.diagnoses.errDotAfterDomainLiteral);
                                        }

                                        assertEnd = true;
                                        elementLength += Buffer$1.byteLength(token, 'utf8');
                                        context.stack.push(context.now);
                                        context.now = internals.components.literal;
                                        parseData.domain += token;
                                        atomData.domains[elementCount] += token;
                                        parseData.literal = '';
                                    }
                                    else {
                                        // Fatal error
                                        updateResult(internals.diagnoses.errExpectingATEXT);
                                    }

                                    break;

                                    // Folding white space
                                case '\r':
                                    if (emailLength === ++i || email[i] !== '\n') {
                                        // Fatal error
                                        updateResult(internals.diagnoses.errCRNoLF);
                                        break;
                                    }

                                    // Fallthrough

                                case ' ':
                                case '\t':
                                    if (elementLength === 0) {
                                        updateResult(elementCount === 0 ? internals.diagnoses.deprecatedCFWSNearAt : internals.diagnoses.deprecatedFWS);
                                    }
                                    else {
                                        // We can't start FWS in the middle of an element, so this better be the end
                                        updateResult(internals.diagnoses.cfwsFWS);
                                        assertEnd = true;
                                    }

                                    context.stack.push(context.now);
                                    context.now = internals.components.contextFWS;
                                    prevToken = token;
                                    break;

                                    // This must be ATEXT
                                default:
                                    // RFC 5322 allows any atext...
                                    // http://tools.ietf.org/html/rfc5322#section-3.2.3
                                    //    atext = ALPHA / DIGIT / ; Printable US-ASCII
                                    //            "!" / "#" /     ;  characters not including
                                    //            "$" / "%" /     ;  specials.  Used for atoms.
                                    //            "&" / "'" /
                                    //            "*" / "+" /
                                    //            "-" / "/" /
                                    //            "=" / "?" /
                                    //            "^" / "_" /
                                    //            "`" / "{" /
                                    //            "|" / "}" /
                                    //            "~"

                                    // But RFC 5321 only allows letter-digit-hyphen to comply with DNS rules
                                    //   (RFCs 1034 & 1123)
                                    // http://tools.ietf.org/html/rfc5321#section-4.1.2
                                    //   sub-domain     = Let-dig [Ldh-str]
                                    //
                                    //   Let-dig        = ALPHA / DIGIT
                                    //
                                    //   Ldh-str        = *( ALPHA / DIGIT / "-" ) Let-dig
                                    //
                                    if (assertEnd) {
                                        // We have encountered ATEXT where it is no longer valid
                                        switch (context.prev) {
                                            case internals.components.contextComment:
                                            case internals.components.contextFWS:
                                                updateResult(internals.diagnoses.errATEXTAfterCFWS);
                                                break;

                                            case internals.components.literal:
                                                updateResult(internals.diagnoses.errATEXTAfterDomainLiteral);
                                                break;

                                                // $lab:coverage:off$
                                            default:
                                                throw new Error('more atext found where none is allowed, but unrecognized prev context: ' + context.prev);
                                                // $lab:coverage:on$
                                        }
                                    }

                                    charCode = token.codePointAt(0);
                                    // Assume this token isn't a hyphen unless we discover it is
                                    hyphenFlag = false;

                                    if (internals.specials(charCode) || internals.c0Controls(charCode) || internals.c1Controls(charCode)) {
                                        // Fatal error
                                        updateResult(internals.diagnoses.errExpectingATEXT);
                                    }
                                    else if (token === '-') {
                                        if (elementLength === 0) {
                                            // Hyphens cannot be at the beginning of a subdomain, fatal error
                                            updateResult(internals.diagnoses.errDomainHyphenStart);
                                        }

                                        hyphenFlag = true;
                                    }
                                    // Check if it's a neither a number nor a latin/unicode letter
                                    else if (charCode < 48 || (charCode > 122 && charCode < 192) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97)) {
                                        // This is not an RFC 5321 subdomain, but still OK by RFC 5322
                                        updateResult(internals.diagnoses.rfc5322Domain);
                                    }

                                    parseData.domain += token;
                                    atomData.domains[elementCount] += token;
                                    elementLength += Buffer$1.byteLength(token, 'utf8');
                            }

                            break;

                            // Domain literal
                        case internals.components.literal:
                            // http://tools.ietf.org/html/rfc5322#section-3.4.1
                            //   domain-literal  =   [CFWS] "[" *([FWS] dtext) [FWS] "]" [CFWS]
                            //
                            //   dtext           =   %d33-90 /          ; Printable US-ASCII
                            //                       %d94-126 /         ;  characters not including
                            //                       obs-dtext          ;  "[", "]", or "\"
                            //
                            //   obs-dtext       =   obs-NO-WS-CTL / quoted-pair
                            switch (token) {
                                // End of domain literal
                                case ']':
                                    if (maxResult < internals.categories.deprecated) {
                                        // Could be a valid RFC 5321 address literal, so let's check

                                        // http://tools.ietf.org/html/rfc5321#section-4.1.2
                                        //   address-literal  = "[" ( IPv4-address-literal /
                                        //                    IPv6-address-literal /
                                        //                    General-address-literal ) "]"
                                        //                    ; See Section 4.1.3
                                        //
                                        // http://tools.ietf.org/html/rfc5321#section-4.1.3
                                        //   IPv4-address-literal  = Snum 3("."  Snum)
                                        //
                                        //   IPv6-address-literal  = "IPv6:" IPv6-addr
                                        //
                                        //   General-address-literal  = Standardized-tag ":" 1*dcontent
                                        //
                                        //   Standardized-tag  = Ldh-str
                                        //                     ; Standardized-tag MUST be specified in a
                                        //                     ; Standards-Track RFC and registered with IANA
                                        //
                                        //   dcontent      = %d33-90 / ; Printable US-ASCII
                                        //                 %d94-126 ; excl. "[", "\", "]"
                                        //
                                        //   Snum          = 1*3DIGIT
                                        //                 ; representing a decimal integer
                                        //                 ; value in the range 0 through 255
                                        //
                                        //   IPv6-addr     = IPv6-full / IPv6-comp / IPv6v4-full / IPv6v4-comp
                                        //
                                        //   IPv6-hex      = 1*4HEXDIG
                                        //
                                        //   IPv6-full     = IPv6-hex 7(":" IPv6-hex)
                                        //
                                        //   IPv6-comp     = [IPv6-hex *5(":" IPv6-hex)] "::"
                                        //                 [IPv6-hex *5(":" IPv6-hex)]
                                        //                 ; The "::" represents at least 2 16-bit groups of
                                        //                 ; zeros.  No more than 6 groups in addition to the
                                        //                 ; "::" may be present.
                                        //
                                        //   IPv6v4-full   = IPv6-hex 5(":" IPv6-hex) ":" IPv4-address-literal
                                        //
                                        //   IPv6v4-comp   = [IPv6-hex *3(":" IPv6-hex)] "::"
                                        //                 [IPv6-hex *3(":" IPv6-hex) ":"]
                                        //                 IPv4-address-literal
                                        //                 ; The "::" represents at least 2 16-bit groups of
                                        //                 ; zeros.  No more than 4 groups in addition to the
                                        //                 ; "::" and IPv4-address-literal may be present.

                                        let index = -1;
                                        let addressLiteral = parseData.literal;
                                        const matchesIP = internals.regex.ipV4.exec(addressLiteral);

                                        // Maybe extract IPv4 part from the end of the address-literal
                                        if (matchesIP) {
                                            index = matchesIP.index;
                                            if (index !== 0) {
                                                // Convert IPv4 part to IPv6 format for futher testing
                                                addressLiteral = addressLiteral.slice(0, index) + '0:0';
                                            }
                                        }

                                        if (index === 0) {
                                            // Nothing there except a valid IPv4 address, so...
                                            updateResult(internals.diagnoses.rfc5321AddressLiteral);
                                        }
                                        else if (addressLiteral.slice(0, 5).toLowerCase() !== 'ipv6:') {
                                            updateResult(internals.diagnoses.rfc5322DomainLiteral);
                                        }
                                        else {
                                            const match = addressLiteral.slice(5);
                                            let maxGroups = internals.maxIPv6Groups;
                                            const groups = match.split(':');
                                            index = match.indexOf('::');

                                            if (!~index) {
                                                // Need exactly the right number of groups
                                                if (groups.length !== maxGroups) {
                                                    updateResult(internals.diagnoses.rfc5322IPv6GroupCount);
                                                }
                                            }
                                            else if (index !== match.lastIndexOf('::')) {
                                                updateResult(internals.diagnoses.rfc5322IPv62x2xColon);
                                            }
                                            else {
                                                if (index === 0 || index === match.length - 2) {
                                                    // RFC 4291 allows :: at the start or end of an address with 7 other groups in addition
                                                    ++maxGroups;
                                                }

                                                if (groups.length > maxGroups) {
                                                    updateResult(internals.diagnoses.rfc5322IPv6MaxGroups);
                                                }
                                                else if (groups.length === maxGroups) {
                                                    // Eliding a single "::"
                                                    updateResult(internals.diagnoses.deprecatedIPv6);
                                                }
                                            }

                                            // IPv6 testing strategy
                                            if (match[0] === ':' && match[1] !== ':') {
                                                updateResult(internals.diagnoses.rfc5322IPv6ColonStart);
                                            }
                                            else if (match[match.length - 1] === ':' && match[match.length - 2] !== ':') {
                                                updateResult(internals.diagnoses.rfc5322IPv6ColonEnd);
                                            }
                                            else if (internals.checkIpV6(groups)) {
                                                updateResult(internals.diagnoses.rfc5321AddressLiteral);
                                            }
                                            else {
                                                updateResult(internals.diagnoses.rfc5322IPv6BadCharacter);
                                            }
                                        }
                                    }
                                    else {
                                        updateResult(internals.diagnoses.rfc5322DomainLiteral);
                                    }

                                    parseData.domain += token;
                                    atomData.domains[elementCount] += token;
                                    elementLength += Buffer$1.byteLength(token, 'utf8');
                                    context.prev = context.now;
                                    context.now = context.stack.pop();
                                    break;

                                case '\\':
                                    updateResult(internals.diagnoses.rfc5322DomainLiteralOBSDText);
                                    context.stack.push(context.now);
                                    context.now = internals.components.contextQuotedPair;
                                    break;

                                    // Folding white space
                                case '\r':
                                    if (emailLength === ++i || email[i] !== '\n') {
                                        updateResult(internals.diagnoses.errCRNoLF);
                                        break;
                                    }

                                    // Fallthrough

                                case ' ':
                                case '\t':
                                    updateResult(internals.diagnoses.cfwsFWS);

                                    context.stack.push(context.now);
                                    context.now = internals.components.contextFWS;
                                    prevToken = token;
                                    break;

                                    // DTEXT
                                default:
                                    // http://tools.ietf.org/html/rfc5322#section-3.4.1
                                    //   dtext         =   %d33-90 /  ; Printable US-ASCII
                                    //                     %d94-126 / ;  characters not including
                                    //                     obs-dtext  ;  "[", "]", or "\"
                                    //
                                    //   obs-dtext     =   obs-NO-WS-CTL / quoted-pair
                                    //
                                    //   obs-NO-WS-CTL =   %d1-8 /    ; US-ASCII control
                                    //                     %d11 /     ;  characters that do not
                                    //                     %d12 /     ;  include the carriage
                                    //                     %d14-31 /  ;  return, line feed, and
                                    //                     %d127      ;  white space characters
                                    charCode = token.codePointAt(0);

                                    // '\r', '\n', ' ', and '\t' have already been parsed above
                                    if ((charCode !== 127 && internals.c1Controls(charCode)) || charCode === 0 || token === '[') {
                                        // Fatal error
                                        updateResult(internals.diagnoses.errExpectingDTEXT);
                                        break;
                                    }
                                    else if (internals.c0Controls(charCode) || charCode === 127) {
                                        updateResult(internals.diagnoses.rfc5322DomainLiteralOBSDText);
                                    }

                                    parseData.literal += token;
                                    parseData.domain += token;
                                    atomData.domains[elementCount] += token;
                                    elementLength += Buffer$1.byteLength(token, 'utf8');
                            }

                            break;

                            // Quoted string
                        case internals.components.contextQuotedString:
                            // http://tools.ietf.org/html/rfc5322#section-3.2.4
                            //   quoted-string = [CFWS]
                            //                   DQUOTE *([FWS] qcontent) [FWS] DQUOTE
                            //                   [CFWS]
                            //
                            //   qcontent      = qtext / quoted-pair
                            switch (token) {
                                // Quoted pair
                                case '\\':
                                    context.stack.push(context.now);
                                    context.now = internals.components.contextQuotedPair;
                                    break;

                                    // Folding white space. Spaces are allowed as regular characters inside a quoted string - it's only FWS if we include '\t' or '\r\n'
                                case '\r':
                                    if (emailLength === ++i || email[i] !== '\n') {
                                        // Fatal error
                                        updateResult(internals.diagnoses.errCRNoLF);
                                        break;
                                    }

                                    // Fallthrough

                                case '\t':
                                    // http://tools.ietf.org/html/rfc5322#section-3.2.2
                                    //   Runs of FWS, comment, or CFWS that occur between lexical tokens in
                                    //   a structured header field are semantically interpreted as a single
                                    //   space character.

                                    // http://tools.ietf.org/html/rfc5322#section-3.2.4
                                    //   the CRLF in any FWS/CFWS that appears within the quoted-string [is]
                                    //   semantically "invisible" and therefore not part of the
                                    //   quoted-string

                                    parseData.local += ' ';
                                    atomData.locals[elementCount] += ' ';
                                    elementLength += Buffer$1.byteLength(token, 'utf8');

                                    updateResult(internals.diagnoses.cfwsFWS);
                                    context.stack.push(context.now);
                                    context.now = internals.components.contextFWS;
                                    prevToken = token;
                                    break;

                                    // End of quoted string
                                case '"':
                                    parseData.local += token;
                                    atomData.locals[elementCount] += token;
                                    elementLength += Buffer$1.byteLength(token, 'utf8');
                                    context.prev = context.now;
                                    context.now = context.stack.pop();
                                    break;

                                    // QTEXT
                                default:
                                    // http://tools.ietf.org/html/rfc5322#section-3.2.4
                                    //   qtext          =   %d33 /             ; Printable US-ASCII
                                    //                      %d35-91 /          ;  characters not including
                                    //                      %d93-126 /         ;  "\" or the quote character
                                    //                      obs-qtext
                                    //
                                    //   obs-qtext      =   obs-NO-WS-CTL
                                    //
                                    //   obs-NO-WS-CTL  =   %d1-8 /            ; US-ASCII control
                                    //                      %d11 /             ;  characters that do not
                                    //                      %d12 /             ;  include the carriage
                                    //                      %d14-31 /          ;  return, line feed, and
                                    //                      %d127              ;  white space characters
                                    charCode = token.codePointAt(0);

                                    if ((charCode !== 127 && internals.c1Controls(charCode)) || charCode === 0 || charCode === 10) {
                                        updateResult(internals.diagnoses.errExpectingQTEXT);
                                    }
                                    else if (internals.c0Controls(charCode) || charCode === 127) {
                                        updateResult(internals.diagnoses.deprecatedQTEXT);
                                    }

                                    parseData.local += token;
                                    atomData.locals[elementCount] += token;
                                    elementLength += Buffer$1.byteLength(token, 'utf8');
                            }

                            // http://tools.ietf.org/html/rfc5322#section-3.4.1
                            //   If the string can be represented as a dot-atom (that is, it contains
                            //   no characters other than atext characters or "." surrounded by atext
                            //   characters), then the dot-atom form SHOULD be used and the quoted-
                            //   string form SHOULD NOT be used.

                            break;
                            // Quoted pair
                        case internals.components.contextQuotedPair:
                            // http://tools.ietf.org/html/rfc5322#section-3.2.1
                            //   quoted-pair     =   ("\" (VCHAR / WSP)) / obs-qp
                            //
                            //   VCHAR           =  %d33-126   ; visible (printing) characters
                            //   WSP             =  SP / HTAB  ; white space
                            //
                            //   obs-qp          =   "\" (%d0 / obs-NO-WS-CTL / LF / CR)
                            //
                            //   obs-NO-WS-CTL   =   %d1-8 /   ; US-ASCII control
                            //                       %d11 /    ;  characters that do not
                            //                       %d12 /    ;  include the carriage
                            //                       %d14-31 / ;  return, line feed, and
                            //                       %d127     ;  white space characters
                            //
                            // i.e. obs-qp       =  "\" (%d0-8, %d10-31 / %d127)
                            charCode = token.codePointAt(0);

                            if (charCode !== 127 &&  internals.c1Controls(charCode)) {
                                // Fatal error
                                updateResult(internals.diagnoses.errExpectingQPair);
                            }
                            else if ((charCode < 31 && charCode !== 9) || charCode === 127) {
                                // ' ' and '\t' are allowed
                                updateResult(internals.diagnoses.deprecatedQP);
                            }

                            // At this point we know where this qpair occurred so we could check to see if the character actually needed to be quoted at all.
                            // http://tools.ietf.org/html/rfc5321#section-4.1.2
                            //   the sending system SHOULD transmit the form that uses the minimum quoting possible.

                            context.prev = context.now;
                            // End of qpair
                            context.now = context.stack.pop();
                            const escapeToken = '\\' + token;

                            switch (context.now) {
                                case internals.components.contextComment:
                                    break;

                                case internals.components.contextQuotedString:
                                    parseData.local += escapeToken;
                                    atomData.locals[elementCount] += escapeToken;

                                    // The maximum sizes specified by RFC 5321 are octet counts, so we must include the backslash
                                    elementLength += 2;
                                    break;

                                case internals.components.literal:
                                    parseData.domain += escapeToken;
                                    atomData.domains[elementCount] += escapeToken;

                                    // The maximum sizes specified by RFC 5321 are octet counts, so we must include the backslash
                                    elementLength += 2;
                                    break;

                                    // $lab:coverage:off$
                                default:
                                    throw new Error('quoted pair logic invoked in an invalid context: ' + context.now);
                                    // $lab:coverage:on$
                            }

                            break;

                            // Comment
                        case internals.components.contextComment:
                            // http://tools.ietf.org/html/rfc5322#section-3.2.2
                            //   comment  = "(" *([FWS] ccontent) [FWS] ")"
                            //
                            //   ccontent = ctext / quoted-pair / comment
                            switch (token) {
                                // Nested comment
                                case '(':
                                    // Nested comments are ok
                                    context.stack.push(context.now);
                                    context.now = internals.components.contextComment;
                                    break;

                                    // End of comment
                                case ')':
                                    context.prev = context.now;
                                    context.now = context.stack.pop();
                                    break;

                                    // Quoted pair
                                case '\\':
                                    context.stack.push(context.now);
                                    context.now = internals.components.contextQuotedPair;
                                    break;

                                    // Folding white space
                                case '\r':
                                    if (emailLength === ++i || email[i] !== '\n') {
                                        // Fatal error
                                        updateResult(internals.diagnoses.errCRNoLF);
                                        break;
                                    }

                                    // Fallthrough

                                case ' ':
                                case '\t':
                                    updateResult(internals.diagnoses.cfwsFWS);

                                    context.stack.push(context.now);
                                    context.now = internals.components.contextFWS;
                                    prevToken = token;
                                    break;

                                    // CTEXT
                                default:
                                    // http://tools.ietf.org/html/rfc5322#section-3.2.3
                                    //   ctext         = %d33-39 /  ; Printable US-ASCII
                                    //                   %d42-91 /  ;  characters not including
                                    //                   %d93-126 / ;  "(", ")", or "\"
                                    //                   obs-ctext
                                    //
                                    //   obs-ctext     = obs-NO-WS-CTL
                                    //
                                    //   obs-NO-WS-CTL = %d1-8 /    ; US-ASCII control
                                    //                   %d11 /     ;  characters that do not
                                    //                   %d12 /     ;  include the carriage
                                    //                   %d14-31 /  ;  return, line feed, and
                                    //                   %d127      ;  white space characters
                                    charCode = token.codePointAt(0);

                                    if (charCode === 0 || charCode === 10 || (charCode !== 127 && internals.c1Controls(charCode))) {
                                        // Fatal error
                                        updateResult(internals.diagnoses.errExpectingCTEXT);
                                        break;
                                    }
                                    else if (internals.c0Controls(charCode) || charCode === 127) {
                                        updateResult(internals.diagnoses.deprecatedCTEXT);
                                    }
                            }

                            break;

                            // Folding white space
                        case internals.components.contextFWS:
                            // http://tools.ietf.org/html/rfc5322#section-3.2.2
                            //   FWS     =   ([*WSP CRLF] 1*WSP) /  obs-FWS
                            //                                   ; Folding white space

                            // But note the erratum:
                            // http://www.rfc-editor.org/errata_search.php?rfc=5322&eid=1908:
                            //   In the obsolete syntax, any amount of folding white space MAY be
                            //   inserted where the obs-FWS rule is allowed.  This creates the
                            //   possibility of having two consecutive "folds" in a line, and
                            //   therefore the possibility that a line which makes up a folded header
                            //   field could be composed entirely of white space.
                            //
                            //   obs-FWS =   1*([CRLF] WSP)

                            if (prevToken === '\r') {
                                if (token === '\r') {
                                    // Fatal error
                                    updateResult(internals.diagnoses.errFWSCRLFx2);
                                    break;
                                }

                                if (++crlfCount > 1) {
                                    // Multiple folds => obsolete FWS
                                    updateResult(internals.diagnoses.deprecatedFWS);
                                }
                                else {
                                    crlfCount = 1;
                                }
                            }

                            switch (token) {
                                case '\r':
                                    if (emailLength === ++i || email[i] !== '\n') {
                                        // Fatal error
                                        updateResult(internals.diagnoses.errCRNoLF);
                                    }

                                    break;

                                case ' ':
                                case '\t':
                                    break;

                                default:
                                    if (prevToken === '\r') {
                                        // Fatal error
                                        updateResult(internals.diagnoses.errFWSCRLFEnd);
                                    }

                                    crlfCount = 0;

                                    // End of FWS
                                    context.prev = context.now;
                                    context.now = context.stack.pop();

                                    // Look at this token again in the parent context
                                    --i;
                            }

                            prevToken = token;
                            break;

                            // Unexpected context
                            // $lab:coverage:off$
                        default:
                            throw new Error('unknown context: ' + context.now);
                            // $lab:coverage:on$
                    } // Primary state machine

                    if (maxResult > internals.categories.rfc5322) {
                        // Fatal error, no point continuing
                        break;
                    }
                } // Token loop

                // Check for errors
                if (maxResult < internals.categories.rfc5322) {
                    const punycodeLength = Punycode.toASCII(parseData.domain).length;
                    // Fatal errors
                    if (context.now === internals.components.contextQuotedString) {
                        updateResult(internals.diagnoses.errUnclosedQuotedString);
                    }
                    else if (context.now === internals.components.contextQuotedPair) {
                        updateResult(internals.diagnoses.errBackslashEnd);
                    }
                    else if (context.now === internals.components.contextComment) {
                        updateResult(internals.diagnoses.errUnclosedComment);
                    }
                    else if (context.now === internals.components.literal) {
                        updateResult(internals.diagnoses.errUnclosedDomainLiteral);
                    }
                    else if (token === '\r') {
                        updateResult(internals.diagnoses.errFWSCRLFEnd);
                    }
                    else if (parseData.domain.length === 0) {
                        updateResult(internals.diagnoses.errNoDomain);
                    }
                    else if (elementLength === 0) {
                        updateResult(internals.diagnoses.errDotEnd);
                    }
                    else if (hyphenFlag) {
                        updateResult(internals.diagnoses.errDomainHyphenEnd);
                    }

                    // Other errors
                    else if (punycodeLength > 255) {
                        // http://tools.ietf.org/html/rfc5321#section-4.5.3.1.2
                        //   The maximum total length of a domain name or number is 255 octets.
                        updateResult(internals.diagnoses.rfc5322DomainTooLong);
                    }
                    else if (Buffer$1.byteLength(parseData.local, 'utf8') + punycodeLength + /* '@' */ 1 > 254) {
                        // http://tools.ietf.org/html/rfc5321#section-4.1.2
                        //   Forward-path   = Path
                        //
                        //   Path           = "<" [ A-d-l ":" ] Mailbox ">"
                        //
                        // http://tools.ietf.org/html/rfc5321#section-4.5.3.1.3
                        //   The maximum total length of a reverse-path or forward-path is 256 octets (including the punctuation and element separators).
                        //
                        // Thus, even without (obsolete) routing information, the Mailbox can only be 254 characters long. This is confirmed by this verified
                        // erratum to RFC 3696:
                        //
                        // http://www.rfc-editor.org/errata_search.php?rfc=3696&eid=1690
                        //   However, there is a restriction in RFC 2821 on the length of an address in MAIL and RCPT commands of 254 characters.  Since
                        //   addresses that do not fit in those fields are not normally useful, the upper limit on address lengths should normally be considered
                        //   to be 254.
                        updateResult(internals.diagnoses.rfc5322TooLong);
                    }
                    else if (elementLength > 63) {
                        // http://tools.ietf.org/html/rfc1035#section-2.3.4
                        // labels   63 octets or less
                        updateResult(internals.diagnoses.rfc5322LabelTooLong);
                    }
                    else if (options.minDomainAtoms && atomData.domains.length < options.minDomainAtoms && (atomData.domains.length !== 1 || atomData.domains[0][0] !== '[')) {
                        updateResult(internals.diagnoses.errDomainTooShort);
                    }
                    else if (internals.hasDomainLiteralThenAtom(atomData.domains)) {
                        updateResult(internals.diagnoses.errDotAfterDomainLiteral);
                    }
                    else if (options.tldWhitelist || options.tldBlacklist) {
                        const tldAtom = atomData.domains[elementCount];

                        if (!internals.validDomain(tldAtom, options)) {
                            updateResult(internals.diagnoses.errUnknownTLD);
                        }
                    }
                } // Check for errors

                // Finish
                if (maxResult < internals.categories.dnsWarn) {
                    // Per RFC 5321, domain atoms are limited to letter-digit-hyphen, so we only need to check code <= 57 to check for a digit
                    const code = atomData.domains[elementCount].codePointAt(0);

                    if (code <= 57) {
                        updateResult(internals.diagnoses.rfc5321TLDNumeric);
                    }
                }

                if (maxResult < threshold) {
                    maxResult = internals.diagnoses.valid;
                }

                const finishResult = diagnose ? maxResult : maxResult < internals.defaultThreshold;

                // $lab:coverage:off$
                if (callback) {
                    callback(finishResult);
                }
                // $lab:coverage:on$

                return finishResult;
            };


            var diagnoses = internals.validate.diagnoses = (function () {

                const diag = {};
                const keys = Object.keys(internals.diagnoses);
                for (let i = 0; i < keys.length; ++i) {
                    const key = keys[i];
                    diag[key] = internals.diagnoses[key];
                }

                return diag;
            })();


            var normalize = internals.normalize;

            var lib = {
            	validate: validate,
            	diagnoses: diagnoses,
            	normalize: normalize
            };

            var constants = {
              error: {
                length: 'Length should be a valid positive number',
                password: 'Password should be a valid string'
              },
              regex: {
                digits: /\d+/,
                letters: /[a-zA-Z]+/,
                symbols: /[`~\!@#\$%\^\&\*\(\)\-_\=\+\[\{\}\]\\\|;:'",<.>\/\?€£¥₹]+/,
                spaces: /[\s]+/
              }
            };

            /**
             * Generic method to test regex
             *
             * @private
             * @param {string} regex - regex to test
             *                           with password
             */
            var regex = constants.regex;

            function _process(regexp) {
              return new RegExp(regexp).test(this.password) === this.positive;
            }

            var lib$1 = {

              /**
               * Method to invert the next validations
               *
               * @param {RegExp} [symbol] - custom Regex which should not be present
               */
              not: function not(symbol) {
                this.positive = false;
                if (symbol) {
                  return _process.call(this, symbol);
                }
                return true;
              },

              /**
               * Method to invert the effects of not()
               *
               * @param {RegExp} [symbol] - custom Regex which should be present
               */
              has: function has(symbol) {
                this.positive = true;
                if (symbol) {
                  return _process.call(this, symbol);
                }
                return true;
              },

              /**
               * Method to invert the effects of not() and
               * to make the api readable and chainable
               *
               */
              is: function is() {
                this.positive = true;
                return true;
              },

              /**
               * Method to specify a minimum length
               *
               * @param {number} num - minimum length
               */
              min: function min(num) {
                return this.password.length >= num;
              },

              /**
               * Method to specify a maximum length
               *
               * @param {number} num - maximum length
               */
              max: function max(num) {
                return this.password.length <= num;
              },

              /**
               * Method to validate the presence of digits
               */
              digits: function digits() {
                return _process.call(this, regex.digits);
              },

              /**
               * Method to validate the presence of letters
               */
              letters: function letters() {
                return _process.call(this, regex.letters);
              },

              /**
               * Method to validate the presence of uppercase letters
               */
              uppercase: function uppercase() {
                return (this.password !== this.password.toLowerCase()) === this.positive;
              },

              /**
               * Method to validate the presence of lowercase letters
               */
              lowercase: function lowercase() {
                return (this.password !== this.password.toUpperCase()) === this.positive;
              },

              /**
               * Method to validate the presence of symbols
               */
              symbols: function symbols() {
                return _process.call(this, regex.symbols);
              },

              /**
               * Method to validate the presence of space
               */
              spaces: function spaces() {
                return _process.call(this, regex.spaces);
              },

              /**
               * Method to provide pre-defined values for password
               *
               * @param {array} list - list of values allowed
               */
              oneOf: function oneOf(list) {
                return list.indexOf(this.password) >= 0 === this.positive;
              }
            };

            var error$1 = constants.error;

            /**
             * Validates that a number is a valid length (positive number)
             *
             * @private
             * @param {number} num - Number to validate
             */
            function _validateLength(num) {
              if (isNaN(Number(num))) {
                throw new Error(error$1.length);
              }
            }

            /**
             * Tests a validation and return the result
             *
             * @private
             * @param {string} property - Property to validate
             * @return {boolean} Boolean value indicting the validity
             *           of the password against the property
             */
            function _isPasswordValidFor(property) {
              return lib$1[property.method].apply(this, property.arguments);
            }

            /**
             * Registers the properties of a password-validation schema object
             *
             * @private
             * @param {string} func - Property name
             * @param {array} args - arguments for the func property
             */
            function _register(func, args) {
              // Add property to the schema
              this.properties.push({ method: func, arguments: args });
              return this;
            }

            class PasswordValidator {
              /**
               * Creates a password-validator schema
               *
               * @constructor
               */
              constructor() {
                this.properties = [];
              }

              /**
               * Method to validate the password against schema
               *
               * @param {string} pwd - password to valdiate
               * @param {object} options - optional options to configure validation
               * @param {boolean} [options.list] - asks for a list of validation
               *           failures instead of just true/false
               * @return {boolean|array} Boolean value indicting the validity
               *           of the password as per schema, if 'options.list'
               *           is not set. Otherwise, it returns an array of
               *           property names which failed validations
               */
              validate(pwd, options) {
                this.list = Boolean(options && options.list);
                this.password = String(pwd);

                this.positive = true;

                if (this.list) {
                  return this.properties.reduce((errorList, property) => {
                    // Applies all validations defined in lib one by one
                    if (!_isPasswordValidFor.call(this, property)) {
                      // If the validation for a property fails,
                      // add it to the error list
                      return errorList.concat(property.method);
                    }
                    return errorList;
                  }, []);
                }
                return this.properties.every(_isPasswordValidFor.bind(this));
              }

              /**
               * Rule to mandate the presence of letters in the password
               */
              letters() {
                return _register.call(this, 'letters', arguments);
              }

              /**
               * Rule to mandate the presence of digits in the password
               */
              digits() {
                return _register.call(this, 'digits', arguments);
              }

              /**
               * Rule to mandate the presence of symbols in the password
               */
              symbols() {
                return _register.call(this, 'symbols', arguments);
              }

              /**
               * Rule to specify a minimum length of the password
               *
               * @param {number} num - minimum length
               */
              min(num) {
                _validateLength(num);
                return _register.call(this, 'min', arguments);
              }

              /**
               * Rule to specify a maximum length of the password
               *
               * @param {number} num - maximum length
               */
              max(num) {
                _validateLength(num);
                return _register.call(this, 'max', arguments);
              }

              /**
               * Rule to mandate the presence of lowercase letters in the password
               */
              lowercase() {
                return _register.call(this, 'lowercase', arguments);
              }

              /**
               * Rule to mandate the presence of uppercase letters in the password
               */
              uppercase() {
                return _register.call(this, 'uppercase', arguments);
              }

              /**
               * Rule to mandate the presence of space in the password
               * It can be used along with 'not' to not allow spaces
               * in the password
               */
              spaces() {
                return _register.call(this, 'spaces', arguments);
              }

              /**
               * Rule to invert the effects of 'not'
               * Apart from that, 'has' is also used
               * to make the api readable and chainable
               */
              has() {
                return _register.call(this, 'has', arguments);
              }

              /**
               * Rule to invert the next applied rules.
               * All the rules applied after 'not' will have opposite effect,
               * until 'has' rule is applied
               */
              not() {
                return _register.call(this, 'not', arguments);
              }

              /**
               * Rule to invert the effects of 'not'
               * Apart from that, 'is' is also used
               * to make the api readable and chainable
               */
              is() {
                return _register.call(this, 'is', arguments);
              }

              /**
               * Rule to whitelist words to be used as password
               *
               * @param {array} list - list of values allowed
               */
              oneOf() {
                return _register.call(this, 'oneOf', arguments);
              }
            }

            var src = PasswordValidator;

            function validateEmail(email) {
                return lib.validate(email);
            }
            var schema = new src();
            schema
                .min(8)
                .max(20)
                .uppercase()
                .lowercase()
                .digits()
                .not().spaces();
            var PASSWORD_MAP = {
                min: 'Password length must be of 8-20',
                max: 'Password length must be of 8-20',
                uppercase: 'Password must have uppercase',
                lowercase: 'Password must have lowercase',
                digits: 'Password must have digits',
                spaces: 'Password cannot have spaces'
            };
            function validatePassword(password) {
                if (!password) {
                    return [PASSWORD_MAP['min']];
                }
                var resultList = schema.validate(password, { list: true });
                return resultList.map(function (r) { return PASSWORD_MAP[r]; });
            }

            var givenPropertiesData = function () { return [
                {
                    name: "Winstar Village Apartments",
                    origin: '',
                    originId: 'originId',
                    description: "Winstar Village is everything you have been waiting for in the Texoma area! We pride ourselves in professional management and offering the very finest in Apartment Living. Our expansive community offers a Central Park with walking trails, and Clubhouse for entertaining. Enjoy the Clubroom for meetings or gatherings, play pool and enjoy snacks by the fireplace, or get fit in our Fitness Center. If movies or theatre setting is on your favorites list... WE HAVE THAT! Enjoy Thunder Basketball, Golf, Football, or take in a movie, we have plenty of seating for everyone. Enjoy Winstar Casino,less than a mile away, or zip on down I-35 to Dallas for the weekend. Winstar Village is THE Destination!",
                    floorPlans: [
                        {
                            bedrooms: 1,
                            bathrooms: 1,
                            originId: '',
                            originType: '',
                            name: "A1",
                            unit: "",
                            availability: "available-soon",
                            photos: [],
                            size: 660,
                            deposit: 0,
                            price: [639, 709]
                        },
                        {
                            bedrooms: 1,
                            originId: '',
                            originType: '',
                            bathrooms: 1,
                            name: "A2",
                            unit: "",
                            availability: "available-soon",
                            photos: [],
                            size: 710,
                            deposit: 0,
                            price: [685, 759]
                        },
                        {
                            bedrooms: 2,
                            originId: '',
                            originType: '',
                            bathrooms: 2,
                            name: "B1",
                            unit: "",
                            availability: "available-soon",
                            photos: [],
                            size: 895,
                            deposit: 0,
                            price: [777, 969]
                        },
                        {
                            bedrooms: 2,
                            originId: '',
                            originType: '',
                            bathrooms: 2,
                            name: "B2",
                            unit: "",
                            availability: "available-soon",
                            photos: [],
                            size: 955,
                            deposit: 0,
                            price: [809, 1009]
                        },
                        {
                            bedrooms: 3,
                            originId: '',
                            originType: '',
                            bathrooms: 2,
                            name: "C1",
                            unit: "",
                            availability: "available-soon",
                            photos: [],
                            size: 1310,
                            deposit: 0,
                            price: [1124, 1320]
                        }
                    ],
                    amenities: [
                        {
                            category: "unique-features",
                            name: "custom",
                            title: "24 hour shuttle access to WinStar Casino"
                        },
                        {
                            category: "unique-features",
                            name: "custom",
                            title: "Breakfast Bar"
                        },
                        {
                            category: "unique-features",
                            name: "custom",
                            title: "Freeway Access"
                        },
                        {
                            category: "unique-features",
                            name: "custom",
                            title: "Gift Wrap Station"
                        },
                        {
                            category: "unique-features",
                            name: "custom",
                            title: "High Ceilings"
                        },
                        {
                            category: "unique-features",
                            name: "custom",
                            title: "Less than 1 Mile from Winstar Casino!"
                        },
                        {
                            category: "unique-features",
                            name: "custom",
                            title: "Linen Storage"
                        },
                        {
                            category: "unique-features",
                            name: "custom",
                            title: "Theatre Room"
                        },
                        {
                            category: "unique-features",
                            name: "custom",
                            title: "Wood Laminate flooring"
                        },
                        {
                            category: "services",
                            name: "package-service",
                            title: "Package Service"
                        },
                        {
                            category: "services",
                            name: "community-wide-wifi",
                            title: "Community-Wide WiFi"
                        },
                        {
                            category: "services",
                            name: "wi-fi-at-pool-and-clubhouse",
                            title: "Wi-Fi at Pool and Clubhouse"
                        },
                        {
                            category: "services",
                            name: "maintenance-on-site",
                            title: "Maintenance on site"
                        },
                        {
                            category: "services",
                            name: "property-manager-on-site",
                            title: "Property Manager on Site"
                        },
                        {
                            category: "services",
                            name: "24-hour-availability",
                            title: "24 Hour Availability"
                        },
                        {
                            category: "services",
                            name: "furnished-units-available",
                            title: "Furnished Units Available"
                        },
                        {
                            category: "services",
                            name: "renters-insurance-program",
                            title: "Renters Insurance Program"
                        },
                        {
                            category: "services",
                            name: "online-services",
                            title: "Online Services"
                        },
                        {
                            category: "services",
                            name: "planned-social-activities",
                            title: "Planned Social Activities"
                        },
                        {
                            category: "services",
                            name: "guest-apartment",
                            title: "Guest Apartment"
                        },
                        {
                            category: "interior",
                            name: "business-center",
                            title: "Business Center"
                        },
                        {
                            category: "interior",
                            name: "clubhouse",
                            title: "Clubhouse"
                        },
                        {
                            category: "interior",
                            name: "lounge",
                            title: "Lounge"
                        },
                        {
                            category: "interior",
                            name: "multi-use-room",
                            title: "Multi Use Room"
                        },
                        {
                            category: "interior",
                            name: "breakfastcoffee-concierge",
                            title: "Breakfast/Coffee Concierge"
                        },
                        {
                            category: "interior",
                            name: "conference-room",
                            title: "Conference Room"
                        },
                        {
                            category: "interior",
                            name: "corporate-suites",
                            title: "Corporate Suites"
                        },
                        {
                            category: "outdoor-space",
                            name: "courtyard",
                            title: "Courtyard"
                        },
                        {
                            category: "outdoor-space",
                            name: "grill",
                            title: "Grill"
                        },
                        {
                            category: "outdoor-space",
                            name: "picnic-area",
                            title: "Picnic Area"
                        },
                        {
                            category: "outdoor-space",
                            name: "balcony",
                            title: "Balcony"
                        },
                        {
                            category: "outdoor-space",
                            name: "patio",
                            title: "Patio"
                        },
                        {
                            category: "outdoor-space",
                            name: "porch",
                            title: "Porch"
                        },
                        {
                            category: "outdoor-space",
                            name: "deck",
                            title: "Deck"
                        },
                        {
                            category: "outdoor-space",
                            name: "grill",
                            title: "Grill"
                        },
                        {
                            category: "fitness-&-recreation",
                            name: "fitness-center",
                            title: "Fitness Center"
                        },
                        {
                            category: "fitness-&-recreation",
                            name: "pool",
                            title: "Pool"
                        },
                        {
                            category: "fitness-&-recreation",
                            name: "bike-storage",
                            title: "Bike Storage"
                        },
                        {
                            category: "fitness-&-recreation",
                            name: "basketball-court",
                            title: "Basketball Court"
                        },
                        {
                            category: "fitness-&-recreation",
                            name: "volleyball-court",
                            title: "Volleyball Court"
                        },
                        {
                            category: "fitness-&-recreation",
                            name: "walkingbiking-trails",
                            title: "Walking/Biking Trails"
                        },
                        {
                            category: "fitness-&-recreation",
                            name: "gameroom",
                            title: "Gameroom"
                        },
                        {
                            category: "fitness-&-recreation",
                            name: "media-centermovie-theatre",
                            title: "Media Center/Movie Theatre"
                        },
                        {
                            category: "features",
                            name: "high-speed-internet-access",
                            title: "High Speed Internet Access"
                        },
                        {
                            category: "features",
                            name: "wi-fi",
                            title: "Wi-Fi"
                        },
                        {
                            category: "features",
                            name: "washerdryer---in-unit",
                            title: "Washer/Dryer - In Unit"
                        },
                        {
                            category: "features",
                            name: "air-conditioning",
                            title: "Air Conditioning"
                        },
                        {
                            category: "features",
                            name: "heating",
                            title: "Heating"
                        },
                        {
                            category: "features",
                            name: "ceiling-fans",
                            title: "Ceiling Fans"
                        },
                        {
                            category: "features",
                            name: "cable-ready",
                            title: "Cable Ready"
                        },
                        {
                            category: "features",
                            name: "satellite-tv",
                            title: "Satellite TV"
                        },
                        {
                            category: "features",
                            name: "tubshower",
                            title: "Tub/Shower"
                        },
                        {
                            category: "features",
                            name: "handrails",
                            title: "Handrails"
                        },
                        {
                            category: "features",
                            name: "sprinkler-system",
                            title: "Sprinkler System"
                        },
                        {
                            category: "features",
                            name: "wheelchair-accessible-rooms",
                            title: "Wheelchair Accessible (Rooms)"
                        },
                        {
                            category: "kitchen",
                            name: "dishwasher",
                            title: "Dishwasher"
                        },
                        {
                            category: "kitchen",
                            name: "disposal",
                            title: "Disposal"
                        },
                        {
                            category: "kitchen",
                            name: "ice-maker",
                            title: "Ice Maker"
                        },
                        {
                            category: "kitchen",
                            name: "pantry",
                            title: "Pantry"
                        },
                        {
                            category: "kitchen",
                            name: "kitchen",
                            title: "Kitchen"
                        },
                        {
                            category: "kitchen",
                            name: "microwave",
                            title: "Microwave"
                        },
                        {
                            category: "kitchen",
                            name: "oven",
                            title: "Oven"
                        },
                        {
                            category: "kitchen",
                            name: "range",
                            title: "Range"
                        },
                        {
                            category: "kitchen",
                            name: "refrigerator",
                            title: "Refrigerator"
                        },
                        {
                            category: "kitchen",
                            name: "freezer",
                            title: "Freezer"
                        },
                        {
                            category: "kitchen",
                            name: "instant-hot-water",
                            title: "Instant Hot Water"
                        },
                        {
                            category: "living-space",
                            name: "hardwood-floors",
                            title: "Hardwood Floors"
                        },
                        {
                            category: "living-space",
                            name: "carpet",
                            title: "Carpet"
                        },
                        {
                            category: "living-space",
                            name: "vinyl-flooring",
                            title: "Vinyl Flooring"
                        },
                        {
                            category: "living-space",
                            name: "dining-room",
                            title: "Dining Room"
                        },
                        {
                            category: "living-space",
                            name: "views",
                            title: "Views"
                        },
                        {
                            category: "living-space",
                            name: "walk-in-closets",
                            title: "Walk-In Closets"
                        },
                        {
                            category: "living-space",
                            name: "furnished",
                            title: "Furnished"
                        },
                        {
                            category: "living-space",
                            name: "double-pane-windows",
                            title: "Double Pane Windows"
                        },
                        {
                            category: "security",
                            name: "package-service",
                            title: "Package Service"
                        },
                        {
                            category: "security",
                            name: "property-manager-on-site",
                            title: "Property Manager on Site"
                        }
                    ],
                    expenses: [
                        {
                            type: "recurring",
                            name: "Cat Rent",
                            price: "$10"
                        },
                        {
                            type: "recurring",
                            name: "Dog Rent",
                            price: "$10"
                        },
                        {
                            type: "one-time",
                            name: "Admin Fee",
                            price: "$100"
                        },
                        {
                            type: "one-time",
                            name: "Application Fee",
                            price: "$40"
                        },
                        {
                            type: "one-time",
                            name: "Cat Fee",
                            price: "$400"
                        },
                        {
                            type: "one-time",
                            name: "Dog Fee",
                            price: "$400"
                        },
                        {
                            type: "utilities-included",
                            name: "Trash Removal",
                            price: "Included"
                        }
                    ],
                    languages: ["English"],
                    officeHours: [
                        {
                            dayOfWeek: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"],
                            opens: "09:00:00",
                            closes: "18:00:00"
                        },
                        {
                            dayOfWeek: ["Saturday"],
                            opens: "10:00:00",
                            closes: "16:00:00"
                        }
                    ],
                    address: {
                        location: {
                            type: "Point",
                            coordinates: [33.77088, -97.12735]
                        },
                        formatted: '',
                        components: []
                    },
                    photos: [
                        {
                            src: "https://images1.apartments.com/i2/W3qcEWDYujbT-Goc1CtTRgiqXGqYYV9rJDvfJC0TaNA/111/winstar-village-apartments-thackerville-ok-primary-photo.jpg",
                            description: "Winstar Village Apartments",
                            mediaType: 1,
                            attachmentType: 3,
                            altText: "Primary Photo - Winstar Village Apartments"
                        }
                    ]
                },
                {
                    name: "NEW DEVELOPMENT Southern Sky Townhomes",
                    origin: '',
                    originId: 'originId',
                    description: "ABOUT SOUTHERN SKY ESTATES TOWNHOMES\n\nThis is uptown luxury living only minutes from WinStar World Casino and Resort in Thackerville, OK and 10 minutes from Gainesville TX and 27 min to Ardmore  OK. These 2 bedroom, 2.5 bathroom townhomes are starting at $1150 and very spacious at 1053 – 1114 sq. ft. These striking  townhomes offer lux modern design with extraordinary custom finishes. Conveniently located between IH -35 and US 77 we are just minutes from casinos,  downtown Thackerville,  Thackerville Elementary, convenient stores,  as well as many other local businesses and eateries Winstar has to offer. It just doesn’t get much better than this for country living!\n\nThat feeling in your stomach when you walk in the front door… its love!  Just imagine walking through your front door with 10 foot ceilings and durable scratch resistant wood-look floors your open concept downstairs goes all the way back to your fabulous kitchen with stainless steel appliances, custom cabinets with brushed nickel hardware, pendant lights over quartz countertops that overhang your large kitchen island. The perfect place to eat breakfast or have a drink with a few friends. And so your friends won’t need to travel far there is half bath under the stairs complete with quartz countertops and lux light fixture. Also under the stairs is a bookshelf that slides out for extra hidden storage. As you ascend the stairs to your personal retreat you will notice the  plush carpet under your feet. At the top of the stairs are French doors that open to your full sized laundry room with tile floors and a large shelf to keep all of your laundry essentials. On either side of the laundry room are the bedroom doors which you notice, even the doors are modern! As you enter the master bedroom there is a designer ceiling fan and light and a good sized closet with built-ins for storage (like shoes!) Across the room is your ensuite bathroom which feels like your personal spa with custom cabinets and built-ins, quartz counters, lux tile floors, and a tiled walk-in shower with rainfall showerhead. Across the hall is the second bedroom which also has a built-ins in the closet and an ensuite with a shower/bathtub combo.  It is time to make this dream your reality! Only a few spaces are available so you better hurry!\n\nFEATURES \n\nContemporary digital Stainless Steel appliances\nCustom Island counter top and 14” quartz overhang for seating.\nContemporary Custom cabinets, Shaker doors with soft close hinges. \nCustom flooring throughout with Plush carpet in the bedrooms & stairs. \nLiving Areas Open & Spacious 10' and 9' ceilings w/Natural Light utilization.\nLED energy efficient can lighting 52 inch ceiling fans with LED light kits. \nWasher and dryer hookups conveniently located upstairs between the two bedrooms.  \nEnergy efficient central heating and air conditioning  \nAttractive Landscaping Indigenous Species w/sod and rear 10×10 Patio\n\nDogs and cats allowed with a nonrefundable $250 deposit and monthly  pet rent of $25.  Cats must be house broken and an 80lb weight limit  on dogs.  Pets must be approved  by Management. \n\nCONTACT\n\nFor further details please email us at sevenkeller1@gmail.com",
                    floorPlans: [
                        {
                            bedrooms: 2,
                            bathrooms: 2,
                            originId: '',
                            originType: '',
                            name: "Center Unit",
                            unit: "19942",
                            availability: "available-soon",
                            photos: [],
                            size: 1053,
                            deposit: 0,
                            price: [1150, 1150]
                        },
                        {
                            bedrooms: 2,
                            bathrooms: 2,
                            originId: '',
                            originType: '',
                            name: "End Unit",
                            unit: "19862",
                            availability: "available-soon",
                            photos: [],
                            size: 1114,
                            deposit: 0,
                            price: [1200, 1200]
                        }
                    ],
                    amenities: [
                        {
                            category: "features",
                            name: "washerdryer-hookup",
                            title: "Washer/Dryer Hookup"
                        },
                        {
                            category: "features",
                            name: "air-conditioning",
                            title: "Air Conditioning"
                        },
                        {
                            category: "features",
                            name: "cable-ready",
                            title: "Cable Ready"
                        },
                        {
                            category: "kitchen",
                            name: "dishwasher",
                            title: "Dishwasher"
                        },
                        {
                            category: "kitchen",
                            name: "disposal",
                            title: "Disposal"
                        },
                        {
                            category: "kitchen",
                            name: "ice-maker",
                            title: "Ice Maker"
                        },
                        {
                            category: "kitchen",
                            name: "stainless-steel-appliances",
                            title: "Stainless Steel Appliances"
                        },
                        {
                            category: "kitchen",
                            name: "kitchen",
                            title: "Kitchen"
                        },
                        {
                            category: "kitchen",
                            name: "microwave",
                            title: "Microwave"
                        },
                        {
                            category: "kitchen",
                            name: "oven",
                            title: "Oven"
                        },
                        {
                            category: "kitchen",
                            name: "range",
                            title: "Range"
                        },
                        {
                            category: "kitchen",
                            name: "refrigerator",
                            title: "Refrigerator"
                        },
                        {
                            category: "living-space",
                            name: "hardwood-floors",
                            title: "Hardwood Floors"
                        },
                        {
                            category: "living-space",
                            name: "carpet",
                            title: "Carpet"
                        },
                        {
                            category: "living-space",
                            name: "tile-floors",
                            title: "Tile Floors"
                        },
                        {
                            category: "living-space",
                            name: "built-in-bookshelves",
                            title: "Built-In Bookshelves"
                        },
                        {
                            category: "living-space",
                            name: "vaulted-ceiling",
                            title: "Vaulted Ceiling"
                        },
                        {
                            category: "living-space",
                            name: "window-coverings",
                            title: "Window Coverings"
                        },
                        {
                            category: "outdoor-space",
                            name: "patio",
                            title: "Patio"
                        }
                    ],
                    expenses: [
                        {
                            type: "recurring",
                            name: "Cat Rent",
                            price: "$25"
                        },
                        {
                            type: "recurring",
                            name: "Dog Rent",
                            price: "$25"
                        },
                        {
                            type: "one-time",
                            name: "Admin Fee",
                            price: "$50"
                        },
                        {
                            type: "one-time",
                            name: "Application Fee",
                            price: "$29"
                        },
                        {
                            type: "one-time",
                            name: "Cat Deposit",
                            price: "$250"
                        },
                        {
                            type: "one-time",
                            name: "Dog Deposit",
                            price: "$250"
                        },
                        {
                            type: "utilities-included",
                            name: "Trash Removal",
                            price: "Included"
                        }
                    ],
                    languages: ["English"],
                    officeHours: [],
                    address: {
                        location: {
                            type: "Point",
                            coordinates: [33.78135, -97.14083]
                        },
                        formatted: '',
                        components: []
                    },
                    photos: [
                        {
                            src: "https://images1.apartments.com/i2/b0nqx5obKBjJyq-Iz8QtkcLIKQXC7B5NRtinvHZBCgk/111/new-development-southern-sky-townhomes-thackerville-ok-primary-photo.jpg",
                            description: "NEW DEVELOPMENT Southern Sky Townhomes",
                            mediaType: 1,
                            attachmentType: 3,
                            altText: "Primary Photo - NEW DEVELOPMENT Southern Sky Townhomes"
                        }
                    ]
                }
            ]; };

            var givenUserData = function () { return ({
                email: 'test@email.com',
                name: 'test name',
                role: 'admin'
            }); };
            var givenUserCredentialsData = function () { return ({
                email: 'test@email.com',
                password: '1aB234bA1'
            }); };

            function encryptQuery(value) {
                return Buffer.from(JSON.stringify(value)).toString('base64');
            }
            function decodeQuery(text) {
                return JSON.parse(Buffer.from(text, 'base64').toString());
            }

            exports.decodeQuery = decodeQuery;
            exports.encryptQuery = encryptQuery;
            exports.givenPropertiesData = givenPropertiesData;
            exports.givenUserCredentialsData = givenUserCredentialsData;
            exports.givenUserData = givenUserData;
            exports.validateEmail = validateEmail;
            exports.validatePassword = validatePassword;

            Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=apartments-shared.umd.js.map
