import isemail from 'isemail'
import PasswordValidator from 'password-validator'

export function validateEmail(email: string) {
  return isemail.validate(email)
}

const schema = new PasswordValidator()
schema
  .min(8)
  .max(20)
  .uppercase()
  .lowercase()
  .digits()
  .not().spaces()

const PASSWORD_MAP: Record<string, string> = {
  min: 'Password length must be of 8-20',
  max: 'Password length must be of 8-20',
  uppercase: 'Password must have uppercase',
  lowercase: 'Password must have lowercase',
  digits: 'Password must have digits',
  spaces: 'Password cannot have spaces'
}

export function validatePassword(password: string): string[] {
  if(!password) {
    return [PASSWORD_MAP['min']]
  }
  const resultList = schema.validate(password, { list: true }) as string[]
  return resultList.map(r => PASSWORD_MAP[r])
}
