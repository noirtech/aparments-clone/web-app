import { IRentProperty } from "../types";

export const givenPropertiesData: () => Array<Partial<IRentProperty>> = () => [
  {
    name: "Winstar Village Apartments",
    origin: '',
    originId: 'originId',
    description:
      "Winstar Village is everything you have been waiting for in the Texoma area! We pride ourselves in professional management and offering the very finest in Apartment Living. Our expansive community offers a Central Park with walking trails, and Clubhouse for entertaining. Enjoy the Clubroom for meetings or gatherings, play pool and enjoy snacks by the fireplace, or get fit in our Fitness Center. If movies or theatre setting is on your favorites list... WE HAVE THAT! Enjoy Thunder Basketball, Golf, Football, or take in a movie, we have plenty of seating for everyone. Enjoy Winstar Casino,less than a mile away, or zip on down I-35 to Dallas for the weekend. Winstar Village is THE Destination!",
    floorPlans: [
      {
        bedrooms: 1,
        bathrooms: 1,
        originId: '',
        originType: '',
        name: "A1",
        unit: "",
        availability: "available-soon",
        photos: [],
        size: 660,
        deposit: 0,
        price: [639, 709]
      },
      {
        bedrooms: 1,
        originId: '',
        originType: '',
        bathrooms: 1,
        name: "A2",
        unit: "",
        availability: "available-soon",
        photos: [],
        size: 710,
        deposit: 0,
        price: [685, 759]
      },
      {
        bedrooms: 2,
        originId: '',
        originType: '',
        bathrooms: 2,
        name: "B1",
        unit: "",
        availability: "available-soon",
        photos: [],
        size: 895,
        deposit: 0,
        price: [777, 969]
      },
      {
        bedrooms: 2,
        originId: '',
        originType: '',
        bathrooms: 2,
        name: "B2",
        unit: "",
        availability: "available-soon",
        photos: [],
        size: 955,
        deposit: 0,
        price: [809, 1009]
      },
      {
        bedrooms: 3,
        originId: '',
        originType: '',
        bathrooms: 2,
        name: "C1",
        unit: "",
        availability: "available-soon",
        photos: [],
        size: 1310,
        deposit: 0,
        price: [1124, 1320]
      }
    ],
    amenities: [
      {
        category: "unique-features",
        name: "custom",
        title: "24 hour shuttle access to WinStar Casino"
      },
      {
        category: "unique-features",
        name: "custom",
        title: "Breakfast Bar"
      },
      {
        category: "unique-features",
        name: "custom",
        title: "Freeway Access"
      },
      {
        category: "unique-features",
        name: "custom",
        title: "Gift Wrap Station"
      },
      {
        category: "unique-features",
        name: "custom",
        title: "High Ceilings"
      },
      {
        category: "unique-features",
        name: "custom",
        title: "Less than 1 Mile from Winstar Casino!"
      },
      {
        category: "unique-features",
        name: "custom",
        title: "Linen Storage"
      },
      {
        category: "unique-features",
        name: "custom",
        title: "Theatre Room"
      },
      {
        category: "unique-features",
        name: "custom",
        title: "Wood Laminate flooring"
      },
      {
        category: "services",
        name: "package-service",
        title: "Package Service"
      },
      {
        category: "services",
        name: "community-wide-wifi",
        title: "Community-Wide WiFi"
      },
      {
        category: "services",
        name: "wi-fi-at-pool-and-clubhouse",
        title: "Wi-Fi at Pool and Clubhouse"
      },
      {
        category: "services",
        name: "maintenance-on-site",
        title: "Maintenance on site"
      },
      {
        category: "services",
        name: "property-manager-on-site",
        title: "Property Manager on Site"
      },
      {
        category: "services",
        name: "24-hour-availability",
        title: "24 Hour Availability"
      },
      {
        category: "services",
        name: "furnished-units-available",
        title: "Furnished Units Available"
      },
      {
        category: "services",
        name: "renters-insurance-program",
        title: "Renters Insurance Program"
      },
      {
        category: "services",
        name: "online-services",
        title: "Online Services"
      },
      {
        category: "services",
        name: "planned-social-activities",
        title: "Planned Social Activities"
      },
      {
        category: "services",
        name: "guest-apartment",
        title: "Guest Apartment"
      },
      {
        category: "interior",
        name: "business-center",
        title: "Business Center"
      },
      {
        category: "interior",
        name: "clubhouse",
        title: "Clubhouse"
      },
      {
        category: "interior",
        name: "lounge",
        title: "Lounge"
      },
      {
        category: "interior",
        name: "multi-use-room",
        title: "Multi Use Room"
      },
      {
        category: "interior",
        name: "breakfastcoffee-concierge",
        title: "Breakfast/Coffee Concierge"
      },
      {
        category: "interior",
        name: "conference-room",
        title: "Conference Room"
      },
      {
        category: "interior",
        name: "corporate-suites",
        title: "Corporate Suites"
      },
      {
        category: "outdoor-space",
        name: "courtyard",
        title: "Courtyard"
      },
      {
        category: "outdoor-space",
        name: "grill",
        title: "Grill"
      },
      {
        category: "outdoor-space",
        name: "picnic-area",
        title: "Picnic Area"
      },
      {
        category: "outdoor-space",
        name: "balcony",
        title: "Balcony"
      },
      {
        category: "outdoor-space",
        name: "patio",
        title: "Patio"
      },
      {
        category: "outdoor-space",
        name: "porch",
        title: "Porch"
      },
      {
        category: "outdoor-space",
        name: "deck",
        title: "Deck"
      },
      {
        category: "outdoor-space",
        name: "grill",
        title: "Grill"
      },
      {
        category: "fitness-&-recreation",
        name: "fitness-center",
        title: "Fitness Center"
      },
      {
        category: "fitness-&-recreation",
        name: "pool",
        title: "Pool"
      },
      {
        category: "fitness-&-recreation",
        name: "bike-storage",
        title: "Bike Storage"
      },
      {
        category: "fitness-&-recreation",
        name: "basketball-court",
        title: "Basketball Court"
      },
      {
        category: "fitness-&-recreation",
        name: "volleyball-court",
        title: "Volleyball Court"
      },
      {
        category: "fitness-&-recreation",
        name: "walkingbiking-trails",
        title: "Walking/Biking Trails"
      },
      {
        category: "fitness-&-recreation",
        name: "gameroom",
        title: "Gameroom"
      },
      {
        category: "fitness-&-recreation",
        name: "media-centermovie-theatre",
        title: "Media Center/Movie Theatre"
      },
      {
        category: "features",
        name: "high-speed-internet-access",
        title: "High Speed Internet Access"
      },
      {
        category: "features",
        name: "wi-fi",
        title: "Wi-Fi"
      },
      {
        category: "features",
        name: "washerdryer---in-unit",
        title: "Washer/Dryer - In Unit"
      },
      {
        category: "features",
        name: "air-conditioning",
        title: "Air Conditioning"
      },
      {
        category: "features",
        name: "heating",
        title: "Heating"
      },
      {
        category: "features",
        name: "ceiling-fans",
        title: "Ceiling Fans"
      },
      {
        category: "features",
        name: "cable-ready",
        title: "Cable Ready"
      },
      {
        category: "features",
        name: "satellite-tv",
        title: "Satellite TV"
      },
      {
        category: "features",
        name: "tubshower",
        title: "Tub/Shower"
      },
      {
        category: "features",
        name: "handrails",
        title: "Handrails"
      },
      {
        category: "features",
        name: "sprinkler-system",
        title: "Sprinkler System"
      },
      {
        category: "features",
        name: "wheelchair-accessible-rooms",
        title: "Wheelchair Accessible (Rooms)"
      },
      {
        category: "kitchen",
        name: "dishwasher",
        title: "Dishwasher"
      },
      {
        category: "kitchen",
        name: "disposal",
        title: "Disposal"
      },
      {
        category: "kitchen",
        name: "ice-maker",
        title: "Ice Maker"
      },
      {
        category: "kitchen",
        name: "pantry",
        title: "Pantry"
      },
      {
        category: "kitchen",
        name: "kitchen",
        title: "Kitchen"
      },
      {
        category: "kitchen",
        name: "microwave",
        title: "Microwave"
      },
      {
        category: "kitchen",
        name: "oven",
        title: "Oven"
      },
      {
        category: "kitchen",
        name: "range",
        title: "Range"
      },
      {
        category: "kitchen",
        name: "refrigerator",
        title: "Refrigerator"
      },
      {
        category: "kitchen",
        name: "freezer",
        title: "Freezer"
      },
      {
        category: "kitchen",
        name: "instant-hot-water",
        title: "Instant Hot Water"
      },
      {
        category: "living-space",
        name: "hardwood-floors",
        title: "Hardwood Floors"
      },
      {
        category: "living-space",
        name: "carpet",
        title: "Carpet"
      },
      {
        category: "living-space",
        name: "vinyl-flooring",
        title: "Vinyl Flooring"
      },
      {
        category: "living-space",
        name: "dining-room",
        title: "Dining Room"
      },
      {
        category: "living-space",
        name: "views",
        title: "Views"
      },
      {
        category: "living-space",
        name: "walk-in-closets",
        title: "Walk-In Closets"
      },
      {
        category: "living-space",
        name: "furnished",
        title: "Furnished"
      },
      {
        category: "living-space",
        name: "double-pane-windows",
        title: "Double Pane Windows"
      },
      {
        category: "security",
        name: "package-service",
        title: "Package Service"
      },
      {
        category: "security",
        name: "property-manager-on-site",
        title: "Property Manager on Site"
      }
    ],
    expenses: [
      {
        type: "recurring",
        name: "Cat Rent",
        price: "$10"
      },
      {
        type: "recurring",
        name: "Dog Rent",
        price: "$10"
      },
      {
        type: "one-time",
        name: "Admin Fee",
        price: "$100"
      },
      {
        type: "one-time",
        name: "Application Fee",
        price: "$40"
      },
      {
        type: "one-time",
        name: "Cat Fee",
        price: "$400"
      },
      {
        type: "one-time",
        name: "Dog Fee",
        price: "$400"
      },
      {
        type: "utilities-included",
        name: "Trash Removal",
        price: "Included"
      }
    ],
    languages: ["English"],
    officeHours: [
      {
        dayOfWeek: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"],
        opens: "09:00:00",
        closes: "18:00:00"
      },
      {
        dayOfWeek: ["Saturday"],
        opens: "10:00:00",
        closes: "16:00:00"
      }
    ],
    address: {
      location: {
        type: "Point",
        coordinates: [33.77088, -97.12735]
      },
      formatted: '',
      components: []
    },
    photos: [
      {
        src:
          "https://images1.apartments.com/i2/W3qcEWDYujbT-Goc1CtTRgiqXGqYYV9rJDvfJC0TaNA/111/winstar-village-apartments-thackerville-ok-primary-photo.jpg",
        description: "Winstar Village Apartments",
        mediaType: 1,
        attachmentType: 3,
        altText: "Primary Photo - Winstar Village Apartments"
      }
    ]
  },
  {
    name: "NEW DEVELOPMENT Southern Sky Townhomes",
    origin: '',
    originId: 'originId',
    description:
      "ABOUT SOUTHERN SKY ESTATES TOWNHOMES\n\nThis is uptown luxury living only minutes from WinStar World Casino and Resort in Thackerville, OK and 10 minutes from Gainesville TX and 27 min to Ardmore  OK. These 2 bedroom, 2.5 bathroom townhomes are starting at $1150 and very spacious at 1053 – 1114 sq. ft. These striking  townhomes offer lux modern design with extraordinary custom finishes. Conveniently located between IH -35 and US 77 we are just minutes from casinos,  downtown Thackerville,  Thackerville Elementary, convenient stores,  as well as many other local businesses and eateries Winstar has to offer. It just doesn’t get much better than this for country living!\n\nThat feeling in your stomach when you walk in the front door… its love!  Just imagine walking through your front door with 10 foot ceilings and durable scratch resistant wood-look floors your open concept downstairs goes all the way back to your fabulous kitchen with stainless steel appliances, custom cabinets with brushed nickel hardware, pendant lights over quartz countertops that overhang your large kitchen island. The perfect place to eat breakfast or have a drink with a few friends. And so your friends won’t need to travel far there is half bath under the stairs complete with quartz countertops and lux light fixture. Also under the stairs is a bookshelf that slides out for extra hidden storage. As you ascend the stairs to your personal retreat you will notice the  plush carpet under your feet. At the top of the stairs are French doors that open to your full sized laundry room with tile floors and a large shelf to keep all of your laundry essentials. On either side of the laundry room are the bedroom doors which you notice, even the doors are modern! As you enter the master bedroom there is a designer ceiling fan and light and a good sized closet with built-ins for storage (like shoes!) Across the room is your ensuite bathroom which feels like your personal spa with custom cabinets and built-ins, quartz counters, lux tile floors, and a tiled walk-in shower with rainfall showerhead. Across the hall is the second bedroom which also has a built-ins in the closet and an ensuite with a shower/bathtub combo.  It is time to make this dream your reality! Only a few spaces are available so you better hurry!\n\nFEATURES \n\nContemporary digital Stainless Steel appliances\nCustom Island counter top and 14” quartz overhang for seating.\nContemporary Custom cabinets, Shaker doors with soft close hinges. \nCustom flooring throughout with Plush carpet in the bedrooms & stairs. \nLiving Areas Open & Spacious 10' and 9' ceilings w/Natural Light utilization.\nLED energy efficient can lighting 52 inch ceiling fans with LED light kits. \nWasher and dryer hookups conveniently located upstairs between the two bedrooms.  \nEnergy efficient central heating and air conditioning  \nAttractive Landscaping Indigenous Species w/sod and rear 10×10 Patio\n\nDogs and cats allowed with a nonrefundable $250 deposit and monthly  pet rent of $25.  Cats must be house broken and an 80lb weight limit  on dogs.  Pets must be approved  by Management. \n\nCONTACT\n\nFor further details please email us at sevenkeller1@gmail.com",
    floorPlans: [
      {
        bedrooms: 2,
        bathrooms: 2,
        originId: '',
        originType: '',
        name: "Center Unit",
        unit: "19942",
        availability: "available-soon",
        photos: [],
        size: 1053,
        deposit: 0,
        price: [1150, 1150]
      },
      {
        bedrooms: 2,
        bathrooms: 2,
        originId: '',
        originType: '',
        name: "End Unit",
        unit: "19862",
        availability: "available-soon",
        photos: [],
        size: 1114,
        deposit: 0,
        price: [1200, 1200]
      }
    ],
    amenities: [
      {
        category: "features",
        name: "washerdryer-hookup",
        title: "Washer/Dryer Hookup"
      },
      {
        category: "features",
        name: "air-conditioning",
        title: "Air Conditioning"
      },
      {
        category: "features",
        name: "cable-ready",
        title: "Cable Ready"
      },
      {
        category: "kitchen",
        name: "dishwasher",
        title: "Dishwasher"
      },
      {
        category: "kitchen",
        name: "disposal",
        title: "Disposal"
      },
      {
        category: "kitchen",
        name: "ice-maker",
        title: "Ice Maker"
      },
      {
        category: "kitchen",
        name: "stainless-steel-appliances",
        title: "Stainless Steel Appliances"
      },
      {
        category: "kitchen",
        name: "kitchen",
        title: "Kitchen"
      },
      {
        category: "kitchen",
        name: "microwave",
        title: "Microwave"
      },
      {
        category: "kitchen",
        name: "oven",
        title: "Oven"
      },
      {
        category: "kitchen",
        name: "range",
        title: "Range"
      },
      {
        category: "kitchen",
        name: "refrigerator",
        title: "Refrigerator"
      },
      {
        category: "living-space",
        name: "hardwood-floors",
        title: "Hardwood Floors"
      },
      {
        category: "living-space",
        name: "carpet",
        title: "Carpet"
      },
      {
        category: "living-space",
        name: "tile-floors",
        title: "Tile Floors"
      },
      {
        category: "living-space",
        name: "built-in-bookshelves",
        title: "Built-In Bookshelves"
      },
      {
        category: "living-space",
        name: "vaulted-ceiling",
        title: "Vaulted Ceiling"
      },
      {
        category: "living-space",
        name: "window-coverings",
        title: "Window Coverings"
      },
      {
        category: "outdoor-space",
        name: "patio",
        title: "Patio"
      }
    ],
    expenses: [
      {
        type: "recurring",
        name: "Cat Rent",
        price: "$25"
      },
      {
        type: "recurring",
        name: "Dog Rent",
        price: "$25"
      },
      {
        type: "one-time",
        name: "Admin Fee",
        price: "$50"
      },
      {
        type: "one-time",
        name: "Application Fee",
        price: "$29"
      },
      {
        type: "one-time",
        name: "Cat Deposit",
        price: "$250"
      },
      {
        type: "one-time",
        name: "Dog Deposit",
        price: "$250"
      },
      {
        type: "utilities-included",
        name: "Trash Removal",
        price: "Included"
      }
    ],
    languages: ["English"],
    officeHours: [],
    address: {
      location: {
        type: "Point",
        coordinates: [33.78135, -97.14083]
      },
      formatted: '',
      components: []
    },
    photos: [
      {
        src:
          "https://images1.apartments.com/i2/b0nqx5obKBjJyq-Iz8QtkcLIKQXC7B5NRtinvHZBCgk/111/new-development-southern-sky-townhomes-thackerville-ok-primary-photo.jpg",
        description: "NEW DEVELOPMENT Southern Sky Townhomes",
        mediaType: 1,
        attachmentType: 3,
        altText: "Primary Photo - NEW DEVELOPMENT Southern Sky Townhomes"
      }
    ]
  }
];
