import { IUser, IUserCredentials } from "../types";

export const givenUserData: () => Partial<IUser> = () => ({
    email: 'test@email.com',
    name: 'test name',
    role: 'admin'
})

export const givenUserCredentialsData: () => IUserCredentials = () => ({
    email: 'test@email.com',
    password: '1aB234bA1'
})
