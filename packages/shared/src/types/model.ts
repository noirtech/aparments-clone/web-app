export interface IFloorPlan {
  originId: string;
  originAttId: number;
  bedrooms: number;
  bathrooms: number;
  price: [number, number];
  photos: IPhoto[];
  name: string;
  unit: string;
  size: number;
  availability: "available-soon" | "available-now" | "none-of-these-are-available" | string;
  deposit: number;
}

export enum AttachmentType {
  PRIMARY = 3,
  BUILDING = 4,
  FLOOR_SKETCH = 10,
  MAP = 16,
  FLOOR = 33,
}

export interface IPhoto {
  src: string;
  description: string;
  mediaType: number;
  attachmentType: AttachmentType;
  altText: string;
}

export interface IAmenity {
  name: string;
  title: string;
  category: string;
  description?: string;
  policies?: string[];
}

export interface INearby {
  name: string;
  category: string;
  driving: string;
  distance: string;
}

export interface ISchool {
  grades: string;
  type: string;
  name: string;
  category: "public" | "private";
  rating: number;
  students: number;
  phone: string;
}

export interface IOfficeHour {
  dayOfWeek: Array<
    "Monday" | "Tuesday" | "Wednesday" | "Thursday" | "Friday" | "Saturday" | "Sunday"
  >;
  opens: string;
  closes: string;
}

export interface ILocation {
  type: "Point";
  coordinates: [number, number];
}

export type ComponentTypes =
  | 'street_number'
  | 'route'
  | 'locality'
  | 'political'
  | 'administrative_area_level_2'
  | 'administrative_area_level_1'
  | 'country'
  | 'postal_code'

export interface IAddressComponent {
  name: string
  types: ComponentTypes[]
}

export interface IGeoLocation {
  location: ILocation;
  formatted: string;
  components: IAddressComponent[]
}

export interface IExpense {
  type: "recurring" | "one-time" | "utilities-included";
  name: string;
  price: number;
}

export interface IFloorPlanAggregate {
  bathRanges: [number, number];
  sizeRanges: [number, number];
  priceRanges: [number, number];
  count: number;
}
export interface IRentAggregate {
  floorPlans: {
    [nBeds: number]: IFloorPlanAggregate;
  };
  minAvailability: string;
}

export interface IRentProperty {
  id: string;
  originId: string;
  origin: string;
  name: string;
  description: string;
  summary?: IRentAggregate;
  cover: string;
  priority: number;
  scam: boolean;
  verified: boolean;
  nearby: INearby[];
  schools: ISchool[];
  floorPlans: IFloorPlan[];
  photos: IPhoto[];
  amenities: IAmenity[];
  languages: string[];
  officeHours: IOfficeHour[];
  address: IGeoLocation;
  expenses: IExpense[];
}

export interface IUserCredentials {
  email: string;
  password: string;
}

export interface IUser {
  id: string;
  email: string;
  name?: string;
  role: "admin" | "subadmin" | "client";
}
