export namespace Configuration {
    export interface ContactUs {
        key: 'contact-us'
        description: string
        groups: Array<{
            title: string
            content: string
        }>
    }
}