import { Filter } from './types'

export * from './validators'
export * from './types'
export * from './test-utils'

export function encryptQuery<T>(value: T): string {
  return Buffer.from(JSON.stringify(value)).toString('base64')
}

export function decodeQuery<T>(text: string): T {
  return JSON.parse(Buffer.from(text, 'base64').toString())
}
