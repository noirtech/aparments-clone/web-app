import { securityId } from '@loopback/security'

import { User } from '../models'
import {
  UserProfileFactory,
  UserIdentityService
} from '@loopback/authentication'
import { Profile } from 'passport'

export type VerifyFunction = (
  accessToken: string,
  refreshToken: string,
  profile: Profile,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  done: (error: any, user?: any, info?: any) => void
) => void

export const verifyFunctionFactory = function (
  userService: UserIdentityService<Profile, User>
): VerifyFunction {
  return function (_, __, profile, done) {
    // look up a linked user for the profile
    userService
      .findOrCreateUser(profile)
      .then((user: User) => {
        done(null, user)
      })
      .catch((err: Error) => {
        done(err)
      })
  }
}

export const ProfileFactory: UserProfileFactory<User> = (user) => {
  // since first name and lastName are optional, no error is thrown if not provided
  let name = ''
  if (user.name) name = user.name
  return {
    [securityId]: user.id,
    id: user.id,
    name,
    roles: [user.role]
  }
}
