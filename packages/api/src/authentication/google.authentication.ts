import passport, { Profile } from 'passport'
import { OAuth2Strategy as GoogleStrategy } from 'passport-google-oauth'

import { Request, ExpressRequestHandler } from '@loopback/rest'
import { bind, inject, BindingScope, Provider } from '@loopback/core'
import {
  asAuthStrategy,
  AuthenticationStrategy,
  UserIdentityService
} from '@loopback/authentication'
import { StrategyAdapter } from '@loopback/authentication-passport'

import { User } from '../models'
import { ProfileFactory, verifyFunctionFactory } from './utils'
import { UserServiceBindings } from '../keys'

@bind.provider({ scope: BindingScope.SINGLETON })
export class GoogleOAuthProvider implements Provider<GoogleStrategy> {
  strategy: GoogleStrategy

  constructor(
    @inject(UserServiceBindings.PASSPORT_USER_IDENTITY_SERVICE)
    public userService: UserIdentityService<Profile, User>
  ) {
    this.strategy = new GoogleStrategy(
      {
        clientID:
          '224034108999-257e0vn8nrj36ah8rn0ql9d83h000pm3.apps.googleusercontent.com',
        clientSecret: 'HAeIsctdREuu8swElDRm85gN',
        callbackURL:
          'https://api.findyournextapartment.com/auth/thirdparty/google/callback'
      },
      verifyFunctionFactory(this.userService)
    )
  }

  value() {
    return this.strategy
  }
}

@bind.provider({ scope: BindingScope.SINGLETON })
export class GoogleOAuth2ExpressMiddleware
  implements Provider<ExpressRequestHandler> {
  constructor(
    @inject('googleStrategy')
    public strategy: GoogleStrategy
  ) {
    passport.use(this.strategy)
  }

  value() {
    return passport.authenticate('google', {
      scope: [
        'https://www.googleapis.com/auth/userinfo.profile',
        'https://www.googleapis.com/auth/userinfo.email'
      ]
    })
  }
}

@bind(asAuthStrategy)
export class GoogleAuthenticationStrategy implements AuthenticationStrategy {
  name = 'oauth2-google'
  protected strategy: StrategyAdapter<User>

  /**
   * create an oauth2 strategy for google
   */
  constructor(
    @inject('googleStrategy')
    public passportStrategy: GoogleStrategy
  ) {
    this.strategy = new StrategyAdapter(
      passportStrategy,
      this.name,
      ProfileFactory
    )
  }

  /**
   * authenticate a request
   * @param request
   */
  async authenticate(request: Request) {
    return this.strategy.authenticate(request)
  }
}
