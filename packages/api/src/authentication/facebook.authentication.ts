import passport, { Profile } from 'passport'
import { Strategy as FacebookStrategy } from 'passport-facebook'

import { Request, ExpressRequestHandler } from '@loopback/rest'
import {
  extensionFor,
  bind,
  inject,
  BindingScope,
  Provider
} from '@loopback/core'
import {
  asAuthStrategy,
  AuthenticationStrategy,
  UserIdentityService
} from '@loopback/authentication'
import { StrategyAdapter } from '@loopback/authentication-passport'

import { User } from '../models'
import { ProfileFactory, verifyFunctionFactory } from './utils'
import { PassportAuthenticationBindings, UserServiceBindings } from '../keys'

@bind.provider({ scope: BindingScope.SINGLETON })
export class FacebookOAuthProvider implements Provider<FacebookStrategy> {
  strategy: FacebookStrategy

  constructor(
    @inject(UserServiceBindings.PASSPORT_USER_IDENTITY_SERVICE)
    public userService: UserIdentityService<Profile, User>
  ) {
    this.strategy = new FacebookStrategy(
      {
        clientID: '',
        clientSecret: '',
        callbackURL: ''
      },
      verifyFunctionFactory(this.userService)
    )
  }

  value() {
    return this.strategy
  }
}

@bind.provider({ scope: BindingScope.SINGLETON })
export class FacebookOAuth2ExpressMiddleware
  implements Provider<ExpressRequestHandler> {
  constructor(
    @inject('facebookStrategy')
    public strategy: FacebookStrategy
  ) {
    passport.use(this.strategy)
  }

  value() {
    return passport.authenticate('facebook')
  }
}

@bind(
  asAuthStrategy,
  extensionFor(PassportAuthenticationBindings.OAUTH2_STRATEGY)
)
export class FacebookAuthenticationStrategy implements AuthenticationStrategy {
  name = 'oauth2-facebook'
  protected strategy: StrategyAdapter<User>

  /**
   * create an oauth2 strategy for facebook
   */
  constructor(
    @inject('facebookStrategy')
    public passportStrategy: FacebookStrategy
  ) {
    this.strategy = new StrategyAdapter(
      passportStrategy,
      this.name,
      ProfileFactory
    )
  }

  /**
   * authenticate a request
   * @param request
   */
  async authenticate(request: Request) {
    return this.strategy.authenticate(request)
  }
}
