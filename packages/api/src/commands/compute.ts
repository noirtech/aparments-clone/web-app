import { StorageDataSource } from '../datasources'
import { PropertyRepository } from '../repositories'
import { IRentAggregate, IRentProperty } from 'apartments-shared/src/types'
import { groupBy, minBy, maxBy } from 'lodash'

const DB = new StorageDataSource()
const propertyRepo = new PropertyRepository(DB)

function calculateCover(property: Pick<IRentProperty, 'photos'>) {
  return property.photos.find(p => p.attachmentType === 3)?.src ?? ''
}
function calculateSummary(property: Pick<IRentProperty, 'floorPlans'>) {
  const summary: IRentAggregate = {
    floorPlans: {},
    minAvailability: 'available-now'
  }

  if (!property.floorPlans.length) {
    return summary
  }

  const grouped = groupBy(property.floorPlans, 'bedrooms')
  for (const [key, floorPlans] of Object.entries(grouped)) {
    const minBath = minBy(floorPlans, 'bathrooms')?.bathrooms
    const maxBath = maxBy(floorPlans, 'bathrooms')?.bathrooms

    const minPrice = minBy(floorPlans, fp => fp.price[0])?.price[0]
    const maxPrice = maxBy(floorPlans, fp => fp.price[1])?.price[1]

    const minSize = minBy(floorPlans, 'size')?.size
    const maxSize = maxBy(floorPlans, 'size')?.size

    summary.floorPlans[+key] = {
      count: floorPlans.length,
      bathRanges: [minBath ?? maxBath ?? 0, maxBath ?? minBath ?? 0],
      sizeRanges: [minSize ?? maxSize ?? 0, maxSize ?? minSize ?? 0],
      priceRanges: [minPrice ?? maxPrice ?? 0, maxPrice ?? minPrice ?? 0]
    }
  }
  return summary
}

async function main() {
  const properties = (await propertyRepo.find({
    fields: {
      id: true,
      floorPlans: true,
      photos: true
    }
  })) as Pick<IRentProperty, 'id' | 'floorPlans' | 'photos'>[]
  await Promise.all(
    properties.map(async property => {
      const summary = calculateSummary(property)
      const cover = calculateCover(property)
      await propertyRepo.updateById(property.id, {
        summary,
        cover
      })
    })
  )
}

main()
  .then(() => {
    console.log('Finish')
    process.exit(0)
  })
  .catch(e => console.error(e))
