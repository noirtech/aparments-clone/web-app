import { prompt } from 'inquirer'
import { StorageDataSource } from '../datasources'
import { UserRepository } from '../repositories'
import { IUser, validateEmail, validatePassword } from 'apartments-shared'
import { BcryptHasherService } from '../services'
const DB = new StorageDataSource()
const passwordHasher = new BcryptHasherService(10)

const userRepo = new UserRepository(DB, passwordHasher)

const requireEmail = (value: string) => {
  if (validateEmail(value)) return true
  return 'Invalid email'
}
const requirePassword = (value: string) => {
  const result = validatePassword(value)
  if (result.length === 0) return true
  return result[0]
}

async function main() {
  const user: IUser = await prompt([
    {
      type: 'input',
      message: 'User email',
      name: 'email',
      validate: requireEmail
    },
    {
      type: 'password',
      message: 'Enter password',
      name: 'password',
      mask: '*',
      validate: requirePassword
    },
    {
      type: 'list',
      name: 'role',
      message: 'Select a role',
      choices: ['admin', 'subadmin']
    }
  ])
  await userRepo.create(user)
}

main()
  .then(() => {
    console.log('Finish')
    process.exit(0)
  })
  .catch(e => console.error(e))
