import { BindingKey } from '@loopback/context'
import { UserService, UserIdentityService } from '@loopback/authentication'
import { User } from './models'
import { PasswordHasher } from './services'
import { IUserCredentials } from 'apartments-shared'
import { Profile as PassportProfile } from 'passport'

export namespace PasswordHasherBindings {
  export const PASSWORD_HASHER = BindingKey.create<PasswordHasher>(
    'services.hasher'
  )
  export const ROUNDS = BindingKey.create<number>('services.hasher.round')
}

export namespace UserServiceBindings {
  export const USER_SERVICE = BindingKey.create<
    UserService<User, IUserCredentials>
  >('services.user.service')
  export const PASSPORT_USER_IDENTITY_SERVICE = BindingKey.create<
    UserIdentityService<PassportProfile, User>
  >('services.passport.identity')
}

export namespace PassportAuthenticationBindings {
  export const OAUTH2_STRATEGY = 'passport.authentication.oauth2.strategy'
}

export namespace OAuthStrategyProviders {
  export const Facebook = BindingKey.create('')
}
