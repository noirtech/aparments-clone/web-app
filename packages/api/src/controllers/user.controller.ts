import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where
} from '@loopback/repository'
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
  HttpErrors,
  api
} from '@loopback/rest'
import { MongoError } from 'mongodb'
import { authenticate } from '@loopback/authentication'

import { User, Property } from '../models'
import { UserRepository, PropertyRepository } from '../repositories'
import { validateCredentials } from '../utils/validators'
import { MongoErrorCodes } from '../utils/mongo'
import { OPERATION_SECURITY_SPEC } from '@loopback/extension-authentication-jwt'
import { userMeAlias } from '../interceptors/authentication'
import { intercept } from '@loopback/core'

@api({ basePath: '/users', paths: {} })
@authenticate('jwt')
@intercept(userMeAlias)
export class UserController {
  constructor(
    @repository(UserRepository)
    public userRepository: UserRepository,
    @repository(PropertyRepository)
    public propertyRepository: PropertyRepository
  ) {}

  @post('/', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'User model instance',
        content: { 'application/json': { schema: getModelSchemaRef(User) } }
      }
    }
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(User, {
            title: 'NewUser',
            exclude: ['id']
          })
        }
      }
    })
    user: Omit<User, 'id'>
  ): Promise<User> {
    try {
      validateCredentials(user)
      return await this.userRepository.create(user)
    } catch (error) {
      if (error instanceof MongoError) {
        const ex: MongoError = error
        if (
          ex.code === MongoErrorCodes.DUPLICATE_KEY &&
          ex.errmsg?.includes('index: uniqueEmail')
        ) {
          throw new HttpErrors.Conflict('Email value is already taken')
        }
      }
      throw error
    }
  }

  @get('/count', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'User model count',
        content: { 'application/json': { schema: CountSchema } }
      }
    }
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(User)) where?: Where<User>
  ): Promise<Count> {
    return this.userRepository.count(where)
  }

  @get('/', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of User model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(User, { includeRelations: true })
            }
          }
        }
      }
    }
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(User))
    filter?: Filter<User>
  ): Promise<User[]> {
    return this.userRepository.find(filter)
  }

  @patch('/', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'User PATCH success count',
        content: { 'application/json': { schema: CountSchema } }
      }
    }
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(User, { partial: true })
        }
      }
    })
    user: User,
    @param.query.object('where', getWhereSchemaFor(User)) where?: Where<User>
  ): Promise<Count> {
    return this.userRepository.updateAll(user, where)
  }

  @get('/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'User model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(User, { includeRelations: true })
          }
        }
      }
    }
  })
  async findById(
    @param.path.string('id') id: string,
    @param.query.object('filter', getFilterSchemaFor(User))
    filter?: Filter<User>
  ): Promise<User> {
    return this.userRepository.findById(id, filter)
  }

  @patch('/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'User PATCH success'
      }
    }
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(User, { partial: true })
        }
      }
    })
    user: User
  ): Promise<void> {
    await this.userRepository.updateById(id, user)
  }

  @put('/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'User PUT success'
      }
    }
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() user: User
  ): Promise<void> {
    await this.userRepository.replaceById(id, user)
  }

  @del('/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'User DELETE success'
      }
    }
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.userRepository.deleteById(id)
  }

  @get('/{id}/favorites', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'User model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Property, { includeRelations: true })
          }
        }
      }
    }
  })
  async findByIdFavorites(
    @param.path.string('id') id: string,
    @param.query.object('filter', getFilterSchemaFor(Property))
    filter?: Filter<Property>
  ): Promise<Property[]> {
    const user = await this.userRepository.findById(id)
    if (!user.favorite || !user.favorite.length) return []

    const and: Where<Property>[] = [{ id: { inq: user.favorite } }]
    if (filter?.where) {
      and.push(filter.where)
    }
    return this.propertyRepository.find({
      ...filter,
      where: {
        and
      }
    })
  }
}
