import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where
} from '@loopback/repository'
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody
} from '@loopback/rest'
import { Configuration } from '../models'
import { ConfigurationRepository } from '../repositories'
import { authenticate } from '@loopback/authentication'
import { authorize } from '@loopback/authorization'

@authenticate('jwt')
@authorize({ allowedRoles: ['admin'] })
export class ConfigurationController {
  constructor(
    @repository(ConfigurationRepository)
    public configurationRepository: ConfigurationRepository
  ) {}

  @post('/configurations', {
    responses: {
      '200': {
        description: 'Configuration model instance',
        content: {
          'application/json': { schema: getModelSchemaRef(Configuration) }
        }
      }
    }
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Configuration)
        }
      }
    })
    configuration: Configuration
  ): Promise<Configuration> {
    return this.configurationRepository.create(configuration)
  }

  @get('/configurations/count', {
    responses: {
      '200': {
        description: 'Configuration model count',
        content: { 'application/json': { schema: CountSchema } }
      }
    }
  })
  @authenticate.skip()
  @authorize.skip()
  async count(
    @param.query.object('where', getWhereSchemaFor(Configuration))
    where?: Where<Configuration>
  ): Promise<Count> {
    return this.configurationRepository.count(where)
  }

  @get('/configurations', {
    responses: {
      '200': {
        description: 'Array of Configuration model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Configuration, {
                includeRelations: true
              })
            }
          }
        }
      }
    }
  })
  @authenticate.skip()
  @authorize.skip()
  async find(
    @param.query.object('filter', getFilterSchemaFor(Configuration))
    filter?: Filter<Configuration>
  ): Promise<Configuration[]> {
    return this.configurationRepository.find(filter)
  }

  @patch('/configurations', {
    responses: {
      '200': {
        description: 'Configuration PATCH success count',
        content: { 'application/json': { schema: CountSchema } }
      }
    }
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Configuration, { partial: true })
        }
      }
    })
    configuration: Configuration,
    @param.query.object('where', getWhereSchemaFor(Configuration))
    where?: Where<Configuration>
  ): Promise<Count> {
    return this.configurationRepository.updateAll(configuration, where)
  }

  @get('/configurations/{id}', {
    responses: {
      '200': {
        description: 'Configuration model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Configuration, { includeRelations: true })
          }
        }
      }
    }
  })
  @authenticate.skip()
  @authorize.skip()
  async findById(
    @param.path.string('id') id: string,
    @param.query.object('filter', getFilterSchemaFor(Configuration))
    filter?: Filter<Configuration>
  ): Promise<Configuration> {
    return this.configurationRepository.findById(id, filter)
  }

  @patch('/configurations/{id}', {
    responses: {
      '204': {
        description: 'Configuration PATCH success'
      }
    }
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Configuration, { partial: true })
        }
      }
    })
    configuration: Configuration
  ): Promise<void> {
    await this.configurationRepository.updateById(id, configuration)
  }

  @put('/configurations/{id}', {
    responses: {
      '204': {
        description: 'Configuration PUT success'
      }
    }
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() configuration: Configuration
  ): Promise<void> {
    await this.configurationRepository.replaceById(id, configuration)
  }

  @del('/configurations/{id}', {
    responses: {
      '204': {
        description: 'Configuration DELETE success'
      }
    }
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.configurationRepository.deleteById(id)
  }
}
