import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where
} from '@loopback/repository'
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody
} from '@loopback/rest'
import { Property } from '../models'
import { PropertyRepository } from '../repositories'
import { authenticate } from '@loopback/authentication'
import { authorize } from '@loopback/authorization'

@authenticate('jwt')
export class PropertyController {
  constructor(
    @repository(PropertyRepository)
    public propertyRepository: PropertyRepository
  ) {}

  @post('/properties', {
    responses: {
      '200': {
        description: 'Property model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Property) } }
      }
    }
  })
  @authorize({ scopes: ['create'] })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Property, {
            title: 'NewProperty',
            exclude: ['id']
          })
        }
      }
    })
    property: Omit<Property, 'id'>
  ): Promise<Property> {
    return this.propertyRepository.create(property)
  }

  @get('/properties/count', {
    responses: {
      '200': {
        description: 'Property model count',
        content: { 'application/json': { schema: CountSchema } }
      }
    }
  })
  @authenticate.skip()
  @authorize.skip()
  async count(
    @param.query.object('where', getWhereSchemaFor(Property))
    where?: Where<Property>
  ): Promise<Count> {
    return this.propertyRepository.count(where)
  }

  @get('/properties', {
    responses: {
      '200': {
        description: 'Array of Property model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Property, { includeRelations: true })
            }
          }
        }
      }
    }
  })
  @authenticate.skip()
  @authorize.skip()
  async find(
    @param.query.object('filter', getFilterSchemaFor(Property))
    filter?: Filter<Property>
  ): Promise<Property[]> {
    return this.propertyRepository.find(filter)
  }

  @patch('/properties', {
    responses: {
      '200': {
        description: 'Property PATCH success count',
        content: { 'application/json': { schema: CountSchema } }
      }
    }
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Property, { partial: true })
        }
      }
    })
    property: Property,
    @param.query.object('where', getWhereSchemaFor(Property))
    where?: Where<Property>
  ): Promise<Count> {
    return this.propertyRepository.updateAll(property, where)
  }

  @get('/properties/{id}', {
    responses: {
      '200': {
        description: 'Property model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Property, { includeRelations: true })
          }
        }
      }
    }
  })
  @authenticate.skip()
  @authorize.skip()
  async findById(
    @param.path.string('id') id: string,
    @param.query.object('filter', getFilterSchemaFor(Property))
    filter?: Filter<Property>
  ): Promise<Property> {
    return this.propertyRepository.findById(id, filter)
  }

  @patch('/properties/{id}', {
    responses: {
      '204': {
        description: 'Property PATCH success'
      }
    }
  })
  @authorize({ scopes: ['update'] })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Property, { partial: true })
        }
      }
    })
    property: Property
  ): Promise<void> {
    await this.propertyRepository.updateById(id, property)
  }

  @put('/properties/{id}', {
    responses: {
      '204': {
        description: 'Property PUT success'
      }
    }
  })
  @authorize({ scopes: ['update'] })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() property: Property
  ): Promise<void> {
    await this.propertyRepository.replaceById(id, property)
  }

  @del('/properties/{id}', {
    responses: {
      '204': {
        description: 'Property DELETE success'
      }
    }
  })
  @authorize({ scopes: ['delete'] })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.propertyRepository.deleteById(id)
  }
}
