import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where
} from '@loopback/repository'
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody
} from '@loopback/rest'
import { Message } from '../models'
import { MessageRepository } from '../repositories'
import { authenticate } from '@loopback/authentication'
import { authorize } from '@loopback/authorization'

@authenticate('jwt')
export class ContactFormController {
  constructor(
    @repository(MessageRepository)
    public messageRepository: MessageRepository
  ) {}

  @post('/messages', {
    responses: {
      '200': {
        description: 'Message model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Message) } }
      }
    }
  })
  @authenticate.skip()
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Message, {
            title: 'NewMessage',
            exclude: ['id']
          })
        }
      }
    })
    message: Omit<Message, 'id'>
  ): Promise<Message> {
    return this.messageRepository.create(message)
  }

  @get('/messages/count', {
    responses: {
      '200': {
        description: 'Message model count',
        content: { 'application/json': { schema: CountSchema } }
      }
    }
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Message))
    where?: Where<Message>
  ): Promise<Count> {
    return this.messageRepository.count(where)
  }

  @get('/messages', {
    responses: {
      '200': {
        description: 'Array of Message model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Message, { includeRelations: true })
            }
          }
        }
      }
    }
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Message))
    filter?: Filter<Message>
  ): Promise<Message[]> {
    return this.messageRepository.find(filter)
  }

  @patch('/messages', {
    responses: {
      '200': {
        description: 'Message PATCH success count',
        content: { 'application/json': { schema: CountSchema } }
      }
    }
  })
  @authorize({ scopes: ['update'] })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Message, { partial: true })
        }
      }
    })
    message: Message,
    @param.query.object('where', getWhereSchemaFor(Message))
    where?: Where<Message>
  ): Promise<Count> {
    return this.messageRepository.updateAll(message, where)
  }

  @get('/messages/{id}', {
    responses: {
      '200': {
        description: 'Message model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Message, { includeRelations: true })
          }
        }
      }
    }
  })
  async findById(
    @param.path.string('id') id: string,
    @param.query.object('filter', getFilterSchemaFor(Message))
    filter?: Filter<Message>
  ): Promise<Message> {
    return this.messageRepository.findById(id, filter)
  }

  @patch('/messages/{id}', {
    responses: {
      '204': {
        description: 'Message PATCH success'
      }
    }
  })
  @authorize({ scopes: ['update'] })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Message, { partial: true })
        }
      }
    })
    message: Message
  ): Promise<void> {
    await this.messageRepository.updateById(id, message)
  }

  @put('/messages/{id}', {
    responses: {
      '204': {
        description: 'Message PUT success'
      }
    }
  })
  @authorize({ scopes: ['update'] })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() message: Message
  ): Promise<void> {
    await this.messageRepository.replaceById(id, message)
  }

  @del('/messages/{id}', {
    responses: {
      '204': {
        description: 'Message DELETE success'
      }
    }
  })
  @authorize({ scopes: ['delete'] })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.messageRepository.deleteById(id)
  }
}
