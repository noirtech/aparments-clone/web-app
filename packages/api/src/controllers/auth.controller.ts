import { inject, intercept, composeInterceptors } from '@loopback/core'

import {
  get,
  post,
  api,
  requestBody,
  getModelSchemaRef,
  HttpErrors,
  RestBindings,
  Response
} from '@loopback/rest'

import { repository } from '@loopback/repository'
import {
  TokenService,
  UserService,
  authenticate,
  AuthenticationBindings
} from '@loopback/authentication'
import { SecurityBindings, UserProfile } from '@loopback/security'

import { MongoError } from 'mongodb'
import { IUserCredentials } from 'apartments-shared'

import { User } from '../models'
import { MongoErrorCodes } from '../utils/mongo'
import { UserRepository } from '../repositories'
import { UserServiceBindings } from '../keys'
import { validateCredentials } from '../utils/validators'
import { TokenServiceBindings } from '@loopback/extension-authentication-jwt'

const CredentialsSchema = {
  type: 'object',
  required: ['email', 'password'],
  properties: {
    email: {
      type: 'string',
      format: 'email'
    },
    password: {
      type: 'string',
      minLength: 8,
      maxLength: 20
    }
  }
}
const CredentialsRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': { schema: CredentialsSchema }
  }
}

@api({ basePath: '/auth', paths: {} })
export class AuthController {
  constructor(
    @repository(UserRepository)
    public userRepository: UserRepository,
    @inject(TokenServiceBindings.TOKEN_SERVICE)
    public jwtService: TokenService,
    @inject(UserServiceBindings.USER_SERVICE)
    public userService: UserService<User, IUserCredentials>
  ) {}

  // Login ThirdParty
  @authenticate('oauth2-google')
  @get('/thirdparty/google/callback')
  async loginWithGoogle(
    @inject(SecurityBindings.USER) userProfile: UserProfile,
    @inject(RestBindings.Http.RESPONSE)
    response: Response
  ) {
    const token = await this.jwtService.generateToken(userProfile)
    response.type('html')
    response.format({
      'text/html': function () {
        response.send(`
        <!DOCTYPE html>
        <html lang="en">
        <head>
          <meta charset="UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <title>Find Your Next Apartment</title>
        </head>
        <body>
          <script>
            window.opener.postMessage('${token}')
            window.close()
          </script>
        </body>
        </html>
        `)
      },
      'application/json': function () {
        response.send({ token })
      }
    })
    response.end()
    return response
  }

  @authenticate('oauth2-facebook')
  @get('/thirdparty/facebook/callback')
  async loginWithFacebook(
    @inject(SecurityBindings.USER) userProfile: UserProfile,
    @inject(RestBindings.Http.RESPONSE)
    response: Response
  ) {
    const token = await this.jwtService.generateToken(userProfile)
    response.type('html')
    response.format({
      'text/html': function () {
        response.send(`
        <!DOCTYPE html>
        <html lang="en">
        <head>
          <meta charset="UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <title>Find Your Next Apartment</title>
        </head>
        <body>
          <script>
            window.opener.postMessage('${token}')
            window.close()
          </script>
        </body>
        </html>
        `)
      },
      'application/json': function () {
        response.send({ token })
      }
    })
    response.end()
    return response
  }

  // Callback
  @intercept(
    composeInterceptors(
      'passport-init-mw',
      // 'passport-facebook',
      'passport-google'
    )
  )
  @get('/thirdparty/{provider}')
  thirdPartyCallback(
    @inject(AuthenticationBindings.AUTHENTICATION_REDIRECT_URL)
    redirectUrl: string,
    @inject(AuthenticationBindings.AUTHENTICATION_REDIRECT_STATUS)
    status: number,
    @inject(RestBindings.Http.RESPONSE)
    response: Response
  ) {
    response.statusCode = status || 302
    response.setHeader('Location', redirectUrl)
    response.end()
    return response
  }

  // JWT Authentication
  @post('/login', {
    responses: {
      '200': {
        description: 'Token',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                token: {
                  type: 'string'
                }
              }
            }
          }
        }
      }
    }
  })
  async login(
    @requestBody(CredentialsRequestBody) credentials: IUserCredentials
  ): Promise<{ token: string }> {
    const user = await this.userService.verifyCredentials(credentials)
    const userProfile = this.userService.convertToUserProfile(user)
    const token = await this.jwtService.generateToken(userProfile)

    return { token }
  }

  @post('/register', {
    responses: {
      '200': {
        description: 'User model instance',
        content: { 'application/json': { schema: getModelSchemaRef(User) } }
      }
    }
  })
  async signUp(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(User, {
            title: 'NewUser',
            exclude: ['id']
          })
        }
      }
    })
    user: Omit<User, 'id'>
  ): Promise<User> {
    try {
      validateCredentials(user)
      return await this.userRepository.create(user)
    } catch (error) {
      if (error instanceof MongoError) {
        const ex: MongoError = error
        if (
          ex.code === MongoErrorCodes.DUPLICATE_KEY &&
          ex.errmsg?.includes('index: uniqueEmail')
        ) {
          throw new HttpErrors.Conflict('Email value is already taken')
        }
      }
      throw error
    }
  }
}
