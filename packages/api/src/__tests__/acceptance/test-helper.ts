import { ApartmentsApplication } from '../..'
import {
  createRestAppClient,
  givenHttpServerConfig,
  Client
} from '@loopback/testlab'
import { testDB } from '../helpers/repository.helpers'
import { IUserCredentials } from 'apartments-shared'

export async function setupApplication(): Promise<AppWithClient> {
  const restConfig = givenHttpServerConfig({})

  const app = new ApartmentsApplication({
    rest: restConfig
  })
  app.dataSource(testDB)
  app.basePath('')

  await app.boot()
  await app.start()

  const client = createRestAppClient(app)

  return { app, client }
}
const tokens: Record<string, string> = {}
export async function authenticate(
  client: Client,
  credentials: IUserCredentials
): Promise<[string, string]> {
  if (tokens[credentials.email]) {
    return ['Authorization', 'Bearer ' + tokens[credentials.email]]
  }
  const {
    body: { token }
  } = await client.post('/auth/login').send(credentials)

  return ['Authorization', 'Bearer ' + token]
}

export interface AppWithClient {
  app: ApartmentsApplication
  client: Client
}
