import {
  givenEmptyDatabase,
  givenUser,
  givenProperty,
  givenRepositories
} from '../helpers/repository.helpers'
import { ApartmentsApplication } from '../..'
import { setupApplication, authenticate } from './test-helper'
import { Client, expect } from '@loopback/testlab'
import { givenUserCredentialsData } from 'apartments-shared'

describe('Authentication endpoint (acceptance)', () => {
  beforeEach(givenEmptyDatabase)
  let app: ApartmentsApplication
  let client: Client

  before('setupApplication', async () => {
    ;({ app, client } = await setupApplication())
  })

  after(async () => {
    await app.stop()
  })

  it('Delete property for admin', async () => {
    const { propertyRepository } = await givenRepositories()
    const credentials = givenUserCredentialsData()
    await givenUser({ ...credentials, role: 'admin' })
    const { id } = await givenProperty()
    const headers = await authenticate(client, credentials)
    const result = await client.del(`/properties/${id}`).set(...headers)
    expect(result.status).to.be.equal(204)
    const { count } = await propertyRepository.count()
    expect(count).to.be.equal(0)
  })
  it('Delete property for subadmin', async () => {
    const { propertyRepository } = await givenRepositories()
    const credentials = givenUserCredentialsData()
    await givenUser({ ...credentials, role: 'subadmin' })
    const { id } = await givenProperty()
    const headers = await authenticate(client, credentials)
    const result = await client.del(`/properties/${id}`).set(...headers)

    expect(result.status).to.be.equal(401)
    const { count } = await propertyRepository.count()
    expect(count).to.be.equal(1)
  })
})
