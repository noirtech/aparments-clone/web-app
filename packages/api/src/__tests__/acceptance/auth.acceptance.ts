import { givenEmptyDatabase, givenUser } from '../helpers/repository.helpers'
import { ApartmentsApplication } from '../..'
import { setupApplication } from './test-helper'
import { Client, expect } from '@loopback/testlab'
import { givenUserCredentialsData } from 'apartments-shared'

describe('Authentication endpoint (acceptance)', () => {
  beforeEach(givenEmptyDatabase)
  let app: ApartmentsApplication
  let client: Client

  before('setupApplication', async () => {
    ;({ app, client } = await setupApplication())
  })

  after(async () => {
    await app.stop()
  })

  it('Login a user', async () => {
    const credentials = givenUserCredentialsData()
    await givenUser(credentials)
    const result = await client.post('/auth/login').send(credentials)
    expect(result.status).to.be.equal(200)
    expect(result.body).to.have.property('token')
  })
})
