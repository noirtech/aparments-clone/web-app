import { UserRepository, PropertyRepository } from '../../repositories'
import { StorageDataSource } from '../../datasources'
import {
  givenUserData,
  IUser,
  givenUserCredentialsData,
  givenPropertiesData
} from 'apartments-shared'
import { IUserCredentials, IRentProperty } from 'apartments-shared/src/types'
import { BcryptHasherService } from '../../services'

export const testDB = new StorageDataSource()

export async function givenRepositories() {
  const passwordHasher = new BcryptHasherService(10)

  const userRepository = new UserRepository(testDB, passwordHasher)
  const propertyRepository = new PropertyRepository(testDB)
  return {
    userRepository,
    propertyRepository
  }
}
export async function givenEmptyDatabase() {
  const { userRepository, propertyRepository } = await givenRepositories()

  await userRepository.deleteAll()
  await propertyRepository.deleteAll()
}

export async function givenUser(data?: Partial<IUser & IUserCredentials>) {
  const { userRepository } = await givenRepositories()

  return userRepository.create({
    ...givenUserData(),
    ...givenUserCredentialsData(),
    ...data
  })
}

export async function givenProperty(data?: Partial<IRentProperty>) {
  const { propertyRepository } = await givenRepositories()
  return propertyRepository.create({
    ...givenPropertiesData()[0],
    ...data
  })
}
