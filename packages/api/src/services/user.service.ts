import { HttpErrors } from '@loopback/rest'
import { repository } from '@loopback/repository'
import { UserProfile, securityId } from '@loopback/security'
import { UserService } from '@loopback/authentication'
import { bind, BindingScope, inject } from '@loopback/core'

import { UserRepository } from '../repositories'
import { User } from '../models'
import { IUserCredentials } from 'apartments-shared'
import { PasswordHasherBindings } from '../keys'
import { PasswordHasher } from './password-hasher.service'

@bind({ scope: BindingScope.SINGLETON })
export class CustomUserService implements UserService<User, IUserCredentials> {
  constructor(
    @repository(UserRepository) public userRepository: UserRepository,
    @inject(PasswordHasherBindings.PASSWORD_HASHER)
    public passwordHasher: PasswordHasher
  ) {}

  async verifyCredentials(credentials: IUserCredentials): Promise<User> {
    const foundUser = await this.userRepository.findOne({
      where: { email: credentials.email }
    })

    if (!foundUser) {
      throw new HttpErrors.Unauthorized(
        `User with email ${credentials.email} not found.`
      )
    }

    const passwordMatched = await this.passwordHasher.comparePassword(
      credentials.password,
      foundUser.password ?? ''
    )

    if (!passwordMatched) {
      throw new HttpErrors.Unauthorized('The credentials are not correct.')
    }

    return foundUser
  }

  convertToUserProfile(user: User): UserProfile {
    // since first name and lastName are optional, no error is thrown if not provided
    let name = ''
    if (user.name) name = user.name

    return {
      [securityId]: user.id,
      id: user.id,
      name,
      roles: [user.role]
    }
  }
}
