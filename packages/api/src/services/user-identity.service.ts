// Copyright IBM Corp. 2020. All Rights Reserved.
// Node module: @loopback/example-passport-login
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import { repository } from '@loopback/repository'
import { Profile as PassportProfile } from 'passport'
import { UserIdentityService } from '@loopback/authentication'
import { UserRepository } from '../repositories'
import { User, UserIdentity } from '../models'

/**
 * User service to accept a 'passport' user profile and save it locally
 */
export class PassportUserIdentityService
  implements UserIdentityService<PassportProfile, User> {
  constructor(
    @repository(UserRepository)
    public userRepository: UserRepository
  ) {}

  /**
   * find a linked local user for an external profile
   * create a local user if not created yet.
   * @param email
   * @param profile
   * @param token
   */
  async findOrCreateUser(profile: PassportProfile): Promise<User> {
    if (!profile.emails || !profile.emails.length) {
      throw new Error('email-id is required in returned profile to login')
    }

    const emails = profile.emails.map((e) => e.value)

    let user = await this.userRepository.findOne({
      where: {
        email: {
          inq: emails
        }
      }
    })

    if (!user) {
      const name =
        profile.name && profile.name.givenName
          ? profile.name.givenName + ' ' + profile.name.familyName
          : profile.displayName

      user = await this.userRepository.create({
        email: emails[0],
        name: name || JSON.stringify(profile.name),
        profiles: [profile]
      })
    }
    return user
  }

  /**
   * link external profile with local user
   * @param userId
   * @param userIdentity
   */
  async linkExternalProfile(
    userId: string,
    userIdentity: PassportProfile
  ): Promise<User> {
    let profile: PassportProfile | null = null
    const user = await this.userRepository.findById(userId)

    if (user.profiles) {
      profile =
        user.profiles.find((identity) => identity.id === userIdentity.id) ??
        null
    } else {
      user.profiles = []
    }

    if (!profile) {
      user.profiles.push(
        new UserIdentity({
          ...userIdentity,
          created: new Date()
        })
      )
    }

    if (!userId) console.log('user id is empty')
    await this.userRepository.update(user)
    return user
  }
}
