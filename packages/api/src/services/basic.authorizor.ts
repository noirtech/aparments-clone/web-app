import {
  AuthorizationContext,
  AuthorizationMetadata,
  AuthorizationDecision,
  Authorizer
} from '@loopback/authorization'
import { SecurityBindings } from '@loopback/security'
import { Provider } from '@loopback/core'
import { UserRepository } from '../repositories'
export class AuthorizationProvider implements Provider<Authorizer> {
  value(): Authorizer {
    return this.authorize.bind(this)
  }
  async authorize(
    ctx: AuthorizationContext,
    metadata: AuthorizationMetadata
  ): Promise<AuthorizationDecision> {
    if (ctx.principals.length === 0) return AuthorizationDecision.ALLOW
    const userRepo: UserRepository = await ctx.invocationContext.get(
      'repositories.UserRepository'
    )

    const user = await userRepo.findById(ctx.principals[0].id)
    const isAdmin = user.role === 'admin'
    if (!metadata.allowedRoles?.includes('subadmin') && !isAdmin)
      return AuthorizationDecision.DENY
    if (metadata.scopes?.includes('create') || isAdmin)
      return AuthorizationDecision.ALLOW
    return AuthorizationDecision.DENY
  }
}
