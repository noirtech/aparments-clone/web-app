import {
  Provider,
  Interceptor,
  ValueOrPromise,
  inject,
  InvocationContext,
  Next
} from '@loopback/core'
import { ExpressRequestHandler, toInterceptor } from '@loopback/express'
import { RestBindings, RequestContext } from '@loopback/rest'
import { SecurityBindings, UserProfile, securityId } from '@loopback/security'
export const userMeAlias: Interceptor = async (invocationCtx, next) => {
  const user = await invocationCtx.get<UserProfile>(SecurityBindings.USER)
  const id = invocationCtx.args[0]
  if (id === 'me') {
    invocationCtx.args[0] = user[securityId]
  }
  // console.log(user)
  return next()
}

export class FacebookOAuthInterceptor implements Provider<Interceptor> {
  constructor(
    @inject('facebookExpressMiddleware')
    public facebookStrategy: ExpressRequestHandler
  ) {}

  value(): ValueOrPromise<Interceptor> {
    return async (invocationCtx: InvocationContext, next: Next) => {
      const requestCtx = invocationCtx.getSync<RequestContext>(
        RestBindings.Http.CONTEXT
      )
      const request = requestCtx.request
      if (request.query['oauth2-provider-name'] === 'facebook') {
        return toInterceptor(this.facebookStrategy)(invocationCtx, next)
      }
      return next()
    }
  }
}

export class GoogleOAuthInterceptor implements Provider<Interceptor> {
  constructor(
    @inject('googleExpressMiddleware')
    public googleStrategy: ExpressRequestHandler
  ) {}

  value(): ValueOrPromise<Interceptor> {
    return async (invocationCtx: InvocationContext, next: Next) => {
      const requestCtx = invocationCtx.getSync<RequestContext>(
        RestBindings.Http.CONTEXT
      )
      const request = requestCtx.request
      if (request.query['oauth2-provider-name'] === 'google') {
        return toInterceptor(this.googleStrategy)(invocationCtx, next)
      }
      return next()
    }
  }
}
