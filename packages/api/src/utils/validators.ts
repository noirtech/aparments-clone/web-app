import {
  validateEmail,
  validatePassword,
  IUserCredentials
} from 'apartments-shared'
import { HttpErrors } from '@loopback/rest'

export function validateCredentials(credentials: Partial<IUserCredentials>) {
  if (!validateEmail(credentials.email ?? '')) {
    throw new HttpErrors.UnprocessableEntity('invalid email')
  }
  const result = validatePassword(credentials.password ?? '')
  if (result.length > 0) {
    throw new HttpErrors.UnprocessableEntity(result[0])
  }
}
