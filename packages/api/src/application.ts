import fs from 'fs'
import path from 'path'
import passport from 'passport'

import { BootMixin } from '@loopback/boot'
import {
  ApplicationConfig,
  BindingScope,
  createBindingFromClass
} from '@loopback/core'
import {
  RestExplorerBindings,
  RestExplorerComponent
} from '@loopback/rest-explorer'
import { RepositoryMixin, SchemaMigrationOptions } from '@loopback/repository'
import { RestApplication, toInterceptor } from '@loopback/rest'
import { ServiceMixin } from '@loopback/service-proxy'
import { HealthComponent } from '@loopback/extension-health'
import { AuthenticationComponent } from '@loopback/authentication'
import {
  JWTAuthenticationComponent,
  SECURITY_SCHEME_SPEC,
  TokenServiceBindings,
  TokenServiceConstants
} from '@loopback/extension-authentication-jwt'
import {
  AuthorizationComponent,
  AuthorizationTags
} from '@loopback/authorization'

import { ApplicationSequence } from './sequence'
import { PasswordHasherBindings, UserServiceBindings } from './keys'
import {
  BcryptHasherService,
  CustomUserService,
  PassportUserIdentityService,
  JWTService
} from './services'
import { ConfigurationRepository } from './repositories'
import { AuthorizationProvider } from './services/basic.authorizor'
import {
  FacebookAuthenticationStrategy,
  GoogleAuthenticationStrategy,
  GoogleOAuthProvider,
  GoogleOAuth2ExpressMiddleware,
  FacebookOAuthProvider,
  FacebookOAuth2ExpressMiddleware
} from './authentication/'
import {
  FacebookOAuthInterceptor,
  GoogleOAuthInterceptor
} from './interceptors/authentication'

export class ApartmentsApplication extends BootMixin(
  ServiceMixin(RepositoryMixin(RestApplication))
) {
  constructor(options: ApplicationConfig = {}) {
    super(options)

    // Set up the custom sequence
    this.sequence(ApplicationSequence)

    // Customize @loopback/rest-explorer configuration here
    this.bind(RestExplorerBindings.CONFIG).to({
      path: '/documentation'
    })

    passport // eslint-disable-next-line @typescript-eslint/no-explicit-any
      .serializeUser(function (user: any, done) {
        done(null, user)
      })
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    passport.deserializeUser(function (user: any, done) {
      done(null, user)
    })

    this.addSecuritySpec()
    this.component(RestExplorerComponent)
    this.component(AuthorizationComponent)
    this.component(HealthComponent)
    this.component(AuthenticationComponent)
    this.component(JWTAuthenticationComponent)

    this.setupBindings()
    this.projectRoot = __dirname
    // Customize @loopback/boot Booter Conventions here
    this.bootOptions = {
      controllers: {
        // Customize ControllerBooter Conventions here
        dirs: ['controllers'],
        extensions: ['.controller.js'],
        nested: true
      }
    }
  }

  setupBindings() {
    // User Services
    this.bind(UserServiceBindings.USER_SERVICE)
      .toClass(CustomUserService)
      .inScope(BindingScope.SINGLETON)
    this.bind(UserServiceBindings.PASSPORT_USER_IDENTITY_SERVICE)
      .toClass(PassportUserIdentityService)
      .inScope(BindingScope.SINGLETON)

    // Authentication strategy providers
    this.add(
      createBindingFromClass(GoogleOAuthProvider, { key: 'googleStrategy' })
    )
    // this.add(
    //   createBindingFromClass(FacebookOAuthProvider, { key: 'facebookStrategy' })
    // )

    // Authentication middleware
    this.add(
      createBindingFromClass(GoogleOAuth2ExpressMiddleware, {
        key: 'googleExpressMiddleware'
      })
    )
    // this.add(
    //   createBindingFromClass(FacebookOAuth2ExpressMiddleware, {
    //     key: 'facebookExpressMiddleware'
    //   })
    // )

    // Authentication Strategies
    this.add(createBindingFromClass(GoogleAuthenticationStrategy))
    // this.add(createBindingFromClass(FacebookAuthenticationStrategy))

    // Express middleware interceptors
    this.bind('passport-init-mw').to(toInterceptor(passport.initialize()))
    // this.bind('passport-facebook').toProvider(FacebookOAuthInterceptor)
    this.bind('passport-google').toProvider(GoogleOAuthInterceptor)

    // Token bindings
    this.bind(TokenServiceBindings.TOKEN_SECRET).to(
      TokenServiceConstants.TOKEN_SECRET_VALUE
    )
    this.bind(TokenServiceBindings.TOKEN_SERVICE).toClass(JWTService)

    this.bind(PasswordHasherBindings.ROUNDS).to(10)

    this.bind(PasswordHasherBindings.PASSWORD_HASHER)
      .toClass(BcryptHasherService)
      .inScope(BindingScope.SINGLETON)

    this.bind('authorizationProviders.general')
      .toProvider(AuthorizationProvider)
      .tag(AuthorizationTags.AUTHORIZER)
  }

  async migrateSchema(options?: SchemaMigrationOptions) {
    await super.migrateSchema(options)
    const configRepo = await this.getRepository(ConfigurationRepository)
    await configRepo.deleteAll()
    const configsDir = path.join(__dirname, '../fixtures/configuration')
    const configFiles = fs.readdirSync(configsDir)
    for (const file of configFiles) {
      if (file.endsWith('.json')) {
        const configFile = path.join(configsDir, file)
        const config = JSON.parse(fs.readFileSync(configFile, 'utf8'))
        await configRepo.create(config)
      }
    }
  }

  addSecuritySpec(): void {
    this.api({
      openapi: '3.0.0',
      info: {
        title: 'test application',
        version: '1.0.0'
      },
      paths: {},
      components: { securitySchemes: SECURITY_SCHEME_SPEC },
      security: [
        {
          // secure all endpoints with 'jwt'
          jwt: []
        }
      ],
      servers: [{ url: '/' }]
    })
  }
}
