import { DefaultCrudRepository } from '@loopback/repository'
import { Property } from '../models'
import { StorageDataSource } from '../datasources'
import { inject } from '@loopback/core'

export class PropertyRepository extends DefaultCrudRepository<
  Property,
  typeof Property.prototype.id
> {
  constructor(@inject('datasources.storage') dataSource: StorageDataSource) {
    super(Property, dataSource)
  }
}
