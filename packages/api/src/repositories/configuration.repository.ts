import { DefaultCrudRepository } from '@loopback/repository'
import { Configuration, ConfigurationRelations } from '../models'
import { StorageDataSource } from '../datasources'
import { inject } from '@loopback/core'

export class ConfigurationRepository extends DefaultCrudRepository<
  Configuration,
  typeof Configuration.prototype.key,
  ConfigurationRelations
> {
  constructor(@inject('datasources.storage') dataSource: StorageDataSource) {
    super(Configuration, dataSource)
  }
}
