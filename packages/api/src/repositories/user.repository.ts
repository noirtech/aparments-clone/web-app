import {
  DefaultCrudRepository,
  Options,
  DataObject
} from '@loopback/repository'
import { User, UserRelations } from '../models'
import { StorageDataSource } from '../datasources'
import { inject } from '@loopback/core'
import { PasswordHasherBindings } from '../keys'
import { PasswordHasher } from '../services'

export class UserRepository extends DefaultCrudRepository<
  User,
  typeof User.prototype.id,
  UserRelations
> {
  constructor(
    @inject('datasources.storage') dataSource: StorageDataSource,
    @inject(PasswordHasherBindings.PASSWORD_HASHER)
    public passwordHasher: PasswordHasher
  ) {
    super(User, dataSource)
  }
  async create(entity: DataObject<User>, options?: Options): Promise<User> {
    let password
    if (entity.password)
      password = await this.passwordHasher.hashPassword(
        entity.password as string
      )
    return super.create({ ...entity, password }, options)
  }
}
