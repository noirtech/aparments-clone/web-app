import { Entity, model, property, Model } from '@loopback/repository'
import {
  IFloorPlan,
  IPhoto,
  IAmenity,
  IExpense,
  IOfficeHour,
  IGeoLocation,
  IRentProperty,
  ILocation,
  INearby,
  IRentAggregate,
  ISchool,
  IFloorPlanAggregate,
  IAddressComponent
} from 'apartments-shared/src/types'
import { ComponentTypes } from 'apartments-shared'

@model()
export class FloorPlan extends Model implements IFloorPlan {
  @property({
    type: 'string',
    required: true
  })
  name: string

  @property()
  originAttId: number

  @property({
    type: 'string',
    default: ''
  })
  unit: string

  @property({
    type: 'number',
    default: 0
  })
  deposit: number

  @property({
    type: 'array',
    itemType: () => Photo,
    default: []
  })
  photos: IPhoto[]

  @property({
    type: 'number',
    required: true
  })
  bedrooms: number

  @property({
    type: 'number',
    required: true
  })
  bathrooms: number

  @property({
    type: 'array',
    itemType: 'number',
    required: true
  })
  price: [number, number]

  @property({
    type: 'number',
    default: ''
  })
  size: number

  @property({
    type: 'string',
    required: true
  })
  availability: string

  @property({
    type: 'string',
    required: true
  })
  originId: string
  @property({
    type: 'string',
    required: true
  })
  originType: string

  constructor(data?: Partial<FloorPlan>) {
    super(data)
  }
  originAttId: number
}

@model()
export class Photo extends Model implements IPhoto {
  @property({
    type: 'string',
    default: ''
  })
  thumbnail: string

  @property({
    type: 'number',
    default: 1
  })
  mediaType: number

  @property({
    type: 'number',
    default: 1
  })
  attachmentType: number

  @property({
    type: 'string',
    default: ''
  })
  altText: string

  @property({
    type: 'string',
    required: true
  })
  src: string

  @property({
    type: 'string',
    default: ''
  })
  description: string

  constructor(data?: Partial<Photo>) {
    super(data)
  }
}
@model()
export class Amenity extends Model implements IAmenity {
  @property({
    type: 'string',
    required: true
  })
  name: string

  @property({
    type: 'string',
    required: true
  })
  category: string

  @property({
    type: 'string',
    required: true
  })
  title: string

  constructor(data?: Partial<Amenity>) {
    super(data)
  }
}

@model()
export class OfficeHour extends Model implements IOfficeHour {
  @property({
    type: 'array',
    itemType: 'string',
    jsonSchema: {
      enum: [
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday',
        'Sunday'
      ]
    },
    required: true
  })
  dayOfWeek: Array<
    | 'Monday'
    | 'Tuesday'
    | 'Wednesday'
    | 'Thursday'
    | 'Friday'
    | 'Saturday'
    | 'Sunday'
  >

  @property({
    type: 'string',
    required: true
  })
  opens: string

  @property({
    type: 'string',
    required: true
  })
  closes: string

  constructor(data?: Partial<OfficeHour>) {
    super(data)
  }
}

@model()
export class Location extends Model implements ILocation {
  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      enum: ['Point']
    }
  })
  type: 'Point'

  @property({
    type: 'array',
    itemType: 'number',
    required: true
  })
  coordinates: [number, number]

  constructor(data?: Partial<Location>) {
    super(data)
  }
}
@model()
export class AddressComponent extends Model implements IAddressComponent {
  @property({
    type: 'string',
    required: true
  })
  name: string
  @property({
    type: 'array',
    itemType: 'string',
    required: true,
    jsonSchema: {
      enum: [
        'street_number',
        'route',
        'locality',
        'political',
        'administrative_area_level_2',
        'administrative_area_level_1',
        'country',
        'postal_code'
      ]
    }
  })
  types: ComponentTypes[]

  constructor(data?: Partial<Location>) {
    super(data)
  }
}

@model()
export class GeoLocation extends Model implements IGeoLocation {
  @property({
    type: () => Location,
    required: true
  })
  location: ILocation

  @property({
    type: 'string',
    default: ''
  })
  formatted: string

  @property({
    type: 'array',
    itemType: () => AddressComponent,
    default: []
  })
  components: IAddressComponent[]

  constructor(data?: Partial<GeoLocation>) {
    super(data)
  }
}

@model()
export class Nearby extends Model implements INearby {
  @property({
    type: 'string',
    required: true
  })
  name: string

  @property({
    type: 'string',
    required: true
  })
  category: string

  @property({
    type: 'string',
    required: true
  })
  driving: string

  @property({
    type: 'string',
    required: true
  })
  distance: string
  constructor(data?: Partial<Nearby>) {
    super(data)
  }
}

@model()
export class FloorPlanAggregate extends Model implements IFloorPlanAggregate {
  constructor(data?: Partial<FloorPlanAggregate>) {
    super(data)
  }
  @property({
    type: 'array',
    default: []
  })
  bathRanges: [number, number]
  @property({
    type: 'array',
    itemType: 'number',
    default: []
  })
  sizeRanges: [number, number]
  @property({
    type: 'array',
    default: []
  })
  priceRanges: [number, number]
  @property({
    type: 'number',
    default: 0
  })
  count: number
}

@model()
export class PropertyAggregated extends Model implements IRentAggregate {
  @property({
    type: 'object',
    default: {}
  })
  floorPlans: {
    [nBeds: number]: IFloorPlanAggregate
  }

  @property({
    type: 'string',
    default: 'available-soon'
  })
  minAvailability: string

  constructor(data?: Partial<PropertyAggregated>) {
    super(data)
  }
}
@model()
export class Expense extends Model implements IExpense {
  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      enum: ['recurring', 'one-time', 'utilities-included']
    }
  })
  type: 'recurring' | 'one-time' | 'utilities-included'

  @property({
    type: 'string',
    required: true
  })
  name: string

  @property({
    type: 'string',
    required: true
  })
  price: number

  constructor(data?: Partial<Expense>) {
    super(data)
  }
}

@model()
export class School extends Model implements ISchool {
  @property({
    type: 'string',
    required: true
  })
  name: string
  @property({
    type: 'string',
    required: true
  })
  grades: string

  @property({
    type: 'string',
    required: true
  })
  type: string

  @property({
    type: 'number',
    default: 1,
    jsonSchema: {
      enum: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    }
  })
  rating: number

  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      enum: ['public', 'private']
    }
  })
  category: 'public' | 'private'

  @property({
    type: 'string',
    required: true
  })
  students: number

  @property({
    type: 'string',
    required: true
  })
  phone: string

  constructor(data?: Partial<GeoLocation>) {
    super(data)
  }
}

@model()
export class Property extends Entity implements IRentProperty {
  @property({
    type: 'number',
    default: 0
  })
  priority: number

  @property({
    type: 'boolean',
    default: false
  })
  scam: boolean

  @property({
    type: 'boolean',
    default: false
  })
  verified: boolean

  @property({
    type: 'string',
    id: true,
    generated: true
  })
  id: string

  @property({
    type: 'array',
    itemType: () => Nearby,
    default: []
  })
  nearby: INearby[]

  @property({
    type: 'array',
    itemType: () => School,
    default: []
  })
  schools: ISchool[]

  @property({
    type: 'string',
    required: true
  })
  name: string
  @property({
    type: 'string'
  })
  origin: string

  @property({
    type: 'string',
    default: ''
  })
  description: string

  @property({
    type: 'array',
    itemType: () => FloorPlan,
    default: []
  })
  floorPlans: IFloorPlan[]

  @property({
    type: 'array',
    itemType: () => Photo,
    default: []
  })
  photos: IPhoto[]

  @property({
    type: 'array',
    itemType: () => Amenity,
    default: []
  })
  amenities: IAmenity[]

  @property({
    type: 'array',
    itemType: 'string',
    default: []
  })
  languages: string[]

  @property({
    type: 'array',
    itemType: () => OfficeHour,
    default: []
  })
  officeHours: IOfficeHour[]

  @property({
    type: () => GeoLocation,
    required: true
  })
  address: IGeoLocation

  @property({
    type: 'array',
    itemType: () => Expense,
    default: []
  })
  expenses: IExpense[]

  @property({
    type: 'date',
    default: () => new Date()
  })
  createdAt: Date

  @property({
    type: () => PropertyAggregated
  })
  summary: IRentAggregate

  @property({
    type: 'string'
  })
  cover: string

  constructor(data?: Partial<Property>) {
    super(data)
  }
  @property({
    type: 'string',
    required: true
  })
  originId: string
}
