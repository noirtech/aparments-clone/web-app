import { Profile } from 'passport'

import { Entity, model, property, Model } from '@loopback/repository'
import { IUserCredentials, IUser } from 'apartments-shared'

@model()
export class UserIdentity extends Model implements Profile {
  @property({
    type: 'string',
    id: true
  })
  id: string

  @property({
    type: 'string',
    required: true
  })
  displayName: string

  @property({
    type: 'string'
  })
  username?: string | undefined

  @property({
    type: 'object'
  })
  name?: {
    familyName: string
    givenName: string
    middleName?: string | undefined
  }

  @property({
    type: 'string',
    required: true
  })
  provider: string

  @property({
    type: 'object'
  })
  emails?: { value: string; type?: string | undefined }[]

  @property({
    type: 'object'
  })
  photos?: { value: string }[]

  @property({
    type: 'object'
  })
  credentials?: object

  @property({
    type: 'date',
    required: true
  })
  created: Date

  constructor(data?: Partial<UserIdentity>) {
    super(data)
  }
}

@model({
  settings: {
    hiddenProperties: ['password']
  }
})
export class User extends Entity implements IUser, Partial<IUserCredentials> {
  @property({
    type: 'string',
    id: true,
    generated: true
  })
  id: string

  // Credentials
  @property({
    type: 'string',
    required: true,
    index: {
      unique: true
    }
  })
  email: string

  @property({
    type: 'string'
  })
  password?: string

  // Profile
  @property({
    type: 'string'
  })
  name?: string

  @property.array(UserIdentity)
  profiles?: UserIdentity[]

  @property.array(String)
  favorite?: string[]

  @property({
    type: 'string',
    jsonSchema: {
      enum: ['admin', 'subadmin', 'client']
    },
    default: 'client'
  })
  role: 'admin' | 'subadmin' | 'client'

  constructor(data?: Partial<User>) {
    super(data)
  }
}

export interface UserRelations {
  // describe navigational properties here
}

export type UserWithRelations = User & UserRelations
