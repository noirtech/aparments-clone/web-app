import { Entity, model, property } from '@loopback/repository'

@model({ settings: { strict: false } })
export class Configuration extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    required: true
  })
  key: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any

  constructor(data?: Partial<Configuration>) {
    super(data)
  }
}

export interface ConfigurationRelations {
  // describe navigational properties here
}

export type ConfigurationWithRelations = Configuration & ConfigurationRelations
