import { inject } from '@loopback/context'
import {
  FindRoute,
  InvokeMethod,
  ParseParams,
  Reject,
  RequestContext,
  RestBindings,
  Send,
  SequenceHandler,
  InvokeMiddleware
} from '@loopback/rest'
import {
  AuthenticationBindings,
  AuthenticateFn,
  AUTHENTICATION_STRATEGY_NOT_FOUND,
  USER_PROFILE_NOT_FOUND
} from '@loopback/authentication'

const SequenceActions = RestBindings.SequenceActions

export class ApplicationSequence implements SequenceHandler {
  @inject(SequenceActions.INVOKE_MIDDLEWARE, { optional: true })
  protected invokeMiddleware: InvokeMiddleware = () => false

  constructor(
    @inject(SequenceActions.FIND_ROUTE) protected findRoute: FindRoute,
    @inject(SequenceActions.PARSE_PARAMS) protected parseParams: ParseParams,
    @inject(SequenceActions.INVOKE_METHOD) protected invoke: InvokeMethod,
    @inject(SequenceActions.SEND) public send: Send,
    @inject(AuthenticationBindings.AUTH_ACTION)
    protected authenticateRequest: AuthenticateFn,
    @inject(SequenceActions.REJECT) public reject: Reject
  ) {}

  async handle(context: RequestContext) {
    try {
      const { request, response } = context

      const finished = await this.invokeMiddleware(context)
      if (finished) {
        return
      }
      const route = this.findRoute(request)
      const args = await this.parseParams(request, route)

      if (route.pathParams && route.pathParams.provider) {
        request.query['oauth2-provider-name'] = route.pathParams.provider
      }

      const t = await this.authenticateRequest(request)
      const result = await this.invoke(route, args)
      this.send(response, result)
    } catch (error) {
      /**
       * Authentication errors for login page are handled by the express app
       */
      if (
        context.request.path === '/login' &&
        (error.status === 401 || error.name === 'UnauthorizedError')
      ) {
        /**
         * The express app that routed the /signup call to LB App, will handle the error event.
         */
        context.response.emit('UnauthorizedError', 'User Authentication Failed')
        return
      }
      if (
        error.code === AUTHENTICATION_STRATEGY_NOT_FOUND ||
        error.code === USER_PROFILE_NOT_FOUND
      ) {
        Object.assign(error, { statusCode: 401 /* Unauthorized */ })
      }
      this.reject(context, error)
    }
  }
}
