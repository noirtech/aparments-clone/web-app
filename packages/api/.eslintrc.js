module.exports = {
  extends: '@loopback/eslint-config',
  plugins: ['prettier'],
  rules: {
    'prettier/prettier': 'error'
  }
}
